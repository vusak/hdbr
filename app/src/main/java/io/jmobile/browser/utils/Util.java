package io.jmobile.browser.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.BaseFragment;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class Util {
    private static SimpleDateFormat dayFormat = new SimpleDateFormat("d MMM", Locale.US);
    private static SimpleDateFormat hourFormat = new SimpleDateFormat("kk", Locale.getDefault());
    private static SimpleDateFormat monthFormat = new SimpleDateFormat("MMM, yyyy", Locale.getDefault());
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.getDefault());
    //    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM, dd, yyyy", Locale.US);
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.US);

    public static Activity getActivity(Object object) {
        return object instanceof Activity ? (Activity) object : ((Fragment) object).getActivity();
    }

    public static boolean isTablet(Context context) {
        Configuration config = context.getResources().getConfiguration();

        if (Build.VERSION.SDK_INT >= 13)
            return config.smallestScreenWidthDp >= 600;
        else
            return false;
    }

    public static boolean isNetworkAbailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;

        } else
            return false;
    }

    public static String getNetworkConnectionType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null ? null : activeNetworkInfo.getTypeName();
    }

    public static void lockRotatation(Activity activity) {
        switch (activity.getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                break;

            case Configuration.ORIENTATION_LANDSCAPE:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                break;
        }
    }

    public static void unlockRotation(Activity activity) {
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null)
            imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    public static void hideKeyBoard(View v) {
        v.clearFocus();
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void showKeyBoard(View v) {
        v.requestFocus();
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
    }

    public static <T extends BaseFragment> T addf(FragmentManager fm, String tag, BaseFragment.BaseFragmentCreator<T> creator) {
        T t = null;
        BaseFragment f = ff(fm, tag);
        if (f != null)
            t = (T) f;
        else {
            t = creator.create();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(creator.getFrameId(), t, tag);
            ft.commit();
        }

        return t;
    }

    public static <T extends BaseFragment> T ff(FragmentManager fm, String tag) {
        if (fm == null || tag == null)
            return null;

        Fragment f = fm.findFragmentByTag(tag);

        try {
            return (T) f;
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            return null;
        }
    }

    // find dialog fragment
    @SuppressWarnings("unchecked")
    public static <T extends BaseDialogFragment> T fdf(FragmentManager fm, String tag) {
        if (fm == null || tag == null)
            return null;

        Fragment f = fm.findFragmentByTag(tag);

        try {
            return (T) f;
        } catch (Exception e) {
            return null;
        }
    }

    // hide dialog fragment
    public static void hdf(FragmentManager fm, String tag) {
        DialogFragment prev = fdf(fm, tag);
        if (prev != null)
            prev.dismissAllowingStateLoss();
    }

    // show dialog fragment
    public static void sdf(FragmentManager fm, BaseDialogFragment d) {
        if (fm == null || d == null || d.getDialogTag() == null)
            return;

        hdf(fm, d.getDialogTag());
        try {
            d.show(fm, d.getDialogTag());
        } catch (Exception e) {
        }
    }

    public static String getTimeString(long date) {
        return timeFormat.format(new Date(date));
    }

    public static String getDateString(long date) {
        return dateFormat.format(new Date(date));
    }

    public static String getFullDateString(long date) {
        return format.format(new Date(date));
    }

    public static String getDay(long date) {
        return dayFormat.format(new Date(date));
    }

    public static String getHour(long date) {
        return hourFormat.format(new Date(date));
    }

    public static String getMonth(long date) {
        return monthFormat.format(new Date(date));
    }

    public static boolean isToday(String date) {
        String today = getDateString(System.currentTimeMillis());
        return today.equalsIgnoreCase(date);
    }

    public static String convertSecondsToHMmSs(long seconds) {
//        long s = seconds % 60;
//        long m = (seconds / 60) % 60;
//        long h = (seconds / (60 * 60)) % 24;
        long h = MILLISECONDS.toHours(seconds) % 24;
        long m = MILLISECONDS.toMinutes(seconds) % 60;
        long s = MILLISECONDS.toSeconds(seconds) % 60;

        return String.format("%d hr %02d min %02d sec", h, m, s);
    }

    public static boolean isYesterday(String date) {
        long temp = 1000 * 60 * 60 * 24;
        String yesterday = getDateString(System.currentTimeMillis() - temp);
        return yesterday.equalsIgnoreCase(date);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String optString(JSONObject o, String name) {
        return o.isNull(name) ? null : o.optString(name);
    }
}
