package io.jmobile.browser.utils.file;

import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.jmobile.browser.utils.LogUtil;

public class FileUtil {
    public static final String TEMP_FILE_EXTENSION = ".download";
    public static final String TEMP_THUMBNAIL_EXTENSION = ".thumbnail";
    private static final long K = 1024;
    private static final long M = K * K;
    private static final long G = M * K;
    private static final long T = G * K;

    public static long getFileSizeFromURL(String url) {
        try {
            return new DownloadFileSizeTask().execute(url).get();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private static long getFileSize(String urlString) {
        if (TextUtils.isEmpty(urlString))
            return -1;

        HttpURLConnection conn = null;
        try {

            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            return (long) conn.getContentLength();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (conn != null)
                conn.disconnect();
        }
    }

    public static ArrayList<String> getSearcApihList(String keyword) {
        try {
            return new SearchApiTask().execute(keyword).get();
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
        return null;
    }

    private static ArrayList<String> getSearchList(String keyword) throws Exception {
        String key = "AIzaSyC6La-To6tQLp_2WcbfC6_OKHYPQqdxhLU";
        String cx = "008401810535068536793:hygmhbikxc8";
//        String qry="Android";
        URL url = new URL(
                "https://www.googleapis.com/customsearch/v1?key=" + key + "&cx=" + cx + "&q=" + keyword + "&alt=json");
        LogUtil.log(url.toString());
        ArrayList<String> result = new ArrayList<>();

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            if (output.contains("\"link\": \"")) {
                String link = output.substring(output.indexOf("\"link\": \"") + ("\"link\": \"").length(), output.indexOf("\","));
                System.out.println(link);       //Will print the google search links
                result.add(link);
            }
        }
        conn.disconnect();

        return result;
    }

    public static boolean mkdirs(File file) {
        return file.exists() || file.mkdirs();
    }

    public static boolean delete(File file) {
        if (!file.exists())
            return false;

        File[] files = file.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isDirectory())
                    delete(f);
                else
                    f.delete();
            }
        }

        return file.delete();
    }

    public static String getFileExtention(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public static long getAvailableInternalTotalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());

        return stat.getBlockSize() * stat.getBlockCount();
    }

    public static String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();

        return convertToStringRepresentation(availableBlocks * blockSize);
    }

    public static String convertToStringRepresentation(final long value) {
        if (value < 1)
            return "waiting...";

        final long[] dividers = new long[]{T, G, M, K, 1};
        final String[] units = new String[]{"TB", "GB", "MB", "KB", "B"};
        if (value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            final long divider = dividers[i];
            if (value >= divider) {
                result = format(value, divider, units[i]);

                if (i == 3)
                    result = format(value, dividers[i - 1], units[i - 1]);
                break;
            }
        }
        return result;
    }

    public static String dashboardSizeUnit(final long value) {
        if (value < 1)
            return "GB";

        final long[] dividers = new long[]{T, G, M, K, 1};
        final String[] units = new String[]{"TB", "GB", "MB", "KB", "B"};
        if (value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            final long divider = dividers[i];
            if (value >= divider) {
//                result = format(value, divider, units[i]);
                result = units[i];

                if (i == 3)
                    result = units[i - 1];
//                    result = format(value , dividers[i -1], units[i - 1]);
                break;
            }
        }
        return result;
    }

    public static String dashboardSize(final long value) {
        if (value < 1)
            return "0.0";

        final long[] dividers = new long[]{T, G, M, K, 1};
        final String[] units = new String[]{"TB", "GB", "MB", "KB", "B"};
        if (value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            final long divider = dividers[i];
            if (value >= divider) {
                result = format2(value, divider);
//                result = units[i];

                if (i == 3)
//                    result = units[i-1];
                    result = format2(value, dividers[i - 1]);
                break;
            }
        }
        return result;
    }

    public static String convertToStringTime(final long time) {
        long duration = time / 1000;
        long hours = duration / 3600;
        long minutes = (duration - hours * 3600) / 60;
        long seconds = duration - (hours * 3600 + minutes * 60);

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private static String format(final long value,
                                 final long divider,
                                 final String unit) {
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return String.format("%.1f %s", Double.valueOf(result), unit);
    }

    private static String format2(final long value,
                                  final long divider) {
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return String.format("%.1f", Double.valueOf(result));
    }

    public static double formatForGraph(final long value,
                                        final long divider) {
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return result;
    }

    public static String getFileName(String fileName) {
        if (TextUtils.isEmpty(fileName))
            return "";

        int separatorIndex = fileName.lastIndexOf(File.separator);
        int dotIndex = fileName.lastIndexOf(".");

        if (dotIndex < 0)
            return fileName.substring(separatorIndex + 1);
        else
            return fileName.substring(separatorIndex + 1, dotIndex);
    }

    public static abstract class DownloadTask<T> extends AsyncTask<Object, Long, Integer> {
        public static final int RESULT_OK = 0;
        public static final int RESULT_CANCEL = 1;
        public static final int RESULT_EXCEPTION = 2;
        private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
        private static final int CORE_POOL_SIZE = CPU_COUNT + 1;
        private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1;
        private static final int KEEP_ALIVE = 1;
        private static final BlockingQueue<Runnable> sPoolWorkQueue = new LinkedBlockingDeque<Runnable>(128);
        private static final ThreadPoolExecutor DOWNLOAD_EXECUTOR = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS, sPoolWorkQueue, new ThreadPoolExecutor.DiscardOldestPolicy());
        private static final int TIMEOUT = 20000;
        private static final int BUFFER_SIZE = 1024 * 2;
        private static String path = null;

        protected WeakReference<T> ref;

        public DownloadTask(T target) {
            ref = new WeakReference<T>(target);
        }

        public abstract void onProgress(long Progress, long total);

        public abstract void onSuccess();

        public abstract void onError(int resultCode);

        @Override
        protected Integer doInBackground(Object... params) {
            String path = (String) params[0];
            String to = (String) params[1];
            boolean continuing = (Boolean) params[2];
            int timeout = (Integer) params[3];
            String token = (String) params[4];
            return downloadInternal(path, to, continuing, timeout, token);
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);

            onProgress(values[0], values[1]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == RESULT_OK)
                onSuccess();
            else
                onError(result);
        }

        private int downloadInternal(String path, String to, boolean continuing, int timeout, String token) {
            InputStream in = null;
            File file = new File(to + TEMP_FILE_EXTENSION);
            if (file.getParentFile() != null)
                mkdirs(file.getParentFile());
            RandomAccessFile out = null;

            try {
                if (!continuing)
                    delete(file);
                file.createNewFile();
                out = new RandomAccessFile(file, "rw");

                long curr = 0;
                if (continuing) {
                    curr = out.length();
                    out.seek(curr);
                }
                // URL url = new URL(baseUrls[i] + path);
                URL url = new URL(path);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(timeout);
                conn.setReadTimeout(timeout);
                conn.setRequestProperty("Range", "bytes=" + curr + "-");
                if (token != null)
                    conn.setRequestProperty("ACCESS_TOKEN", token);
                conn.setInstanceFollowRedirects(true);
                HttpURLConnection.setFollowRedirects(true);
                int status = conn.getResponseCode();
                if (status != HttpURLConnection.HTTP_OK) {
                    if (status == HttpURLConnection.HTTP_MOVED_TEMP
                            || status == HttpURLConnection.HTTP_MOVED_PERM
                            || status == HttpURLConnection.HTTP_SEE_OTHER) {
                        String newUrl = conn.getHeaderField("Location");
                        conn = (HttpURLConnection) new URL(newUrl).openConnection();
                        conn.setConnectTimeout(timeout);
                        conn.setReadTimeout(timeout);
                        conn.setRequestProperty("Range", "bytes=" + curr + "-");
                        if (token != null)
                            conn.setRequestProperty("ACCESS_TOKEN", token);
                    }
                }
                conn.connect();

                in = conn.getInputStream();
                long remain = conn.getContentLength();
                long length = curr + remain;
                byte[] buffer = new byte[BUFFER_SIZE];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    if (!isCancelled()) {
                        out.write(buffer, 0, read);
                        curr += read;
//                        Thread.sleep(1);
                        publishProgress(curr, length);
                    } else {
                        if (!continuing)
                            delete(file);
                        return RESULT_CANCEL;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (!continuing)
                    delete(file);
                return RESULT_EXCEPTION;
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException e) {
                }

                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                }
            }

            File toFile = new File(to);
            file.renameTo(toFile);
            return RESULT_OK;
        }

        public T getTarget() {
            return ref.get();
        }

        public void setTarget(T target) {
            if (ref != null)
                ref.clear();
            ;
            ref = new WeakReference<T>(target);
        }

        public void download(String from, String to, String[] baseUrls, String accessToken) {
            download(from, to, true, TIMEOUT, accessToken);
        }

        public void download(String from, String to, boolean continuing, String accessToken) {
            download(from, to, continuing, TIMEOUT, accessToken);
        }

        public void download(String from, String to, boolean continuing, int timeout, String accessToken) {
            executeOnExecutor(DOWNLOAD_EXECUTOR, from, to, continuing, timeout, accessToken);
        }
    }

    private static class DownloadFileSizeTask extends AsyncTask<String, Void, Long> {
        @Override
        protected Long doInBackground(String... params) {
            return getFileSize(params[0]);
        }
    }

    private static class SearchApiTask extends AsyncTask<String, Void, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(String... params) {
            try {
                return getSearchList(params[0]);
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }

            return null;
        }
    }
}
