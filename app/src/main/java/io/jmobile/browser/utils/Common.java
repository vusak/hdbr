package io.jmobile.browser.utils;

import android.content.Context;


public class Common {
    public static String FAN_MAIN_NATIVE = "1952254934995789_1952257708328845";
    public static String FAN_DASH_NATIVE = "1952254934995789_1952485481639401";
    public static String FAN_TAB_NATIVE = "1952254934995789_1983117115242904";
    public static String FAN_LIST_DOWNLOAD = "1952254934995789_1952485358306080";
    public static String FAN_LIST_QUICKMARK = "1952254934995789_1952257878328828";
    public static String FAN_MAIN_BANNER = "1952254934995789_1952485131639436";

    public static String ADMOB_SPLASH_BANNER = "ca-app-pub-8870534780213740/2624829424";
    public static String ADMOB_EXIT_BANNER = "ca-app-pub-8870534780213740/6994218887";
    public static String ADMOB_EXIT_BANNER_TEST = "ca-app-pub-3940256099942544/2177258514";
    public static String ADMOB_MAIN_NATIVE_MID = "ca-app-pub-8870534780213740/8227409504";
    public static String ADMOB_DASH_NATIVE_MID = "ca-app-pub-8870534780213740/9129427638";
    public static String ADMOB_MAIN_NATIVE_ADVANCED = "ca-app-pub-8870534780213740/1171931356";
    public static String ADMOB_MAIN_NATIVE_ADVANCED_TEST = "ca-app-pub-3940256099942544/2247696110";
    public static String ADMOB_DASH_NATIVE_ADVANCED = "ca-app-pub-8870534780213740/3820353415";
    public static String ADMOB_TAB_NATIVE_ADVANCED = "ca-app-pub-8870534780213740/5989359368";
    public static String ADMOB_MAIN_NATIVE_MID_TEST = "ca-app-pub-3940256099942544/2793859312";
    public static String ADMOB_LIST_DOWNLOAD_NATIVE = "ca-app-pub-8870534780213740/2368578162";
    public static String ADMOB_LIST_QUICKMARK_NATIVE = "ca-app-pub-8870534780213740/5206849719";
    public static String ADMOB_LIST_NATIVE_TEST = "ca-app-pub-3940256099942544/2247696110";
    public static String ADMOB_MAIN_BANNER_OPT = "ca-app-pub-8870534780213740/3730116512";
    public static String ADMOB_MAIN_BANNER_OPT_TEST = "ca-app-pub-3940256099942544/6300978111";
    public static String ADMOB_DOWNLOAD_NATIVE_ADVANCED = "ca-app-pub-8870534780213740/8721401319";

    //    public static String MOBVISTA_APP_WALL = "10738";
    public static String SITE = "";


    public static String PREFIX_DOWNLOAD_FILE = "JMB_";
    public static int width = 720;
    public static boolean NOW_LOADING = false;
    public static boolean NOW_DOWNLOAD = false;
    public static String FRAG_INTENT_DOWNLOAD_STATUS = "downloadStatus";
    public static String FRAG_INTENT_SELECTED_TAB = "selectedTab";
    public static String FRAG_INTENT_ADD_QUICK_DIAL = "addQuickDial";
    public static int ARG_TAB_BOOKMARKS = 0;
    public static int ARG_TAB_READING = 1;
    public static int ARG_TAB_HISTORY = 2;
    public static String INTENT_GO_TO_URL = "go_to_url";
    public static String TAG_DIALOG_ALERT_YOUTUBE = "youtube_alert";
    public static String TAG_DIALOG_ALERT_WARNING = "warning_alert";
    public static String TAG_DIALOG_ADD_DOWNLOAD = "add_download";
    public static String TAG_DIALOG_DELETE_DOWNLOAD_FOLDER = "delete_download_folder";
    public static String TAG_DIALOG_DELETE_ALL_ITEMS = "delete_all_items";
    public static String TAG_DIALOG_DELETE_ITEMS = "delete_items";
    public static String META_NAME_NAME = "og:title";
    public static String META_NAME_DESCRIPTION = "og:description";
    public static String META_NAME_URL = "og:url";
    public static String META_NAME_IMAGE = "og:image";
    public static String META_NAME_TAG = "og:video:tag";
    public static String META_NAME_TYPE = "og:type";
    public static String MAIN_JMOBILE_APP = "http://jmobile.io/apps/";

    public static String getVideoDir(Context context) {
        String path;
        try {
            path = android.os.Environment.getExternalStorageDirectory()
                    + "/JM Browser/";
        } catch (Exception e) {
            path = context.getFilesDir().getPath() + "/JM Browser/";
        }
        return path;
    }

    public static String checkVideoExtension(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp.*")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4.*")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac.*")) {
            fileExt = ".flac";
        } else if (url.matches(".*.ogg.*")) {
            fileExt = ".ogg";
        } else if (url.matches(".*.wma.*")) {
            fileExt = ".wma";
        } else if (url.matches(".*.ape.*")) {
            fileExt = ".ape";
        } else if (url.matches(".*.avi.*")) {
            fileExt = ".avi";
        } else if (url.matches(".*.mp3.*")) {
            fileExt = ".mp3";
        } else if (url.matches(".*.wmv.*")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.wma.*")) {
            fileExt = ".wma";
        } else if (url.matches(".*.mpg.*")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv.*")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv.*")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf.*")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov.*")) {
            fileExt = ".mov";
        } else {
            fileExt = ".mp4";
        }
        return fileExt;
    }

    public static String checkAvailableDownload(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp.*")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4.*")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac.*")) {
            fileExt = ".flac";
        } else if (url.matches(".*.wmv.*")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.mpg.*")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv.*")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv.*")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf.*")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov.*")) {
            fileExt = ".mov";
        } else {
            fileExt = "novideo";
        }
        return fileExt;
    }
}
