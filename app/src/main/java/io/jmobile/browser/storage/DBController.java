package io.jmobile.browser.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.BrowserItem;
import io.jmobile.browser.data.DashboardItem;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.data.ExtraItem;
import io.jmobile.browser.data.FavoriteItem;
import io.jmobile.browser.data.HistoryItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.data.ReadingItem;

/**
 * Created by Cecil on 2017-05-30.
 */

public class DBController {
    public static final String DB_ID = "_id";

    private static final String DB_NAME = "jbrowser.db";
    private static final int DB_VERSION = 3;

    private Context context;
    private DBHelper helper;
    private SQLiteDatabase db;

    public DBController(Context context) {
        this.context = context;

        if (helper == null) {
            helper = new DBHelper(context.getApplicationContext());
            db = helper.getWritableDatabase();
        }
    }

    public synchronized long getNextDBId(String tableName) {
        long id = -1;

        Cursor cursor = db.rawQuery("select seq from SQLITE_SEQUENCE where name = ?", new String[]{tableName});
        if (cursor.moveToFirst())
            id = cursor.getLong(cursor.getColumnIndex("seq"));
        cursor.close();

        return id + 1;
    }

    public synchronized void deleteAllData() {

    }

    public synchronized List<DownloadItem> getTotalDownloadItems() {
        return DownloadItem.getDownloadItems(db, null, null, null, null, null, DownloadItem.DOWNLOAD_START_AT + " ASC", null);
    }

    public synchronized List<DownloadItem> getAllDownloadItems() {
        return DownloadItem.getDownloadItems(db, null, null, null, null, null, DownloadItem.ROW_ID + " DESC", null);
    }

    public synchronized List<DownloadItem> getAllRDownloadItems() {
        return DownloadItem.getDownloadItems(db, null, null, null, null, null, DownloadItem.DOWNLOAD_START_AT + " DESC", null);
    }

    public synchronized List<DownloadItem> getAllCDownloadItems() {
        return DownloadItem.getDownloadItems(db, null, null, null, null, null, DownloadItem.DOWNLOAD_AT + " DESC", null);
    }

    public synchronized List<DownloadItem> getDownloadItems(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return DownloadItem.getDownloadItems(db, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public synchronized DownloadItem getDownloadItem(String id) {
        return DownloadItem.getDownloadItem(db, id);
    }

    public synchronized boolean isDownloadFileName(String name) {
        return DownloadItem.isDownloadFileName(db, name);
    }

    public synchronized boolean restateDownloadState() {
        return DownloadItem.restateDownloadState(db);
    }

    public synchronized boolean insertOrUpdateDownloadItem(DownloadItem item) {
        return DownloadItem.insertOrUpdateDownloadItem(db, item);
    }

    public synchronized boolean updateDownloadState(DownloadItem item) {
        return DownloadItem.updateDownloadState(db, item);
    }

    public synchronized boolean cancelAllItems() {
        return DownloadItem.cancelAllDownloadItems(db);
    }

    public synchronized boolean deleteDownloadItem(DownloadItem item) {
        return DownloadItem.deleteDownloadItem(db, item);
    }

    public synchronized boolean deleteDownloadItems(ArrayList<DownloadItem> list) {
        return DownloadItem.deleteDownloadedItems(db, list);
    }

    public synchronized boolean deleteAllNonDownloadItems() {
        return DownloadItem.deleteAllNonDownloadItems(db);
    }

    public synchronized boolean deleteAllDownloadedItems() {
        return DownloadItem.deleteAllDownloadedItems(db);
    }

    public synchronized int getReadyDownloadItemCount() {
        return DownloadItem.readyDownloadItemCount(db);
    }

    public synchronized QuickDialItem getQuickDialItem(String id) {
        return QuickDialItem.getQuickDialItem(db, id);
    }

    public synchronized List<QuickDialItem> getQuickDialItems() {
        return QuickDialItem.getQuickDialItems(db, null, null, null, null, null, QuickDialItem.QUICKDIAL_ORDER + " ASC", null);
    }

    public synchronized boolean insertOrUpdateQuickDialItem(QuickDialItem item) {
        return QuickDialItem.insertOrUpdateQuickDialItem(db, item);
    }

    public synchronized boolean updateQuickDial(QuickDialItem item) {
        return QuickDialItem.updateQuickDial(db, item);
    }

    public synchronized boolean updateQuickDialOrder(QuickDialItem item) {
        return QuickDialItem.updateQuickDialOrder(db, item);
    }

    public synchronized boolean updateQuickDialShowing(QuickDialItem item) {
        return QuickDialItem.updateQuickDialShowing(db, item);
    }

    public synchronized boolean deleteQuickDialItem(QuickDialItem item) {
        return QuickDialItem.deleteQuickDialItem(db, item);
    }

    public synchronized boolean deleteQuickDialItem(BookmarkItem item) {
        return QuickDialItem.deleteQuickDialItem(db, item);
    }

    public synchronized boolean deleteAllQuickDialItems() {
        return QuickDialItem.deleteAllQuickDialItems(db);
    }

    public synchronized BookmarkItem getBookmarkItem(String id) {
        return BookmarkItem.getBookmarkItem(db, id);
    }

    public synchronized List<BookmarkItem> getBookmarkItems() {
        return BookmarkItem.getBookmarkItems(db, null, null, null, null, null, BookmarkItem.BOOKMARK_ORDER + " ASC", null);
    }

    public synchronized boolean insertOrUpdateBookmarkItem(BookmarkItem item) {
        return BookmarkItem.insertOrUpdateBookmarkItem(db, item);
    }

    public synchronized boolean updateBookmark(BookmarkItem item) {
        return BookmarkItem.updateBookmark(db, item);
    }

    public synchronized boolean updateBookmarkOrder(BookmarkItem item) {
        return BookmarkItem.updateBookmarkOrder(db, item);
    }

    public synchronized boolean updateBookmarkShowing(BookmarkItem item) {
        return BookmarkItem.updateBookmarkShowing(db, item);
    }

    public synchronized boolean deleteBookmarkItem(BookmarkItem item) {
        return BookmarkItem.deleteBookmarkItem(db, item);
    }

    public synchronized boolean deleteAllBookmarkItems() {
        return BookmarkItem.deleteAllBookmarkItems(db);
    }

    public synchronized ReadingItem getReadingItem(String id) {
        return ReadingItem.getReadingItem(db, id);
    }

    public synchronized List<ReadingItem> getReadingItems() {
        return ReadingItem.getReadingItems(db, null, null, null, null, null, ReadingItem.ROW_ID + " DESC", null);
    }

    public synchronized boolean deleteDoneReadingItem() {
        return ReadingItem.deleteDoneReadingItem(db);
    }

    public synchronized boolean insertOrUpdateReadingItem(ReadingItem item) {
        return ReadingItem.insertOrUpdateReadingItem(db, item);
    }

    public synchronized boolean deleteReadingItem(ReadingItem item) {
        return ReadingItem.deleteReadingItem(db, item);
    }

    public synchronized boolean deleteAllReadingItems() {
        return ReadingItem.deleteAllReadingItems(db);
    }

    public synchronized HistoryItem getHistoryItem(String id) {
        return HistoryItem.getHistoryItem(db, id);
    }

    public synchronized List<HistoryItem> getHistoryItems() {
        return HistoryItem.getHistoryItems(db, null, null, null, null, null, HistoryItem.ROW_ID + " DESC", null);
    }

    public synchronized boolean insertOrUpdateHistoryItem(HistoryItem item) {
        return HistoryItem.insertOrUpdateHistoryItem(db, item);
    }

    public synchronized boolean deleteHistory(int past) {
        return HistoryItem.deleteHistory(db, past);
    }

    public synchronized boolean deleteHistoryItem(HistoryItem item) {
        return HistoryItem.deleteHistoryItem(db, item);
    }

    public synchronized boolean deleteAllHistoryItems() {
        return HistoryItem.deleteAllHistoryItems(db);
    }

    public synchronized FavoriteItem getFavoriteItem(String url) {
        return FavoriteItem.getFavoriteItem(db, url);
    }

    public synchronized List<FavoriteItem> getFavoriteItems() {
        return FavoriteItem.getFavoriteItems(db, null, null, null, null, null, FavoriteItem.FAVORITE_COUNT + " DESC", null);
    }

    public synchronized boolean insertOrUpdateFavoriteItem(FavoriteItem item) {
        return FavoriteItem.insertOrUpdateFavoriteItem(db, item);
    }

    public synchronized boolean deleteFavoriteItem(FavoriteItem item) {
        return FavoriteItem.deleteFavoriteItem(db, item);
    }

    public synchronized boolean deleteAllFavoriteItem() {
        return FavoriteItem.deleteAllFavoriteItems(db);
    }

    public synchronized BrowserItem getBrowserItem(String id) {
        return BrowserItem.getBrowserItem(db, id);
    }

    public synchronized List<BrowserItem> getBrowserItems() {
        return BrowserItem.getBrowserItems(db, null, null, null, null, null, BrowserItem.BROWSER_ORDER + " ASC", null);
    }

    public synchronized boolean insertOrUpdateBrowserItem(BrowserItem item) {
        return BrowserItem.insertOrUpdateBrowserItem(db, item);
    }

    public synchronized boolean updateBrowser(BrowserItem item) {
        return BrowserItem.updateBrowser(db, item);
    }

    public synchronized boolean updateBrowserkOrder(BrowserItem item) {
        return BrowserItem.updateBrowserOrder(db, item);
    }

    public synchronized boolean updateBrowserPrivacy(BrowserItem item) {
        return BrowserItem.updateBrowserPrivacy(db, item);
    }

    public synchronized boolean deleteBrowserItem(BrowserItem item) {
        return BrowserItem.deleteBrowserItem(db, item);
    }

    public synchronized boolean deleteAllBrowserItems() {
        return BrowserItem.deleteAllBrowserItems(db);
    }

    public synchronized boolean insertOrUpdateExtraItem(ExtraItem item) {
        return ExtraItem.insertOrUpdateExtraItem(db, item);
    }

    public synchronized boolean insertOrUpdateExtraItem(byte[] home) {
        return ExtraItem.insertOrUpdateExtraItem(db, home);
    }

    public synchronized byte[] getExtraHomeImage() {
        return ExtraItem.getExtraHomeImage(db);
    }

    public synchronized boolean insertOrUpdateDashboardItem(DashboardItem item) {
        return DashboardItem.insertOrUpdateDashboardItem(db, item);
    }

    public synchronized List<DashboardItem> getDashboardItems() {
        return DashboardItem.getDashboardItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean deleteAllDashboardItems() {
        return DashboardItem.deleteAllDashboardItems(db);
    }

    private static class DBHelper extends SQLiteOpenHelper {
        DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create tables
            createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch (oldVersion) {
                case 1:
                    updateVer1ToVer2(db);
                case 2:
                    updateVer2toVer3(db);
            }
        }

        private void updateVer1ToVer2(SQLiteDatabase db) {
            BrowserItem.createTableBrowserItems(db);
            ExtraItem.createTableExtraItems(db);
        }

        private void updateVer2toVer3(SQLiteDatabase db) {
            DashboardItem.createTableDashboard(db);
        }

        private void dropAllTables(SQLiteDatabase db) {

        }

        private void createAllTables(SQLiteDatabase db) {
            DownloadItem.createTableDownload(db);
            BookmarkItem.createTableBookmarks(db);
            ReadingItem.createTableReadings(db);
            HistoryItem.createTableHistory(db);
            FavoriteItem.createTableFavorite(db);
            QuickDialItem.createTableQuickDials(db);
            BrowserItem.createTableBrowserItems(db);
            ExtraItem.createTableExtraItems(db);
            DashboardItem.createTableDashboard(db);
        }
    }
}
