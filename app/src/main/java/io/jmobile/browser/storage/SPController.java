package io.jmobile.browser.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

import io.jmobile.browser.data.DownloadItem;

/**
 * Created by Cecil on 2017-05-30.
 */

public class SPController {
    public static final String FIRST_SET_HOW_TO_USE = "first_set_how_to_use";
    public static final boolean DEFAULT_FIRST_SET_HOW_TO_USE = true;
    public static final String FIRST_SET_DASHBOARD = "first_set_dashboard";
    public static final boolean DEFAULT_FIRST_SET_DASHBOARD = true;
    private static final String BROWSER_URL = "browser_url";
    private static final String DEFAULT_BROWSER_URL = "";
    private static final String CHECKED_DOWNLOAD_URL = "checked_download_url";
    private static final String DEFAULT_CHECKED_DOWNLOAD_URL = "";

    private static final String SET_USE_WIFI_ONLY = "set_use_wifi_only";
    private static final boolean DEFAULT_SET_USE_WIFI_ONLY = false;

    private static final String SET_SEARCH_ENGINE = "set_search_engine";
    private static final int DEFAULT_SEARCH_ENGINE = 0;

    private static final String SET_MAX_DOWNLOADS = "set_max_download";
    private static final int DEFAULT_MAX_DOWNLOADS = 2;

    private static final String SET_USE_TIME_FILTERING = "set_use_time_filtering";
    private static final int DEFAULT_USE_TIME_FILTERING = 0;

    private static final String SET_USE_NOTIFICATION = "set_use_notification";
    private static final boolean DEFAULT_USE_NOTIFICATION = false;

    private static final String SET_USE_SAVE_PASSWORDS = "set_use_save_passwords";
    private static final boolean DEFAULT_USE_SAVE_PASSWORDS = true;

    private static final String DOWNLOAD_LIST_MODE = "download_list_mode";
    private static final boolean DEFAULT_DOWNLOAD_LIST_MODE = false;

    private static final String CURRENT_BROWSER_INDEX = "current_browser_index";
    private static final int DEFAULT_CURRENT_BROWSER_INDEX = -1;

    private static final String BROWSER_TAB_COUNT = "browser_tab_count";
    private static final int DEFAULT_BROWSER_TAB_COUNT = 1;

    private static final String DOWNLOAD_LIST_SORT = "download_list_sort";
    private static final String DEFAULT_DOWNLOAD_LIST_SORT = DownloadItem.DOWNLOAD_SORT_DATE_DESC;

    private static final String DOWNLOAD_LIST_SORT_DATE_ASC = "download_list_sort_date_asc";
    private static final boolean DEFAULT_DOWNLOAD_LIST_SORT_DATE_ASC = false;

    private static final String DOWNLOAD_LIST_SORT_SIZE_ASC = "download_list_sort_date_asc";
    private static final boolean DEFAULT_DOWNLOAD_LIST_SORT_SIZE_ASC = false;
    protected SharedPreferences sp;

    public SPController(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getBrowserUrl() {
        return sp.getString(BROWSER_URL, DEFAULT_BROWSER_URL);
    }

    public void setBrowserUrl(String val) {
        put(BROWSER_URL, val);
    }

    public boolean isFirstSetHowToUse() {
        return sp.getBoolean(FIRST_SET_HOW_TO_USE, DEFAULT_FIRST_SET_HOW_TO_USE);
    }

    public void setFirstSetHowToUse(boolean val) {
        put(FIRST_SET_HOW_TO_USE, val);
    }

    public boolean isFirstSetDashboard() {
        return sp.getBoolean(FIRST_SET_DASHBOARD, DEFAULT_FIRST_SET_DASHBOARD);
    }

    public void setFirstSetDashboard(boolean val) {
        put(FIRST_SET_DASHBOARD, val);
    }

    public String getCheckedDownloadUrl() {
        return sp.getString(CHECKED_DOWNLOAD_URL, DEFAULT_CHECKED_DOWNLOAD_URL);
    }

    public void setCheckedDownloadUrl(String val) {
        put(CHECKED_DOWNLOAD_URL, val);
    }

    public boolean isWifiOnly() {
        return sp.getBoolean(SET_USE_WIFI_ONLY, DEFAULT_SET_USE_WIFI_ONLY);
    }

    public void setUseWifiOnly(boolean val) {
        put(SET_USE_WIFI_ONLY, val);
    }

    public int getSearchEngine() {
        return sp.getInt(SET_SEARCH_ENGINE, DEFAULT_SEARCH_ENGINE);
    }

    public void setSearchEngine(int val) {
        put(SET_SEARCH_ENGINE, val);
    }

    public int getMaxDownloads() {
        return sp.getInt(SET_MAX_DOWNLOADS, DEFAULT_MAX_DOWNLOADS);
    }

    public void setMaxDownloads(int val) {
        put(SET_MAX_DOWNLOADS, val);
    }

    public int getTimeFiltering() {
        return sp.getInt(SET_USE_TIME_FILTERING, DEFAULT_USE_TIME_FILTERING);
    }

    public void setTimeFiltering(int val) {
        put(SET_USE_TIME_FILTERING, val);
    }

    public boolean isUseNotification() {
        return sp.getBoolean(SET_USE_NOTIFICATION, DEFAULT_USE_NOTIFICATION);
    }

    public void setUseNotification(boolean val) {
        put(SET_USE_NOTIFICATION, val);
    }

    public boolean isSavePasswords() {
        return sp.getBoolean(SET_USE_SAVE_PASSWORDS, DEFAULT_USE_SAVE_PASSWORDS);
    }

    public void setSetUseSavePasswords(boolean val) {
        put(SET_USE_SAVE_PASSWORDS, val);
    }

    public boolean isDownloadListMode() {
        return sp.getBoolean(DOWNLOAD_LIST_MODE, DEFAULT_DOWNLOAD_LIST_MODE);
    }

    public void setDownloadListMode(boolean val) {
        put(DOWNLOAD_LIST_MODE, val);
    }

    public int getCurrentBrowserIndex() {
        return sp.getInt(CURRENT_BROWSER_INDEX, DEFAULT_CURRENT_BROWSER_INDEX);
    }

    public void setCurrentBrowserIndex(int val) {
        put(CURRENT_BROWSER_INDEX, val);
    }

    public int getBrowserTabCount() {
        return sp.getInt(BROWSER_TAB_COUNT, DEFAULT_BROWSER_TAB_COUNT);
    }

    public void setBrowserTabCount(int val) {
        put(BROWSER_TAB_COUNT, val);
    }

    public String getDownloadListSort() {
        return sp.getString(DOWNLOAD_LIST_SORT, DEFAULT_DOWNLOAD_LIST_SORT);
    }

    public void setDownloadListSort(String val) {
        put(DOWNLOAD_LIST_SORT, val);
    }

    public boolean isDownloadListSortDateAsc() {
        return sp.getBoolean(DOWNLOAD_LIST_SORT_DATE_ASC, DEFAULT_DOWNLOAD_LIST_SORT_DATE_ASC);
    }

    public void setDownloadListSortDateAsc(boolean val) {
        put(DOWNLOAD_LIST_SORT_DATE_ASC, val);
    }

    public boolean isDownloadListSortSizeAsc() {
        return sp.getBoolean(DOWNLOAD_LIST_SORT_SIZE_ASC, DEFAULT_DOWNLOAD_LIST_SORT_SIZE_ASC);
    }

    public void setDownloadListSortSizeAsc(boolean val) {
        put(DOWNLOAD_LIST_SORT_SIZE_ASC, val);
    }

    public void clearSP() {
        sp.edit().clear().commit();
    }

    protected void put(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    protected void put(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }

    protected void put(String key, float value) {
        sp.edit().putFloat(key, value).commit();
    }

    protected void put(String key, long value) {
        sp.edit().putLong(key, value).commit();
    }

    protected void put(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    protected void put(String key, Set<String> value) {
        sp.edit().putStringSet(key, value).commit();
    }
}
