package io.jmobile.browser;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import io.jmobile.browser.download.JDownloadManager;
import io.jmobile.browser.download.JNetworkMonitor;
import io.jmobile.browser.storage.DBController;
import io.jmobile.browser.storage.SPController;


public class BaseApplication extends Application implements JNetworkMonitor.OnChangeNetworkStatusListener {
    private SPController sp;
    private DBController db;
    private ResourceWrapper r;
    private JDownloadManager downloadManager;
    private JNetworkMonitor networkMonitor;

    @Override
    public void onCreate() {
        super.onCreate();

        sp = new SPController(getApplicationContext());
        db = new DBController(getApplicationContext());
        r = new ResourceWrapper(getResources());

        downloadManager = new JDownloadManager(this);
        networkMonitor = new JNetworkMonitor(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkMonitor, filter);
    }

    public SPController getSPController() {
        return this.sp;
    }

    public DBController getDBController() {
        return this.db;
    }

    public ResourceWrapper getResourceWrapper() {
        return r;
    }

    public JDownloadManager getDownloadManager() {
        return this.downloadManager;
    }

    public JNetworkMonitor getNetworkMonitor() {
        return this.networkMonitor;
    }

    @Override
    public void onNetworkChanged(int status) {
        if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE) {
            Toast.makeText(this, "NETWORK_TYPE_MOBILE", Toast.LENGTH_SHORT).show();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_WIFI) {
            Toast.makeText(this, "NETWORK_TYPE_WIFI", Toast.LENGTH_SHORT).show();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
            Toast.makeText(this, "NETWORK_TYPE_NOT_CONNECTED", Toast.LENGTH_SHORT).show();
        }
    }

    public static class ResourceWrapper {
        private Resources r;

        public ResourceWrapper(Resources r) {
            this.r = r;
        }

        public int px(int id) {
            return r.getDimensionPixelSize(id);
        }

        public String s(int id) {
            return r.getString(id);
        }

        public String[] sa(int id) {
            return r.getStringArray(id);
        }

        public int c(Context context, int id) {
            return ContextCompat.getColor(context, id);
        }

        public Drawable d(Context context, int id) {
            return ContextCompat.getDrawable(context, id);
        }

        public boolean b(int id) {
            return r.getBoolean(id);
        }

        public int i(int id) {
            return r.getInteger(id);
        }

        public long l(int id) {
            return Long.parseLong(s(id));
        }
    }
}
