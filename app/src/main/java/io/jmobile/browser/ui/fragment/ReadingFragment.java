package io.jmobile.browser.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.jmobile.browser.R;
import io.jmobile.browser.data.ReadingItem;
import io.jmobile.browser.ui.adapter.ReadingAdapter;
import io.jmobile.browser.ui.base.BaseFragment;

public class ReadingFragment extends BaseFragment {
    ExpandableListView lv;
    ReadingAdapter adapter;
    ArrayList<ReadingItem> list;
    Map<String, List<ReadingItem>> map;
    ArrayList<String> groupNameList;

    LinearLayout emptyLayout;

    private ReadingFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_reading;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        emptyLayout = fv(R.id.layout_empty);

        lv = fv(R.id.lv);
        lv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String group = groupNameList.get(groupPosition);
                if (adapter.isCheckMode()) {
                    adapter.select(group, childPosition);

                    if (listener != null)
                        listener.onReadingCheckChanged(adapter.getSelectedItem().size());
                } else {

                    String url = map.get(group).get(childPosition).getReadingUrl();
                    ReadingItem item = map.get(group).get(childPosition);
                    if (!item.isReadingDone()) {
                        item.setReadingDone(true);
                        db.insertOrUpdateReadingItem(item);
                    }

                    if (listener != null)
                        listener.onReadingItemClick(url);
                }
                return false;
            }
        });

        list = new ArrayList<>();
        map = new HashMap<>();
        groupNameList = new ArrayList<>();


        adapter = new ReadingAdapter(getActivity(), map, groupNameList);
//        adapter = new ReadingAdapter(getActivity(), R.layout.item_reading, list, new ReAdapter.ReOnItemClickListener() {
//            @Override
//            public void OnItemClick(int position, Object item) {
//                if (adapter.isCheckMode())
//                    adapter.select(position);
//
//                if (listener != null)
//                    listener.onReadingCheckChanged(adapter.getSelectedList().size());
//            }
//        });
        lv.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }

    public void deleteItems(boolean isAll) {
        if (isAll)
            db.deleteAllReadingItems();
        else {
            for (ReadingItem item : adapter.getSelectedItem()) {
                db.deleteReadingItem(item);
            }
            adapter.clearSelection();
            listener.onReadingCheckChanged(0);
        }
        refreshList();
    }

    public void refreshList() {
        if (list == null)
            return;

        list.clear();
        list.addAll(db.getReadingItems());

        groupNameList.clear();
        map.clear();

        for (ReadingItem item : list) {
            if (item.isReadingDone()) {
                String read = r.s(R.string.reading_read);
                if (groupNameList.indexOf(read) < 0) {
                    groupNameList.add(read);
                    map.put(read, new ArrayList<ReadingItem>());
                }
                map.get(read).add(item);
            } else {
                String unRead = r.s(R.string.reading_unread);
                if (groupNameList.indexOf(unRead) < 0) {
                    groupNameList.add(0, unRead);
                    map.put(unRead, new ArrayList<ReadingItem>());
                }

                map.get(unRead).add(item);
            }
        }
        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        emptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);

        for (int i = 0; i < groupNameList.size(); i++)
            lv.expandGroup(i);

        if (listener != null)
            listener.notiEmptyReadingList(empty);
    }

    public void selectModeClick(boolean mode) {
        if (adapter != null)
            adapter.setCheckMode(mode);
    }

    public void setListener(ReadingFragmentListener listener) {
        this.listener = listener;
    }

    public static interface ReadingFragmentListener {
        public void onReadingCheckChanged(int count);

        public void onReadingItemClick(String url);

        public void notiEmptyReadingList(boolean empty);
    }

}
