package io.jmobile.browser.ui.fragment;


import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.BrowserItem;
import io.jmobile.browser.data.FavoriteItem;
import io.jmobile.browser.data.HistoryItem;
import io.jmobile.browser.data.ReadingItem;
import io.jmobile.browser.download.DDownloadLinkProvider;
import io.jmobile.browser.download.TDownloadLinkProvider;
import io.jmobile.browser.download.VDownloadLinkProvider;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.ui.base.ExpandLayout;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.image.ImageUtil;

public class BrowserFragment extends BaseFragment implements GestureDetector.OnGestureListener {

    public static final String INTENT_URI_START = "intent:";
    public static final String INTENT_FALLBACK_URL = "browser_fallback_url";
    public static final String URI_SCHEME_MARKET = "market://details?id=";

    public WebView webView;
    TextView urlEdit, newTabButton;
    ImageButton refreshButton;
    ProgressBar loadingProgress;
    ExpandLayout urlExpand;

    String currentTitle, currentSite, addHistoryUrl;
    Bitmap currentIcon;

    String readingTitle, readingUrl, readingDescription, readingImage;
    ArrayList<String> readingTags;
    GestureDetector detector;
    float initialY = -1;
    String checkUrl = "";
    boolean CLEAR_HISTORY = false;

    BrowserItem item;
    LinearLayout rootLayout;

    LinearLayout mainTopLayout;
    private RelativeLayout childLayout;
    private RelativeLayout browserLayout;
    private ImageButton mainCloseButton;
    private TextView titleText;

    private BrowserFragmentListsner listener;

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();

        detector = new GestureDetector(getActivity(), this);

        if (webView != null) {

            if ((webView.copyBackForwardList() == null || (webView.copyBackForwardList().getSize() <= 0) && (item != null && item.getBrowserHistory() != null))) {
                if (webView.getOriginalUrl() != null && webView.getOriginalUrl().equalsIgnoreCase(item.getBrowserUrl()))
                    return;

//            if (item.getBrowserHistory() != null) {
                byte[] bundleBytes = item.getBrowserHistory();
                final Parcel parcel = Parcel.obtain();
                parcel.unmarshall(bundleBytes, 0, bundleBytes.length);
                parcel.setDataPosition(0);
                Bundle bundle = (Bundle) parcel.readBundle();
                parcel.recycle();

                webView.restoreState(bundle);

                if (webView.getUrl() != null)
                    urlEdit.setText(webView.getUrl());
            }
        }

        if (getArguments() != null) {
            String url = getArguments().getString(Common.INTENT_GO_TO_URL);
            if (!TextUtils.isEmpty(url))
                goUrl(url);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_browser;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (webView != null) {
            webView.resumeTimers();
            webView.onResume();
            webView.reload();
        }

        if (urlExpand != null)
            urlExpand.collapse(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }


    }

    @Override
    public void onDestroy() {
        if (webView != null) {
            webView.stopLoading();
            webView.onPause();
            webView.destroy();
        }

        super.onDestroy();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (childLayout != null && childLayout.getVisibility() == View.VISIBLE)
            return true;

        if (urlExpand.isExpanded() && distanceY > 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.collapse(true);
            initialY = distanceY;
            return true;
        } else if (!urlExpand.isExpanded() && distanceY < 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.expand(true);
            initialY = distanceY;
            return true;
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    public boolean onPrevButtonClicked() {
        if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
            WebView view = (WebView) browserLayout.getChildAt(0);
            if (view != null && view.canGoBack())
                view.goBack();
            else
                closeChild();
            return true;
        }
        else if (webView != null && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return false;
    }

    public void onNextButtonClicked() {
        if (webView != null)
            webView.goForward();
    }

    public void setURLString(String url) {
        urlEdit.setText(url);
    }

    public void setListener(BrowserFragmentListsner listener) {
        this.listener = listener;
    }

    public BrowserItem getBrowserItem() {
        return this.item;
    }

    public void setBrowserItem(BrowserItem item) {
        if (item == null)
            return;

        this.item = item;

    }

    public void goUrl(final String url) {
        if (urlExpand != null && !urlExpand.isExpanded())
            urlExpand.expand(false);

        if (urlEdit != null)
            urlEdit.post(new Runnable() {
                @Override
                public void run() {
                    urlEdit.setText(url);
                }
            });

        if (webView != null)
            webView.post(new Runnable() {
                @Override
                public void run() {
                    webView.onResume();
                    webView.loadUrl(url);
//                    webView.postUrl(url, null);
                }
            });

        if (newTabButton != null)
            newTabButton.post(new Runnable() {
                @Override
                public void run() {
                    newTabButton.setText("" + sp.getBrowserTabCount());
                }
            });
//            newTabButton.setText("" + count);
    }

    public void newTabButtonClicked() {
        Bundle bundle = new Bundle();
        webView.saveState(bundle);
        final Parcel parcel = Parcel.obtain();
        bundle.writeToParcel(parcel, 0);
        byte[] bundleBytes = parcel.marshall();
        parcel.recycle();

        if (bundleBytes != null)
            item.setBrowserHistory(bundleBytes);
        new UpdateBrowserItem().execute(item);
    }

    private void initView() {
        if (LogUtil.isDebugMode() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            WebView.setWebContentsDebuggingEnabled(true);

        rootLayout = fv(R.id.layout_root);

        urlExpand = fv(R.id.layout_expend_url);
        urlExpand.expand(false);

        browserLayout = fv(R.id.mainBrowserLayout);
        childLayout = fv(R.id.mainAdChildLayout);
        titleText = fv(R.id.mainTitleText);
        mainTopLayout = fv(R.id.mainLinearLayout);
        mainCloseButton = fv(R.id.mainCloseButton);
        mainCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeChild();
            }
        });

        newTabButton = fv(R.id.text_new_tab);
        newTabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newTabButtonClicked();
            }
        });
        newTabButton.setText("" + sp.getBrowserTabCount());

        webView = fv(R.id.webview);

        if (!sp.isSavePasswords()) {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(webView.getContext());
            android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.removeSessionCookies(null);
                cookieManager.removeAllCookies(null);
            } else {
                cookieManager.removeSessionCookie();
                cookieManager.removeAllCookie();
            }
            cookieSyncManager.sync();
        }

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDatabaseEnabled(sp.isSavePasswords());
        webView.getSettings().setAppCacheEnabled(sp.isSavePasswords());
        webView.getSettings().setDomStorageEnabled(sp.isSavePasswords());
        webView.getSettings().setSavePassword(sp.isSavePasswords());
        webView.getSettings().setSaveFormData(sp.isSavePasswords());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportMultipleWindows(true);

        webView.setWebViewClient(new MyWebViewClient());
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return detector.onTouchEvent(event);
            }
        });
        webView.requestFocus();

        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                currentTitle = title;
                currentSite = view.getOriginalUrl();
                item.setBrowserTitle(title);
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);
//                item.setBrowserIcon(ImageUtil.getImageBytes(icon));
                currentIcon = icon;
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                loadingProgress.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView.HitTestResult result = view.getHitTestResult();
                String url = result.getExtra();
                if (TextUtils.isEmpty(url)) {
                    Message href = view.getHandler().obtainMessage();
                    view.requestFocusNodeHref(href);

                    url = href.getData().getString("url");
                    if (TextUtils.isEmpty(url)) {
                        openChild(resultMsg);
                        return true;
                    }
                }
                switch (result.getType()) {
                    case WebView.HitTestResult.EMAIL_TYPE:
                        openEmail(url);
                        return false;
                }
                if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                    if (childLayout != null && childLayout.getVisibility() == View.VISIBLE) {
                        closeChild();
                    }
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return false;
                }
                // 구글 마켓 이동 doFallback
                else if (url.toLowerCase().startsWith(INTENT_URI_START)) {
                    Intent parsedIntent = null;
                    try {
                        parsedIntent = Intent.parseUri(url, 0);
                        startActivity(parsedIntent);
                    } catch (ActivityNotFoundException | URISyntaxException e) {
                        return doFallback(view, parsedIntent);
                    }
                } else {
                    openChild(resultMsg);
                }
                return true;
            }
        });

        urlEdit = fv(R.id.text_url);
        urlEdit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (webView != null)
                    webView.onPause();
                if (listener != null)
                    listener.onUrlEditClick(webView.getUrl());
                return false;
            }
        });

        refreshButton = fv(R.id.btn_url_refresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.reload();
            }
        });

        loadingProgress = fv(R.id.progress_url);
    }

    private void addHistoryList(String url) {
        if (TextUtils.isEmpty(url))
            return;

        addHistoryUrl = url;
        HistoryItem item = new HistoryItem();
        item.setHistoryId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setHistoryUrl(url);
        item.setHistoryTitle(currentTitle);
        item.setHistoryAt(System.currentTimeMillis());


        new AddHistoryItem().execute(item);
    }

    public void addBookmarkItem() {
        if (currentSite == null || currentTitle == null || currentSite.isEmpty() || currentTitle.isEmpty()) {
            if (listener != null)
                listener.onShowToast(r.s(R.string.toast_message_failed_bookmak));
            return;
        }

        BookmarkItem item = new BookmarkItem();
        item.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setBookmarkUrl(currentSite);
        item.setBookmarkName(currentTitle);
        item.setBookmarkShowing(false);
        item.setBookmarkOrder((int) db.getNextDBId(BookmarkItem.TABLE_NAME));
        item.setBookmarkAt(System.currentTimeMillis());

        new AddBookmarkItem().execute(item);
    }

    public void addReadingItem() {
        if (readingTitle == null || readingUrl == null || readingTitle.isEmpty() || readingUrl.isEmpty()) {
            if (listener != null)
                listener.onShowToast(r.s(R.string.toast_message_failed_reading));
            return;
        }

        ReadingItem item = new ReadingItem();
        item.setReadingId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setReadingUrl(readingUrl);
        item.setReadingTitle(readingTitle);
        item.setReadingContents(readingDescription);
        item.setReadingAt(System.currentTimeMillis());

        new AddReadingItem().execute(item);
//        return db.insertOrUpdateReadingItem(item);
    }

    public void setHomeData() {
        if (webView != null) {
//            webView.loadUrl("about:blank");
            webView.clearHistory();
            item.setBrowserUrl("HOME");
            item.setBrowserTitle("HOME");
            item.setBrowserIcon((byte[]) null);
            item.setBrowserThumbnail((byte[]) null);
            item.setBrowserHistory((byte[]) null);
            db.updateBrowser(item);
        }
    }

    public void clearCurrData() {

        readingTitle = "";
        readingUrl = "";
        readingDescription = "";
        readingImage = "";
        currentIcon = null;
        currentTitle = "";
        currentSite = "";
        addHistoryUrl = "";

        if (listener != null)
            listener.onClearReadyList();
    }

    public void stopWebViewLoading() {
        if (webView != null) {
            webView.onPause();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static interface BrowserFragmentListsner {
        public void onUrlEditClick(String url);

        public void onAccessYoutebe();

        public void onAccessDownloadUrl(String site, String url);

        public void onWebviewScrolled();

        public void onShowToast(String message);

        public void onNewTabClick();

        public void onClearReadyList();
    }

    private class MyWebViewClient extends WebViewClient {

        String compareText = "";

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            resend.sendToTarget();
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            if (url.equals("about:blank")) {
                urlEdit.setText("");
                clearCurrData();
            }
            super.onPageCommitVisible(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url.matches("(?i).*youtube.*")) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return;
            }
            loadingProgress.setVisibility(View.VISIBLE);
            clearCurrData();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            view.loadUrl("javascript:window.Android.getHeader(document.getElementsByTagName('head')[0].innerHTML);");

            compareText = url;
            loadingProgress.setVisibility(View.INVISIBLE);
            LogUtil.log("cecil onpageFinished :: " + url);

            if (url.equals("about:blank") || Common.SITE.equals(compareText)) {
                return;
            }

            Common.SITE = url;
            sp.setBrowserUrl(url);
            item.setBrowserUrl(url);
            urlEdit.setText(url);
            currentSite = url;
            urlExpand.collapse(false);

//            addHistoryList(currentSite);
            super.onPageFinished(view, url);
        }

        @Override
        public void onLoadResource(WebView view, final String url) {

            if (!Common.checkAvailableDownload(url).equals("novideo")) { // url에 비디오 관련 확장자 없음
                if (!checkUrl.equals(url)) {
                    checkUrl = url;
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                URLConnection u = new URL(url).openConnection();
                                String type = u.getHeaderField("Content-Type");

                                if (url.matches("(?i).*youtube.*")) {

                                } else if (type.matches("video.*")) {
                                    if (url.contains("dailymotion.com/sec(")) {
                                        /** Daily Motion
                                         if (url.contains("frag(2)")) {
                                         String videoLink = currentSite;
                                         int index = currentSite.indexOf("?playlist");
                                         if (index > 0)
                                         videoLink = currentSite.substring(0, index);
                                         LogUtil.log("DAILYMOTION VIDEO LINK :: " + videoLink);
                                         if (!TextUtils.isEmpty(videoLink)) {
                                         new generateDDownloadLink().execute(videoLink);
                                         }
                                         }
                                         **/
                                    }
                                    else if (listener != null)
                                        listener.onAccessDownloadUrl(currentSite, url);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    t.start();
                }
            }else if (url.contains("skyfiregce-vimeo") && url.contains("master.json")) {
                LogUtil.log("onLoadResource :: " + url);
                String[] temp = url.split("/");
                Pattern p = Pattern.compile("(^[0-9]*$)");

                String videoId = "";
                if (temp != null && temp.length > 0) {
                    for (String v : temp) {
                        LogUtil.log("check url :: " + v);
                        if (!TextUtils.isEmpty(v) && p.matcher(v).find()) {
                            videoId = v;
                            break;
                        }
                    }
                }
                if (!TextUtils.isEmpty(videoId)) {
                    new generateVDownloadLink().execute(videoId);

                }
            } else if (url.contains("clips.twitch.tv") && url.contains("/view")) {
                LogUtil.log("HDVD URL (onLoadResource) :: " + url);
                String source = url.replace("view", "status");
                if (!TextUtils.isEmpty(source))
                    new generateTDownloadLink().execute(source);
            }
            super.onLoadResource(view, url);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            addHistoryList(currentSite);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
            if (Common.checkAvailableDownload(url).equals("novideo") && url.matches("http.*")) { // url에 비디오 관련 확장자 없음
                if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
//                    view.postUrl(url, null);
//                    view.loadUrl(url);
                    return false;
                }
            } else {
//                if (!sp.getCheckedDownloadUrl().equals(url)) {
//                    sp.setCheckedDownloadUrl(url);
                if (!checkUrl.equals(url)) {
                    checkUrl = url;
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                URLConnection u = new URL(url).openConnection();
                                String type = u.getHeaderField("Content-Type");
                                if (url.matches("(?i).*youtube.*")) {

                                } else if (type.matches("video.*")) {
//                                    downloadHandler.sendEmptyMessage(0);
                                    if (listener != null)
                                        listener.onAccessDownloadUrl(currentSite, url);
                                } else {
//                                    view.loadUrl(url);
//                                    return false;
                                }


                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }
                    });

                    t.start();
                }
            }

            return true;
        }


    }

    // 구글 마켓 이동
    private boolean doFallback(WebView view, Intent parsedIntent) {
        if (parsedIntent == null) {
            return false;
        }

        String fallbackUrl = parsedIntent.getStringExtra(INTENT_FALLBACK_URL);
        if (fallbackUrl != null) {
            view.loadUrl(fallbackUrl);
            return true;
        }

        String packageName = parsedIntent.getPackage();
        if (packageName != null) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URI_SCHEME_MARKET + packageName)));
            return true;
        }
        return false;
    }

    private void closeChild() {
        int colorStart = r.c(getActivity(), R.color.alpha30_black);
        int colorEnd = Color.TRANSPARENT;

        ValueAnimator colorAnim = ObjectAnimator.ofInt((View) mainTopLayout, "backgroundColor", colorStart, colorEnd);
        colorAnim.setDuration(100);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(0);
        colorAnim.start();

        Animation slideClose = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_close);
        childLayout.startAnimation(slideClose);
        slideClose.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                titleText.setText("");
                childLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void openChild(Message resultMsg) {
        // remove any current child views
        browserLayout.removeAllViews();
        // make the child web view's layout visible
        childLayout.setVisibility(View.VISIBLE);

        // now create a new web view
        WebView newView = new WebView(getActivity());
        WebSettings settings = newView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSupportMultipleWindows(true);
        settings.setUseWideViewPort(false);
        newView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                // once the view has loaded, display its title
                titleText.setText(view.getTitle());
            }
        });
        newView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                openChild(resultMsg);
                return true;
            }
        });
        // add the new web view to the layout
        newView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        browserLayout.addView(newView);
        // tell the transport about the new view
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(newView);
        resultMsg.sendToTarget();

        // let's be cool and slide the new web view up into view
        Animation slideOpen = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_open);
        childLayout.startAnimation(slideOpen);

        if (urlExpand != null && urlExpand.isExpanded())
            urlExpand.collapse(true);

        int colorStart = Color.TRANSPARENT;
        int colorEnd = r.c(getActivity(), R.color.alpha30_black);

        ValueAnimator colorAnim = ObjectAnimator.ofInt((View) mainTopLayout, "backgroundColor", colorStart, colorEnd);
        colorAnim.setDuration(4000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(0);
        colorAnim.start();
    }

    private void openEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + email));
        intent.putExtra(Intent.EXTRA_EMAIL, email);

        startActivity(intent);
    }

    private class MyJavascriptInterface {

        @JavascriptInterface
        public void getHeader(String header) {
            readingTitle = "";
            readingUrl = "";
            readingDescription = "";
            readingImage = "";
            readingTags = new ArrayList<>();

            Pattern p = Pattern.compile("(<meta.*?property=\")(.*?)(\".*?content=\")(.*?)(\".*?>)", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
            Matcher m = p.matcher(header);
            while (m.find()) {
                String name = m.group(2);
                String content = m.group(4).replace("&quot;", "\"");
                if (name.equalsIgnoreCase(Common.META_NAME_NAME))
                    readingTitle = content;

                if (name.equalsIgnoreCase(Common.META_NAME_URL))
                    readingUrl = content;

                if (name.equalsIgnoreCase(Common.META_NAME_DESCRIPTION))
                    readingDescription = content;

                if (name.equalsIgnoreCase(Common.META_NAME_IMAGE))
                    readingImage = content;

                if (name.equalsIgnoreCase(Common.META_NAME_TAG))
                    readingTags.add(content);

                LogUtil.log("name : " + name + "  content : " + content);
            }
        }
    }

    class UpdateBrowserItem extends AsyncTask<BrowserItem, Void, BrowserItem> {
        @Override
        protected BrowserItem doInBackground(BrowserItem... params) {
            BrowserItem item = params[0];
            if (webView != null) {
                item.setBrowserThumbnail(ImageUtil.getScreenShotByteArray(getActivity(), webView));
            }

            if (currentIcon != null) {
                item.setBrowserIcon(ImageUtil.getImageBytes(currentIcon));
            }

            return item;
        }

        @Override
        protected void onPostExecute(BrowserItem browserItem) {
            db.insertOrUpdateBrowserItem(browserItem);
            if (listener != null)
                listener.onNewTabClick();
        }
    }

    class AddReadingItem extends AsyncTask<ReadingItem, Void, ReadingItem> {
        @Override
        protected ReadingItem doInBackground(ReadingItem... params) {
            ReadingItem item = params[0];
            Bitmap bitmap = null;
            if (!readingImage.isEmpty()) {
                bitmap = ImageUtil.getImageFromURLNonTask(readingImage);
                if (bitmap != null)
                    item.setReadingThumbnail(ImageUtil.getImageBytes(bitmap));
            }
            return item;
        }

        @Override
        protected void onPostExecute(ReadingItem item) {
            boolean result = db.insertOrUpdateReadingItem(item);
            if (listener != null) {
                if (item == null || !result)
                    listener.onShowToast(r.s(R.string.toast_message_failed_reading));
                else
                    listener.onShowToast(r.s(R.string.toast_message_added_reading));
            }
        }
    }

    class AddBookmarkItem extends AsyncTask<BookmarkItem, Void, BookmarkItem> {
        @Override
        protected BookmarkItem doInBackground(BookmarkItem... params) {
            BookmarkItem item = params[0];
            String path = item.getBookmarkUrl();
            if (TextUtils.isEmpty(path))
                return null;

            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);

            if (bitmap != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));
            else if (currentIcon != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(currentIcon));

            return item;
        }

        @Override
        protected void onPostExecute(BookmarkItem item) {

            boolean result = db.insertOrUpdateBookmarkItem(item);
            if (listener != null) {
                if (item == null || !result)
                    listener.onShowToast(r.s(R.string.toast_message_failed_bookmak));
                else
                    listener.onShowToast(r.s(R.string.toast_message_added_bookmak));
            }
        }
    }

    class AddHistoryItem extends AsyncTask<HistoryItem, Void, HistoryItem> {
        @Override
        protected HistoryItem doInBackground(HistoryItem... params) {
            HistoryItem item = params[0];
            String path = item.getHistoryUrl();
            if (TextUtils.isEmpty(path))
                return null;

            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);

            if (bitmap != null)
                item.setHistoryIcon(ImageUtil.getImageBytes(bitmap));
            else if (currentIcon != null)
                item.setHistoryIcon(ImageUtil.getImageBytes(currentIcon));

            return item;
        }

        @Override
        protected void onPostExecute(HistoryItem item) {
            db.insertOrUpdateHistoryItem(item);

            String temp = item.getHistoryUrl().replace("http://", "");
            temp = temp.replace("https://", "");
            FavoriteItem favorite = db.getFavoriteItem(temp);
            if (favorite != null) {
                favorite.setFavoriteCount(favorite.getFavoriteCount() + 1);
            } else
                favorite = new FavoriteItem();
            favorite.setFavoriteUrl(temp);
            favorite.setFavoriteSite(item.getHistoryUrl());
            favorite.setFavoriteTitle(item.getHistoryTitle());
            favorite.setFavoriteIcon(item.getHistoryIcon());
            favorite.setFavoriteAt(item.getHistoryAt());
            db.insertOrUpdateFavoriteItem(favorite);
        }
    }

    /**
     * DailyMotion
     **/
    class generateDDownloadLink extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String videoId = strings[0];
            String downloadUrl = "";
            try {
                DDownloadLinkProvider provider = new DDownloadLinkProvider(videoId);
                downloadUrl = provider.getVideoLink();
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            return downloadUrl;
        }

        @Override
        protected void onPostExecute(String downloadUrl) {
            super.onPostExecute(downloadUrl);
            if (listener != null)
                listener.onAccessDownloadUrl(currentSite, downloadUrl);
        }
    }

    /**
     * Twitch Clip
     */
    class generateTDownloadLink extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String videoId = strings[0];
            String downloadUrl = "";
            try {
                TDownloadLinkProvider provider = new TDownloadLinkProvider(videoId);
                downloadUrl = provider.getVideoLink();
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            return downloadUrl;
        }

        @Override
        protected void onPostExecute(String downloadUrl) {
            super.onPostExecute(downloadUrl);
            if (listener != null)
                listener.onAccessDownloadUrl(currentSite, downloadUrl);
        }
    }

    /**
     * Vimeo
     **/
    class generateVDownloadLink extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String videoId = strings[0];
            String downloadUrl = "";
            try {
                VDownloadLinkProvider provider = new VDownloadLinkProvider(videoId);
                downloadUrl = provider.generateDownloadLink();
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            return downloadUrl;
        }

        @Override
        protected void onPostExecute(String downloadUrl) {
            super.onPostExecute(downloadUrl);
            if (listener != null)
                listener.onAccessDownloadUrl(currentSite, downloadUrl);
        }
    }

}
