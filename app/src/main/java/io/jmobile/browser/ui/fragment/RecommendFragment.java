package io.jmobile.browser.ui.fragment;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.ui.adapter.BookmarkAdapter;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.utils.image.ImageUtil;

//import com.mopub.nativeads.MoPubRecyclerAdapter;
//import com.mopub.nativeads.NativeAdFactory;

public class RecommendFragment extends BaseFragment {

    RecyclerView lv;
    LinearLayoutManager manager;
    BookmarkAdapter adapter;
    //    MoPubRecyclerAdapter adAdapter;
    ArrayList<BookmarkItem> list;
    LinearLayout emptyLayout;
    private RecommendFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_bookmarks;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        emptyLayout = fv(R.id.layout_empty);
        emptyLayout.setVisibility(View.GONE);

        lv = fv(R.id.lv);
        lv.setVisibility(View.VISIBLE);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new BookmarkAdapter(getActivity(), R.layout.item_bookmark, list, new BookmarkAdapter.BookmarkAdapterListener() {
            @Override
            public void onEditBookmarkClick(final int position, BookmarkItem item) {

            }

            @Override
            public void OnItemClick(int position, BookmarkItem item) {
//                position = adAdapter.getOriginalPosition(position);
//                item = adapter.getItem(position);
                if (!checkQuickDialLimit() && !item.isBookmarkShowing()) {
                    if (listener != null)
                        listener.onShowToast(r.s(R.string.toast_message_limit_quick));
                } else {
                    item.setBookmarkShowing(!item.isBookmarkShowing());
                    list.get(position).setBookmarkShowing(item.isBookmarkShowing());
                    adapter.notifyItemChanged(position);

                    if (listener != null) {
                        listener.onBookmarkItemClick(item);
                    }
                }
            }

            @Override
            public void OnItemLongClick(int position, BookmarkItem item) {

            }
        });
        lv.setAdapter(adapter);
//        adAdapter = NativeAdFactory.getMoPubRecyclerAdapter(getActivity(), adapter, Common.ADX_LIST_QUICKMARK_NATIVE);
//        lv.setAdapter(adAdapter);
//        adAdapter.loadAds(Common.ADX_LIST_QUICKMARK_NATIVE);
        adapter.setMode(BookmarkAdapter.MODE_ADD_QUICK);


    }

    @Override
    public void onResume() {
        initRecommendList();
        super.onResume();
    }

    public void setListener(RecommendFragmentListener listener) {
        this.listener = listener;
    }

    public void initRecommendList() {

        list.clear();
        String[] nameList = getResources().getStringArray(R.array.recommend_name);
        String[] urlList = getResources().getStringArray(R.array.recommend_url);
        TypedArray iconList = getResources().obtainTypedArray(R.array.recommend_icon);

        for (int i = 0; i < nameList.length; i++) {
            BookmarkItem item = new BookmarkItem();
            item.setBookmarkId("" + i);
            item.setBookmarkUrl(urlList[i]);
            item.setBookmarkShowing(false);
            item.setBookmarkName(nameList[i]);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), iconList.getResourceId(i, -1));
            if (bitmap != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));
            list.add(item);
        }

        BookmarkItem hItem = new BookmarkItem();
        hItem.setBookmarkId(QuickDialItem.ID_HOW_TO_USE);
        hItem.setBookmarkUrl(QuickDialItem.ID_HOW_TO_USE);
        hItem.setBookmarkShowing(false);
        hItem.setBookmarkName(getString(R.string.settings_title_how));
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.btn_how_to_use_50);
        if (bitmap != null)
            hItem.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));
        list.add(0, hItem);

        ArrayList<QuickDialItem> quickList = new ArrayList<>();
        quickList.addAll(db.getQuickDialItems());
        if (list != null && list.size() > 0) {
            for (QuickDialItem quick : quickList) {
                if (quick.getQuickDialFrom() == QuickDialItem.ADD_RECOMMEND) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getBookmarkId().equalsIgnoreCase(quick.getQuickdialFromId()))
                            list.get(i).setBookmarkShowing(true);

                    }
                }
            }
        }

        adapter.notifyDataSetChanged();

    }

    private boolean checkQuickDialLimit() {
        return db.getQuickDialItems() == null || db.getQuickDialItems().size() < QuickDialItem.MAX_SHOWING;
    }

    public static interface RecommendFragmentListener {
        public void onBookmarkItemClick(BookmarkItem item);

        public void onShowToast(String message);
    }

//    private void showLimitMessage() {
//        if (fdf(MessageDialog.TAG) == null) {
//            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//            dlg.setMessage("No more Quick Dial can be added.");
//            dlg.setPositiveLabel(r.s(R.string.ok));
//            sdf(dlg);
//        }
//    }
}
