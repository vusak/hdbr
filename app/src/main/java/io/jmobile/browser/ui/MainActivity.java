package io.jmobile.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import io.jmobile.browser.R;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;

public class MainActivity extends BaseActivity {

    LinearLayout splashAdLayout, bannerLayout;
    Button startButton;
    TextView startText, mainText, subText;
    ProgressBar startProgress;
    ProgressHandler startHandler;
    boolean isRunning = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.gray7_100));
        }
        initView();
//        initAd();
    }

    private void initView() {
        startButton = fv(R.id.button_start);
        startButton.setVisibility(View.GONE);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FrontActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mainText = fv(R.id.text_main);
        subText = fv(R.id.text_sub);
        startText = fv(R.id.text_start);
        startProgress = fv(R.id.progress_start);
        showProgress();
    }

    private void showProgress() {
        startProgress.setProgress(0);
        startHandler = new ProgressHandler();
        isRunning = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 100 && isRunning; i++) {
                        Thread.sleep(20);
                        startHandler.sendEmptyMessage(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void initAd() {
        splashAdLayout = fv(R.id.layout_splash_ads);
        AdView admobIntroBanner = new AdView(MainActivity.this);
        admobIntroBanner.setAdSize(AdSize.MEDIUM_RECTANGLE);
        admobIntroBanner.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_EXIT_BANNER_TEST : Common.ADMOB_SPLASH_BANNER);
//        admobExitBanner.setBackground(getResources().getDrawable(R.drawable.bg_none));
        admobIntroBanner.setBackgroundColor(Color.TRANSPARENT);
        if (LogUtil.DEBUG_MODE) {
            AdRequest request = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
                    .addTestDevice("AC98C820A50B4AD8A2106EDE96FB87D4")  // An example device ID
                    .build();
            admobIntroBanner.loadAd(request);
        } else
            admobIntroBanner.loadAd(new AdRequest.Builder().build());
        splashAdLayout.addView(admobIntroBanner);

//        bannerLayout = fv(R.id.layout_splash_banner);
//        AdView admobBanner = new AdView(MainActivity.this);
//        admobBanner.setAdSize(AdSize.BANNER);
//        admobBanner.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_BANNER_OPT_TEST : Common.ADMOB_MAIN_BANNER_OPT);
//        admobBanner.setBackgroundColor(Color.TRANSPARENT);
//        admobBanner.loadAd(new AdRequest.Builder().build());
//        bannerLayout.addView(admobBanner);
//        admobBanner.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                LogUtil.log("fanBanner MainActivity onAdLoaded()");
//            }
//        });
    }

    class ProgressHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            startProgress.incrementProgressBy(2);

            if (startProgress.getProgress() >= startProgress.getMax()) {
                startHandler.removeMessages(0);
                startProgress.setVisibility(View.GONE);
                startText.setVisibility(View.GONE);
                startButton.setVisibility(View.VISIBLE);
                mainText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                subText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
            }
        }
    }
}
