package io.jmobile.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.data.DashboardItem;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.ui.base.ExpandLayout;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.file.FileUtil;

public class DashboardActivity extends BaseActivity {
    private static final int XLABEL_MAX_TODAY = 24;
    private static final int XLABEL_MAX_WEEKLY = 7;
    private static final int XLABEL_MAX_MONTHLY = 30;
    private static final long K = 1024;
    private static final long M = K * K;
    private static final long G = M * K;
    private static final long T = G * K;
    ImageButton backButton, menuButton, menuCloseButton;
    Button resetButton;
    ExpandLayout menuExpand;
    View closeMenuView;
    GraphView graph;
    LineGraphSeries<DataPoint> series;
    FrameLayout nativeAdsLayout;
    NativeAd fanNativeAd;
//    com.mopub.nativeads.NativeAd mNativeAd;
//    View mAdView;
//    LinearLayout mContentView;

    ProgressBar totalMemorySizeProgress;
    TextView totalMemorySizeText;
    ArrayList<DashboardItem> allItems, filterItems;
    ArrayList<DashGraphItem> graphItems;
    String xLabel = "h";
    private TabLayout tabLayout;
    private TextView countText1, countText2, sizeText1, sizeText2, sizeSubText1, sizeSubText2, timeText;
    private LinearLayout graphModeLayout, textModeLayout;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(r.c(this, R.color.set_title_background));
        }

        initView();
//        initAds();
        refreshLIst();
    }

    @Override
    public void onBackPressed() {
        if (menuExpand != null && menuExpand.isExpanded())
            visibleMenu(false);
        else {
            Intent intent = new Intent(DashboardActivity.this, DownloadActivity.class);
            startActivity(intent);
            finish();
//            super.onBackPressed();
        }
    }

    private void initView() {
        backButton = fv(R.id.btn_top_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        menuExpand = fv(R.id.layout_expend_menu);
        menuExpand.collapse(false);
        closeMenuView = fv(R.id.view_close_menu);
        closeMenuView.setVisibility(View.GONE);
        closeMenuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
            }
        });
        menuButton = fv(R.id.btn_top_menu);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(true);
            }
        });

        menuCloseButton = fv(R.id.btn_menu_close);
        menuCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
            }
        });

        resetButton = fv(R.id.btn_menu_reset_all);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
                db.deleteAllDashboardItems();
                refreshLIst();
            }
        });

        graphModeLayout = fv(R.id.layout_mode_graph);
        textModeLayout = fv(R.id.layout_mode_text);
        textModeLayout.setVisibility(View.GONE);
        tabLayout = fv(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.dashboard_today)));
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.dashboard_weekly)));
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.dashboard_monthly)));
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.dashboard_all)));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (menuExpand.isExpanded())
                    menuExpand.collapse(false);

                boolean isAll = tab.getText().equals(r.s(R.string.dashboard_all));
                graphModeLayout.setVisibility(isAll ? View.GONE : View.VISIBLE);
                textModeLayout.setVisibility(isAll ? View.VISIBLE : View.GONE);
                refreshLIst();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        countText1 = fv(R.id.text_total_count);
        countText2 = fv(R.id.text_total_count2);
        sizeText1 = fv(R.id.text_total_size);
        sizeText2 = fv(R.id.text_total_size2);
        sizeSubText1 = fv(R.id.text_total_size_sub);
        sizeSubText2 = fv(R.id.text_total_size_sub2);
        timeText = fv(R.id.text_total_time);

        graph = fv(R.id.graph);
        series = new LineGraphSeries<>();
        series.setTitle("Dashboard Item");
        series.setDrawBackground(true);
        series.setBackgroundColor(r.c(this, R.color.red_1_10));
        series.setColor(r.c(this, R.color.red1_100));
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(12);
        graph.addSeries(series);

        allItems = new ArrayList<>();
        filterItems = new ArrayList<>();
        graphItems = new ArrayList<>();

        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        long availableBlocks = stat.getAvailableBlocks();

        long total = blockSize * totalBlocks;
        long available = blockSize * availableBlocks;
        long used = total - available;

        totalMemorySizeProgress = fv(R.id.progress_down_total);
        totalMemorySizeProgress.setMax((int) (total / 10000));
        totalMemorySizeProgress.setProgress((int) (used / 10000));
        totalMemorySizeText = fv(R.id.text_down_total);
        totalMemorySizeText.setText(FileUtil.convertToStringRepresentation((total - available)) + " / " + FileUtil.convertToStringRepresentation(total));


    }

    private void visibleMenu(boolean show) {
        if (show)
            menuExpand.expand(true);
        else
            menuExpand.collapse(true);
        closeMenuView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void refreshLIst() {
        int count = 0, count1 = 0;
        long size = 0, time = 0;
        long size1 = 0;

        allItems.clear();
        filterItems.clear();
        graphItems.clear();

        long current = System.currentTimeMillis();
        String today = Util.getDateString(current);
        long compare = 1000 * 60 * 60 * 24;


        int max = 0;
        allItems.addAll(db.getDashboardItems());
        for (DashboardItem item : allItems) {

            String hour = item.getDashHour();
            String month = item.getDashMonth();
            String day = item.getDashDay();

            count += item.getDashCount();
            size += item.getDashSize();
            time += item.getDashTime();

            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    if (item.getDashDate().equalsIgnoreCase(today)) {
                        filterItems.add(item);
                        int idx = Integer.parseInt(item.getDashHour());

                        if (idx == 24)
                            idx = 0;

                        boolean include = false;
                        DashGraphItem dItem = new DashGraphItem();
                        dItem.setIndex(idx);
                        for (int i = 0; i < graphItems.size(); i++) {
                            if (graphItems.get(i).getIndex() == idx) {
                                dItem = graphItems.get(i);
                                include = true;
                                break;
                            }
                        }

                        if (dItem != null) {
                            dItem.setSize(dItem.size + item.getDashSize());
                            dItem.setCount(dItem.count + item.getDashCount());
                        }

                        if (!include)
                            graphItems.add(dItem);
                    }
                    break;

                case 1:
                    long limit = current - (compare * 7);
                    String dayily[] = new String[]{
                            Util.getDay(current - (compare * 6)),
                            Util.getDay(current - (compare * 5)),
                            Util.getDay(current - (compare * 4)),
                            Util.getDay(current - (compare * 3)),
                            Util.getDay(current - (compare * 2)),
                            Util.getDay(current - compare),
                            Util.getDay(current)
                    };
                    if (item.getDashAt() > limit) {
                        filterItems.add(item);
                        for (int i = 0; i < dayily.length; i++) {
                            if (item.getDashDay().equalsIgnoreCase(dayily[i])) {
                                boolean include = false;
                                DashGraphItem dItem = new DashGraphItem();
                                dItem.setIndex(i);
                                for (int j = 0; j < graphItems.size(); j++) {
                                    if (graphItems.get(j).getIndex() == i) {
                                        dItem = graphItems.get(j);
                                        include = true;
                                        break;
                                    }
                                }
                                if (dItem != null) {
                                    dItem.setSize(dItem.size + item.getDashSize());
                                    dItem.setCount(dItem.count + item.getDashCount());
                                }

                                if (!include)
                                    graphItems.add(dItem);
                                break;
                            }
                        }
                    }

                    break;

                case 2:
                    String monthly[] = new String[]{
                            "Jan, 2017", "Feb, 2017", "Mar, 2017", "Apr, 2017", "May, 2017", "Jun, 2017", "Jul, 2017", "Aug, 2017", "Sep, 2017", "Oct, 2017", "Nov, 2017", "Dec, 2017",
                    };
                    limit = current - (compare * 365);
                    if (item.getDashAt() > limit) {
                        filterItems.add(item);
                        for (int i = 0; i < monthly.length; i++) {
                            if (item.getDashMonth().equalsIgnoreCase(monthly[i])) {
                                boolean include = false;
                                DashGraphItem dItem = new DashGraphItem();
                                dItem.setIndex(i);
                                for (int j = 0; j < graphItems.size(); j++) {
                                    if (graphItems.get(j).getIndex() == i) {
                                        dItem = graphItems.get(j);
                                        include = true;
                                        break;
                                    }
                                }
                                if (dItem != null) {
                                    dItem.setSize(dItem.size + item.getDashSize());
                                    dItem.setCount(dItem.count + item.getDashCount());
                                }

                                if (!include)
                                    graphItems.add(dItem);
                                break;
                            }
                        }
                    }
                    break;

                case 3:
                    break;
            }
        }
        countText2.setText("" + count);
        sizeText2.setText(FileUtil.dashboardSize(size));
        sizeSubText2.setText(FileUtil.dashboardSizeUnit(size));

        if (tabLayout.getSelectedTabPosition() != 3)
            time = 0;
        for (DashboardItem item : filterItems) {
            count1 += item.getDashCount();
            size1 += item.getDashSize();
            time += item.getDashTime();
        }

        countText1.setText("" + count1);
        sizeText1.setText(FileUtil.dashboardSize(size1));
        sizeSubText1.setText(FileUtil.dashboardSizeUnit(size1));
        timeText.setText(Util.convertSecondsToHMmSs(time));

        switch (tabLayout.getSelectedTabPosition()) {
            case 0:
                String hh[] = new String[]{
                        "1", "", "", "3", "", "", "6", "", "", "9", "", "", "12", "", "", "15", "", "", "18", "", "", "21", "", "", "24"
                };
                StaticLabelsFormatter hformatter = new StaticLabelsFormatter(graph);
                hformatter.setHorizontalLabels(hh);
                graph.getGridLabelRenderer().setLabelFormatter(hformatter);
                graph.getViewport().setMinX(0);
                graph.getViewport().setMaxX(24);
                break;

            case 1:
                String dd[] = new String[]{
                        Util.getDay(current - (compare * 6)),
                        "",
                        "",
                        Util.getDay(current - (compare * 3)),
                        "",
                        "",
                        Util.getDay(current)
                };
                StaticLabelsFormatter dformatter = new StaticLabelsFormatter(graph);
                dformatter.setHorizontalLabels(dd);
                graph.getGridLabelRenderer().setLabelFormatter(dformatter);
                graph.getViewport().setMinX(0);
                graph.getViewport().setMaxX(6);
                break;

            case 2:
                String mm[] = new String[]{
                        "", "", "Mar", "", "", "Jun", "", "", "Sep", "", "", "Dec"
                };
                StaticLabelsFormatter mformatter = new StaticLabelsFormatter(graph);
                mformatter.setHorizontalLabels(mm);
                graph.getGridLabelRenderer().setLabelFormatter(mformatter);
                graph.getViewport().setMinX(0);
                graph.getViewport().setMaxX(11);
                break;
        }


        graph.getViewport().setXAxisBoundsManual(true);
        series.resetData(generateData());
    }

    private DataPoint[] generateData() {
        DataPoint[] result = new DataPoint[graphItems.size()];
        int index = 0;
        for (DashGraphItem item : graphItems) {
            DataPoint dp = new DataPoint(item.getIndex(), FileUtil.formatForGraph(item.getSize(), M));
            result[index] = dp;
            index++;
        }
        return result;
    }

    private void initAds() {
//        mContentView = fv(R.id.adView_native_main);
//        NativeAdFactory.addListener(new NativeAdFactory.NativeAdListener() {
//            @Override
//            public void onSuccess(String s, com.mopub.nativeads.NativeAd nativeAd) {
//                if (Common.ADX_DASH_NATIVE.equals(s)) {
//                    mNativeAd = nativeAd;
//                    mAdView = NativeAdFactory.getNativeAdView(DashboardActivity.this, Common.ADX_DASH_NATIVE, mContentView, null);
//                    mContentView.addView(mAdView);
//                }
//            }
//
//            @Override
//            public void onFailure(String s) {
//
//            }
//        });
//        NativeAdFactory.loadAd(Common.ADX_DASH_NATIVE);
//        nativeAdsLayout = fv(R.id.layout_native_ads);
//        fanNativeAd = new NativeAd(this, Common.FAN_DASH_NATIVE);
//        fanNativeAd.setAdListener(new com.facebook.ads.AdListener() {
//            @Override
//            public void onError(Ad ad, AdError adError) {
//                LogUtil.log("fanNativeAdDash onError()" + adError.getErrorMessage().toString());
//                if (fanNativeAd != null)
//                    fanNativeAd.destroy();
//
//                nativeAdsLayout.removeAllViews();
//
//                // Ad error callback
//                onLoadAdmobAdvanced();
////                NativeExpressAdView nativeExpressAdView = new NativeExpressAdView(DashboardActivity.this);
////                nativeExpressAdView.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_NATIVE_MID_TEST : Common.ADMOB_DASH_NATIVE_MID);
////                nativeExpressAdView.setAdSize(new AdSize(360,132));
////                nativeExpressAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
////                    @Override
////                    public void onAdOpened() {
////                        LogUtil.log("fanNativeAdDash onAdOpened()");
////                        super.onAdOpened();
////                    }
////
////                    @Override
////                    public void onAdLoaded() {
////                        LogUtil.log("fanNativeAdDash onAdLoaded()");
////                        super.onAdLoaded();
////                    }
////
////                    @Override
////                    public void onAdFailedToLoad(int i) {
////                        LogUtil.log("fanNativeAdDash onAdFailedToLoad()" + i);
////                        super.onAdFailedToLoad(i);
////                    }
////                });
////
////                nativeExpressAdView.loadAd(new AdRequest.Builder().build());
////                nativeAdsLayout.addView(nativeExpressAdView);
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                LogUtil.log("fanNativeAdDash onAdLoaded()");
//                if (fanNativeAd.isAdLoaded()) {
//                    fanNativeAd.unregisterView();
//                }
//
//                // Add the Ad view into the ad container.
//                LayoutInflater inflater = LayoutInflater.from(DashboardActivity.this);
//                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
//                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.fan_native_dash, nativeAdsLayout, false);
//                nativeAdsLayout.addView(adView);
//
//                // Create native UI using the ad metadata.
//                ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
//                TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
//                MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
//                //TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
//                Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);
//
//                // Set the Text.
//                nativeAdTitle.setText(fanNativeAd.getAdTitle());
//                //nativeAdBody.setText(nativeAd.getAdBody());
//                nativeAdCallToAction.setText(fanNativeAd.getAdCallToAction());
//
//                // Download and display the ad icon.
//                NativeAd.Image adIcon = fanNativeAd.getAdIcon();
//                NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);
//
//                // Download and display the cover image.
//                nativeAdMedia.setNativeAd(fanNativeAd);
//
//                // Add the AdChoices icon
//                LinearLayout adChoicesContainer = fv(R.id.ad_choices_container);
//                AdChoicesView adChoicesView = new AdChoicesView(DashboardActivity.this, fanNativeAd, true);
//                adChoicesContainer.addView(adChoicesView);
//
//                // Register the Title and CTA button to listen for clicks.
//                List<View> clickableViews = new ArrayList<>();
//                clickableViews.add(nativeAdTitle);
//                clickableViews.add(nativeAdCallToAction);
//                fanNativeAd.registerViewForInteraction(nativeAdsLayout, clickableViews);
//
//
//            }
//
//
//            @Override
//            public void onAdClicked(Ad ad) {
//                // Ad clicked callback
//                LogUtil.log("fanNativeAdDash onAdClicked()");
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//                LogUtil.log("fanNativeAdDash onLoggingImpression()");
//            }
//        });
//        fanNativeAd.loadAd(NativeAd.MediaCacheFlag.ALL);
    }

    private void onLoadAdmobAdvanced() {
        AdLoader adLoader = new AdLoader.Builder(this, LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_NATIVE_ADVANCED_TEST : Common.ADMOB_DASH_NATIVE_ADVANCED)
                .forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
                    @Override
                    public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                        if (nativeAppInstallAd == null || getLayoutInflater() == null)
                            return;
                        NativeAppInstallAdView adView = (NativeAppInstallAdView) getLayoutInflater().inflate(R.layout.admob_app_install_dash, null);
                        populateAppInstallAdView(nativeAppInstallAd, adView);
                        nativeAdsLayout.removeAllViews();
                        ;
                        nativeAdsLayout.addView(adView);
                    }
                })
                .forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
                    @Override
                    public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                        if (nativeContentAd == null || getLayoutInflater() == null)
                            return;

                        NativeContentAdView adView = (NativeContentAdView) getLayoutInflater()
                                .inflate(R.layout.admob_content_dash, null);
                        populateContentAdView(nativeContentAd, adView);
                        nativeAdsLayout.removeAllViews();
                        nativeAdsLayout.addView(adView);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder().build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAppInstallAd.getVideoController();

        // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
        // VideoController will call methods on this object when events occur in the video
        // lifecycle.
//        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
//            public void onVideoEnd() {
//                // Publishers should allow native ads to complete video playback before refreshing
//                // or replacing them with another ad in the same UI location.
//                mRefresh.setEnabled(true);
//                mVideoStatus.setText("Video status: Video playback has ended.");
//                super.onVideoEnd();
//            }
//        });

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
//        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));
//        adView.setImageView(adView.findViewById(R.id.bg_image));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());
//        ((ImageView) adView.getImageView()).setImageDrawable(nativeAppInstallAd.getImages().get(0).getDrawable());

        // Apps can check the VideoController's hasVideoContent property to determine if the
        // NativeAppInstallAd has a video asset.
//        if (vc.hasVideoContent()) {
//            mVideoStatus.setText(String.format(Locale.getDefault(),
//                    "Video status: Ad contains a %.2f:1 video asset.",
//                    vc.getAspectRatio()));
//        } else {
//            mRefresh.setEnabled(true);
//            mVideoStatus.setText("Video status: Ad does not contain a video asset.");
//        }

        // These assets aren't guaranteed to be in every NativeAppInstallAd, so it's important to
        // check before trying to display them.
//        if (nativeAppInstallAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAppInstallAd.getPrice());
//        }

//        if (nativeAppInstallAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAppInstallAd.getStore());
//        }

//        if (nativeAppInstallAd.getStarRating() == null) {
//            adView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) adView.getStarRatingView())
//                    .setRating(nativeAppInstallAd.getStarRating().floatValue());
//            adView.getStarRatingView().setVisibility(View.VISIBLE);
//        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        if (nativeContentAd.getImages().size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    private class DashGraphItem {
        private String label;
        private int index;
        private long size = 0;
        private int count = 0;

        public DashGraphItem() {

        }

        public DashGraphItem(int index, String label, long size, int count) {
            this.label = label;
            this.index = index;
            this.size = size;
            this.count = count;
        }

        public String getLabel() {
            return this.label;
        }

        public DashGraphItem setLabel(String label) {
            this.label = label;

            return this;
        }

        public int getIndex() {
            return index;
        }

        public DashGraphItem setIndex(int index) {
            this.index = index;

            return this;
        }

        public long getSize() {
            return this.size;
        }

        public DashGraphItem setSize(long size) {
            this.size = size;

            return this;
        }

        public int getCount() {
            return this.count;
        }

        public DashGraphItem setCount(int count) {
            this.count = count;

            return this;
        }
    }

}
