package io.jmobile.browser.ui;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.ui.base.ExpandLayout;
import io.jmobile.browser.ui.dialog.EditBookmarkDialog;
import io.jmobile.browser.ui.fragment.FromBookmarkFragment;
import io.jmobile.browser.ui.fragment.RecommendFragment;
import io.jmobile.browser.utils.image.ImageUtil;

public class QuickDialActivity extends BaseActivity implements View.OnClickListener {

    ImageButton backButton, menuButton, homeButton, addUrlButton;
    TextView titleText, toastText;
    ExpandLayout toastExpand;
    private final Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (toastExpand.isExpanded())
                toastExpand.collapse(true);
        }
    };
    Button undoButton;
    View closeToastView;
    FromBookmarkFragment bookmarkFragment;
    RecommendFragment recommendFragment;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private BookmarkItem curBookmark = null;
    private QuickDialItem curQickDial = null;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_dial);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(r.c(this, R.color.set_title_background));
        }

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_top_add_url:
                if (checkQuickDialLimit())
                    showAddUrlDialog();
                else
                    showMessageToast(r.s(R.string.toast_message_limit_quick));
                break;

            case R.id.button_undo:
//                visibleToast(false);
                deleteQuickDial();
                break;
        }
    }

    private void initView() {
        titleText = fv(R.id.text_top_title);
        titleText.setText(r.s(R.string.title_quick_dial));

        backButton = fv(R.id.btn_top_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        menuButton = fv(R.id.btn_top_menu);
        menuButton.setVisibility(View.GONE);

        homeButton = fv(R.id.btn_top_hdfm);
        homeButton.setVisibility(View.GONE);

        addUrlButton = fv(R.id.btn_top_add_url);
        addUrlButton.setVisibility(View.VISIBLE);
        addUrlButton.setOnClickListener(this);

        toastExpand = fv(R.id.layout_expend_toast);
        toastExpand.collapse(false);
        toastText = fv(R.id.text_toast);

        closeToastView = fv(R.id.view_close_toast);
        closeToastView.setVisibility(View.GONE);
        closeToastView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                visibleToast(false);
                curQickDial = null;
                curBookmark = null;
            }
        });

        undoButton = fv(R.id.button_undo);
        undoButton.setOnClickListener(this);

        viewPager = fv(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = fv(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        recommendFragment = new RecommendFragment();
        recommendFragment.setListener(new RecommendFragment.RecommendFragmentListener() {
            @Override
            public void onBookmarkItemClick(BookmarkItem item) {
                curBookmark = item;
                curQickDial = null;
                if (item.isBookmarkShowing()) {
//                    visibleToast(true);
                    addQuickDial();
                } else
                    deleteQuickDial();
            }

            @Override
            public void onShowToast(String message) {
                showMessageToast(message);
            }
        });
        bookmarkFragment = new FromBookmarkFragment();
        bookmarkFragment.setListener(new FromBookmarkFragment.BookmarksFragmentListener() {
            @Override
            public void onBookmarkItemClick(BookmarkItem item) {
                curBookmark = item;
                curQickDial = null;
                if (item.isBookmarkShowing()) {
//                    visibleToast(true);
                    addQuickDial();
                } else
                    deleteQuickDial();
            }

            @Override
            public void onShowToast(String message) {
                showMessageToast(message);
            }
        });
        adapter.addFrag(recommendFragment, getString(R.string.title_tab_recommended));
        adapter.addFrag(bookmarkFragment, getString(R.string.title_tab_from_bookmark));
        viewPager.setAdapter(adapter);
    }

//    private void visibleToast(boolean show) {
//        if (show) {
//            toastText.setText(r.s(R.string.toast_message_added_quick));
//            undoButton.setVisibility(View.VISIBLE);
//            toastExpand.expand(true);
//        }
//        else
//            toastExpand.collapse(true);
//        closeToastView.setVisibility(show ? View.VISIBLE : View.GONE);
//    }

    private void showAddUrlDialog() {
        QuickDialItem item = new QuickDialItem();
        item.setQuickDialId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setQuickDialShowing(false);
        item.setQuickDialOrder((int) db.getNextDBId(QuickDialItem.TABLE_NAME));
        item.setQuickDialAt(System.currentTimeMillis());
        item.setQuickDialFrom(QuickDialItem.ADD_NEW);
        if (fdf(EditBookmarkDialog.TAG) == null) {
            EditBookmarkDialog dlg = EditBookmarkDialog.newInstance(EditBookmarkDialog.TAG, this, item);
            sdf(dlg);
            dlg.setListener(new EditBookmarkDialog.EditBookmarkDialogListener() {
                @Override
                public void onSaveButtonClick(BookmarkItem item) {

                }

                @Override
                public void onSaveButtonClick(QuickDialItem item) {
                    new AddQuickDialItem().execute(item);
                }

                @Override
                public void onCancelButtonClick() {

                }
            });
        }
    }

    private boolean checkQuickDialLimit() {
        return db.getQuickDialItems() == null || db.getQuickDialItems().size() < QuickDialItem.MAX_SHOWING;
    }

    private void showMessageToast(String message) {
        toastText.setText(message);
        toastExpand.expand(true);
        toastHandler.removeCallbacksAndMessages(null);
        toastHandler.sendEmptyMessageDelayed(0, 1500);
        undoButton.setVisibility(View.GONE);
//        if (fdf(MessageDialog.TAG) == null) {
//            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//            dlg.setMessage("No more Quick Dial can be added.");
//            dlg.setPositiveLabel(r.s(R.string.ok));
//            sdf(dlg);
//        }
    }

    private void addQuickDial() {
        QuickDialItem item = new QuickDialItem();
        item.setQuickDialId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setQuickDialShowing(false);
        item.setQuickDialOrder((int) db.getNextDBId(QuickDialItem.TABLE_NAME));
        item.setQuickDialAt(System.currentTimeMillis());
        item.setQuickDialFrom(tabLayout.getSelectedTabPosition() == 0 ? QuickDialItem.ADD_RECOMMEND : QuickDialItem.ADD_BOOKMARK);
        item.setQuickDialFromId(curBookmark.getBookmarkId());
        item.setQuickDialIcon(curBookmark.getBookmarkIcon());
        item.setQuickDialName(curBookmark.getBookmarkName());
        item.setquickUrl(curBookmark.getBookmarkUrl());
        db.insertOrUpdateQuickDialItem(item);

        showMessageToast(r.s(R.string.toast_message_added_quick));
    }

    private void deleteQuickDial() {
        if (curQickDial == null && curBookmark == null)
            return;

        if (curBookmark != null) {
            db.deleteQuickDialItem(curBookmark);

            if (tabLayout.getSelectedTabPosition() == 0) {
                if (recommendFragment != null)
                    recommendFragment.initRecommendList();
            } else {
                curBookmark.setBookmarkShowing(false);
                db.updateBookmarkShowing(curBookmark);
                if (bookmarkFragment != null)
                    bookmarkFragment.refreshLIst();
            }
        } else if (curQickDial != null) {
            db.deleteQuickDialItem(curQickDial);
        }

        curQickDial = null;
        curBookmark = null;

        showMessageToast(r.s(R.string.toast_message_deleted_quick));
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    class AddQuickDialItem extends AsyncTask<QuickDialItem, Void, QuickDialItem> {
        @Override
        protected QuickDialItem doInBackground(QuickDialItem... params) {
            QuickDialItem item = params[0];
            String path = item.getQuickDialUrl();
            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);

            if (bitmap != null)
                item.setQuickDialIcon(ImageUtil.getImageBytes(bitmap));

            return item;
        }

        @Override
        protected void onPostExecute(QuickDialItem quickDialItem) {
            db.insertOrUpdateQuickDialItem(quickDialItem);

            curQickDial = quickDialItem;
            curBookmark = null;
            showMessageToast(r.s(R.string.toast_message_added_quick));
        }
    }
}
