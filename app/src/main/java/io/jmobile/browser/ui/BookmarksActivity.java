package io.jmobile.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.ui.adapter.BookmarkAdapter;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.ExpandLayout;
import io.jmobile.browser.ui.dialog.ClearHistoryDialog;
import io.jmobile.browser.ui.dialog.EditBookmarkDialog;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.ui.fragment.BookmarksFragment;
import io.jmobile.browser.ui.fragment.HistoryFragment;
import io.jmobile.browser.ui.fragment.ReadingFragment;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.image.ImageUtil;

public class BookmarksActivity extends BaseActivity implements View.OnClickListener,
        BookmarksFragment.BookmarksFragmentListener, ReadingFragment.ReadingFragmentListener, HistoryFragment.HistoryFragmentListener {

    ImageButton backButton, menuButton, homeButton, closeButton, delButton, menuCloseButton;
    TextView titleText, delCountText;
    ExpandLayout menuExpand, toastExpand;
    View closeMenuView;
    Button undoButton;
    Button selDelButton, allDelButton, historyClearButton, editButton, addBookmarkButton, delReadButton;

    BookmarksFragment bookmarksFragment;
    ReadingFragment readingFragment;
    HistoryFragment historyFragment;
    LinearLayout normalLayout, delLayout;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(r.c(this, R.color.set_title_background));
        }

        initView();
        if (tabLayout != null) {
            int tab = getIntent().getIntExtra(Common.FRAG_INTENT_SELECTED_TAB, 0);
            tabLayout.getTabAt(tab).select();
            setMenuVisible(tab);
        }

        if (getIntent() != null) {
            boolean quickMode = getIntent().getBooleanExtra(Common.FRAG_INTENT_ADD_QUICK_DIAL, false);
            if (quickMode) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Common.FRAG_INTENT_ADD_QUICK_DIAL, true);
                bookmarksFragment.setArguments(bundle);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (menuExpand != null && menuExpand.isExpanded())
            visibleMenu(false);
        else
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_top_menu:
                visibleMenu(true);
                menuExpand.expand(true);
                break;

            case R.id.btn_menu_close:
                visibleMenu(false);
                break;

            case R.id.btn_menu_add_to_bookmark:
                visibleMenu(false);
                if (fdf(EditBookmarkDialog.TAG) == null) {
                    EditBookmarkDialog dlg = EditBookmarkDialog.newInstance(EditBookmarkDialog.TAG, this, (BookmarkItem) null);
                    sdf(dlg);
                    dlg.setListener(new EditBookmarkDialog.EditBookmarkDialogListener() {
                        @Override
                        public void onSaveButtonClick(BookmarkItem item) {
                            new AddNewBookmarkItem().execute(item);
                        }

                        @Override
                        public void onSaveButtonClick(QuickDialItem item) {

                        }

                        @Override
                        public void onCancelButtonClick() {

                        }
                    });
                }
                break;

            case R.id.btn_menu_edit:
                visibleMenu(false);
                setEditMode(true);
                break;

            case R.id.button_undo:
                break;

            case R.id.btn_menu_del_read:
                visibleMenu(false);
                deleteReadItems();
                break;

            case R.id.btn_menu_del_history:
                visibleMenu(false);
                if (fdf(ClearHistoryDialog.TAG) == null) {
                    ClearHistoryDialog dlg = ClearHistoryDialog.newInstance(ClearHistoryDialog.TAG);
                    dlg.setListener(new ClearHistoryDialog.ClearHistoryDialogListener() {
                        @Override
                        public void onDeleteButtonClick(final int past) {

                            db.deleteHistory(past);
                            if (historyFragment != null)
                                historyFragment.refreshList();

                        }

                        @Override
                        public void onCancelButtonClick() {

                        }
                    });
                    sdf(dlg);
                }
                break;

            case R.id.btn_menu_del_sel:
                setDeleteMode(true);
                break;

            case R.id.btn_menu_del_all:
                visibleMenu(false);
                deleteAllItems();
                break;

            case R.id.btn_top_close:
                if (delButton.getVisibility() == View.GONE)
                    setEditMode(false);
                else
                    setDeleteMode(false);
                break;

            case R.id.btn_top_delete:
                deleteItems();
                break;
        }
    }

    @Override
    public void notiEmptyBookmarkList(boolean empty) {
        if (tabLayout.getSelectedTabPosition() != 0)
            return;

        if (empty)
            setDeleteMode(false);
        menuButton.setVisibility(View.VISIBLE);
        editButton.setVisibility(empty ? View.GONE : View.VISIBLE);
        selDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
        allDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
    }

    @Override
    public void notiEmptyReadingList(boolean empty) {
        if (tabLayout.getSelectedTabPosition() != 1)
            return;

        if (empty)
            setDeleteMode(false);
        menuButton.setVisibility(empty ? View.GONE : View.VISIBLE);
    }

    @Override
    public void notiEmptyHistoryList(boolean empty) {
        if (tabLayout.getSelectedTabPosition() != 2)
            return;

        menuButton.setVisibility(empty ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCheckChanged(int count) {
        if (count <= 0)
            delCountText.setText("");
        else
            delCountText.setText(count + " " + r.s(R.string.del_selected_title));
    }

    @Override
    public void onReadingCheckChanged(int count) {
        if (count <= 0)
            delCountText.setText("");
        else
            delCountText.setText(count + " " + r.s(R.string.del_selected_title));
    }

    @Override
    public void onReadingItemClick(String url) {
        Intent intent = new Intent(BookmarksActivity.this, FrontActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Common.INTENT_GO_TO_URL, url);
        startActivity(intent);

        finish();
    }

    @Override
    public void onBookmarkItemClick(String url) {
        Intent intent = new Intent(BookmarksActivity.this, FrontActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Common.INTENT_GO_TO_URL, url);
        startActivity(intent);

        finish();
    }

    @Override
    public void onHistoryItemClick(String url) {
        Intent intent = new Intent(BookmarksActivity.this, FrontActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Common.INTENT_GO_TO_URL, url);
        startActivity(intent);

        finish();
    }

    private void initView() {
        titleText = fv(R.id.text_top_title);
        titleText.setText(r.s(R.string.action_bookmarks));

        backButton = fv(R.id.btn_top_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        menuButton = fv(R.id.btn_top_menu);
        menuButton.setOnClickListener(this);

        homeButton = fv(R.id.btn_top_hdfm);
        homeButton.setVisibility(View.GONE);

        menuExpand = fv(R.id.layout_expend_menu);
        menuExpand.collapse(false);

        closeMenuView = fv(R.id.view_close_menu);
        closeMenuView.setVisibility(View.GONE);
        closeMenuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
            }
        });

        toastExpand = fv(R.id.layout_expend_toast);
        toastExpand.expand(true);

        undoButton = fv(R.id.button_undo);
        undoButton.setOnClickListener(this);
        closeButton = fv(R.id.btn_top_close);
        closeButton.setOnClickListener(this);

        delCountText = fv(R.id.text_top_del_count);

        delButton = fv(R.id.btn_top_delete);
        delButton.setOnClickListener(this);

        menuCloseButton = fv(R.id.btn_menu_close);
        menuCloseButton.setOnClickListener(this);
        addBookmarkButton = fv(R.id.btn_menu_add_to_bookmark);
        addBookmarkButton.setOnClickListener(this);
        editButton = fv(R.id.btn_menu_edit);
        editButton.setOnClickListener(this);
        delReadButton = fv(R.id.btn_menu_del_read);
        delReadButton.setOnClickListener(this);
        selDelButton = fv(R.id.btn_menu_del_sel);
        selDelButton.setOnClickListener(this);
        allDelButton = fv(R.id.btn_menu_del_all);
        allDelButton.setOnClickListener(this);
        historyClearButton = fv(R.id.btn_menu_del_history);
        historyClearButton.setOnClickListener(this);
        historyClearButton.setVisibility(View.GONE);

        normalLayout = fv(R.id.layout_normal);
        normalLayout.setVisibility(View.VISIBLE);

        delLayout = fv(R.id.layout_delete);
        delLayout.setVisibility(View.GONE);

        viewPager = fv(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = fv(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                titleText.setText(tab.getText());

                setMenuVisible(tab.getPosition());
                if (menuExpand.isExpanded())
                    menuExpand.collapse(false);
                setDeleteMode(false);
                setEditMode(false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        bookmarksFragment = new BookmarksFragment();
        bookmarksFragment.setListener(this);
        readingFragment = new ReadingFragment();
        readingFragment.setListener(this);
        historyFragment = new HistoryFragment();
        historyFragment.setLIstener(this);
        adapter.addFrag(bookmarksFragment, getString(R.string.title_bookmarks));
        adapter.addFrag(readingFragment, getString(R.string.title_readinglist));
        adapter.addFrag(historyFragment, getString(R.string.title_history));
        viewPager.setAdapter(adapter);
    }

    private void setMenuVisible(int position) {
        boolean isBookmark = position == 0;
        boolean isReading = position == 1;
        boolean isHistory = position == 2;

        addBookmarkButton.setVisibility(isBookmark ? View.VISIBLE : View.GONE);
        editButton.setVisibility(isBookmark ? View.VISIBLE : View.GONE);
        delReadButton.setVisibility(isReading ? View.VISIBLE : View.GONE);
        selDelButton.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        allDelButton.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        historyClearButton.setVisibility(isHistory ? View.VISIBLE : View.GONE);

        if (isBookmark && bookmarksFragment != null)
            bookmarksFragment.refreshLIst();

        if (isReading && readingFragment != null)
            readingFragment.refreshList();

        if (isHistory && historyFragment != null)
            historyFragment.refreshList();
    }

    @SuppressLint("NewApi")
    private void setDeleteMode(boolean isMode) {
        normalLayout.setVisibility(isMode ? View.GONE : View.VISIBLE);
        delLayout.setVisibility(isMode ? View.VISIBLE : View.GONE);
        delLayout.setBackgroundColor(r.c(this, R.color.main_del));
        delCountText.setText("");
        delButton.setVisibility(View.VISIBLE);

        if (isMode) {
            menuExpand.collapse(true);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().setStatusBarColor(r.c(this, R.color.main_del));
            }
            tabLayout.setBackgroundColor(r.c(this, R.color.main_del));
            tabLayout.setTabTextColors(r.c(this, R.color.bookmarks_tab_del_text), r.c(this, R.color.white));

            if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_BOOKMARKS)
                bookmarksFragment.selectModeClick(true);
            else if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_READING)
                readingFragment.selectModeClick(true);
        } else {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().setStatusBarColor(r.c(this, R.color.set_title_background));
            }
            tabLayout.setBackgroundColor(r.c(this, R.color.set_title_background));
            tabLayout.setTabTextColors(r.c(this, R.color.bookmarks_tab_text), r.c(this, R.color.colorAccent));

            if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_BOOKMARKS)
                bookmarksFragment.selectModeClick(false);
            else if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_READING)
                readingFragment.selectModeClick(false);
        }
    }

    private void setEditMode(boolean mode) {
        normalLayout.setVisibility(mode ? View.GONE : viewPager.VISIBLE);
        delLayout.setVisibility(mode ? View.VISIBLE : View.GONE);
        delLayout.setBackgroundColor(r.c(this, R.color.set_title_background));
        delCountText.setText(r.s(R.string.sub_menu_edit));
        delButton.setVisibility(View.GONE);

        if (bookmarksFragment != null)
            bookmarksFragment.setMode(mode ? BookmarkAdapter.MODE_EDIT : BookmarkAdapter.MODE_NONE);
    }

    private void deleteAllItems() {
        if (fdf(Common.TAG_DIALOG_DELETE_ALL_ITEMS) == null) {
            MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_DELETE_ALL_ITEMS);
            dlg.setTitle(r.s(R.string.dlg_title_delete));
            String message = r.s(R.string.dlg_msg_delete_all_reading_list);
            if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_BOOKMARKS)
                message = r.s(R.string.dlg_msg_delete_all_bookmark);
            dlg.setMessage(message);
            dlg.setNegativeLabel(r.s(R.string.cancel));
            dlg.setPositiveLabel(r.s(R.string.delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_BOOKMARKS)
                        bookmarksFragment.deleteItems(true);
                    else if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_READING)
                        readingFragment.deleteItems(true);
                }
            });
            sdf(dlg);
        }
    }

    private void deleteItems() {
        if (delCountText.getText().toString().isEmpty()) {
            if (fdf("no_select") == null) {
                MessageDialog dlg = MessageDialog.newInstance("no_select");
                dlg.setTitle(r.s(R.string.dlg_title_delete));
                dlg.setMessage(r.s(R.string.dlg_msg_no_selected_item));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }
            return;
        }

        if (fdf("delete_selected_items") == null) {
            MessageDialog dlg = MessageDialog.newInstance("delete_selected_items");
            dlg.setTitle(r.s(R.string.dlg_title_delete));
            dlg.setMessage((r.s(R.string.dlg_msg_delete_item)));
            dlg.setNegativeLabel(r.s(R.string.cancel));
            dlg.setPositiveLabel(r.s(R.string.delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_BOOKMARKS)
                        bookmarksFragment.deleteItems(false);
                    else if (tabLayout.getSelectedTabPosition() == Common.ARG_TAB_READING)
                        readingFragment.deleteItems(false);
                }
            });
            sdf(dlg);
        }
    }

    private void deleteReadItems() {
        if (fdf("delete_read_items") == null) {
            MessageDialog dlg = MessageDialog.newInstance("delete_read_items");
            dlg.setTitle(r.s(R.string.dlg_title_delete));
            dlg.setMessage((r.s(R.string.dlg_msg_delete_read_item)));
            dlg.setNegativeLabel(r.s(R.string.cancel));
            dlg.setPositiveLabel(r.s(R.string.delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    db.deleteDoneReadingItem();
                    if (readingFragment != null)
                        readingFragment.refreshList();
                }
            });
            sdf(dlg);
        }
    }

    private void visibleMenu(boolean show) {
        if (show)
            menuExpand.expand(true);
        else
            menuExpand.collapse(true);
        closeMenuView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    class AddNewBookmarkItem extends AsyncTask<BookmarkItem, Void, BookmarkItem> {
        @Override
        protected BookmarkItem doInBackground(BookmarkItem... params) {
            BookmarkItem item = params[0];
            String path = item.getBookmarkUrl();
            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);

            if (bitmap != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));

            return item;
        }

        @Override
        protected void onPostExecute(BookmarkItem item) {
            db.insertOrUpdateBookmarkItem(item);
            if (bookmarksFragment != null)
                bookmarksFragment.refreshLIst();
        }
    }
}
