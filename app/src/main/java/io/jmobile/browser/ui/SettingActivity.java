package io.jmobile.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.ui.dialog.SettingsDialog;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.file.FileUtil;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    ImageButton backButton, homeButton, menuButton;

    TextView titleText, downFolderText, notiPopText;
    Button downDelButton, clearRecordButton;
    Switch useWifiSwitch, useNotiSwitch, usePwdSwitch;

    LinearLayout engineLayout, maxDownloadsLayout, timeFilterLayout;
    TextView enginText, maxDownloadText, timeFilterText;
    LinearLayout hdfmLayout, howLayout, moreLayout, feedbackLayout;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.set_title_background));
        }
        initView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_top_back:
                onBackPressed();
                break;

            case R.id.btn_del_down_folder:
                deleteDownloadFolder();
                break;

            case R.id.lay_btn_search_engine:
                if (fdf(SettingsDialog.TAG) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(r.sa(R.array.settings_search_engine)));
                    SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG,
                            r.s(R.string.settings_search_engine), list, sp.getSearchEngine());
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            int index = list.indexOf(tag);
                            if (index > -1)
                                sp.setSearchEngine(index);
                            enginText.setText(tag);
                        }
                    });
                    sdf(dlg);


                }
                break;

            case R.id.lay_btn_max_downloads:
                if (fdf(SettingsDialog.TAG) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(r.sa(R.array.settings_max_download)));
                    SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG,
                            r.s(R.string.settings_max_downloads), list, sp.getMaxDownloads());
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            int index = list.indexOf(tag);
                            if (index > -1)
                                sp.setMaxDownloads(index);
                            maxDownloadText.setText(tag);
                        }
                    });
                    sdf(dlg);


                }
                break;

            case R.id.lay_btn_use_time_filtering:
                if (fdf(SettingsDialog.TAG) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(r.sa(R.array.settings_time_filter)));
                    SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG,
                            r.s(R.string.settings_use_time_filtering), list, sp.getTimeFiltering());
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            int index = list.indexOf(tag);
                            if (index > -1)
                                sp.setTimeFiltering(index);
                            timeFilterText.setText(tag);
                        }
                    });
                    sdf(dlg);


                }
                break;

            case R.id.btn_clear_records:
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG)
                            .setMessage(r.s(R.string.dlg_del_app_records_msg))
                            .setNegativeLabel(r.s(R.string.cancel))
                            .setPositiveLabel(r.s(R.string.delete1));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            new ClearRecordTask().execute();
                        }
                    });
                    sdf(dlg);
                }
                break;

            case R.id.lay_btn_how:
                break;

            case R.id.lay_btn_more:
                try {
                    Uri uri = Uri.parse("https://play.google.com/store/apps/dev?id=6800756349887572109");
                    Intent i = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(i);
                } catch (Exception e) {
                }
                break;

            case R.id.lay_btn_hdfm:
                lauchHDF();
                break;

            case R.id.lay_btn_feedback:
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "jeffrey@jmobile.io"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Suggest to JM Browser"); //제목
                    startActivity(intent);
                } catch (Exception e) {
                }
                break;

            default:
                break;
        }
    }

    private void initView() {
        titleText = fv(R.id.text_top_title);
        titleText.setText(r.s(R.string.title_setting));
        backButton = fv(R.id.btn_top_back);
        backButton.setOnClickListener(this);
        homeButton = fv(R.id.btn_top_hdfm);
        homeButton.setVisibility(View.GONE);
        menuButton = fv(R.id.btn_top_menu);
        menuButton.setVisibility(View.GONE);

        downFolderText = fv(R.id.text_download_url);
        downFolderText.setText(Common.getVideoDir(this));
        downDelButton = fv(R.id.btn_del_down_folder);
        downDelButton.setOnClickListener(this);

        useWifiSwitch = fv(R.id.switch_use_cellular_data);
        useWifiSwitch.setChecked(sp.isWifiOnly());
        useWifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sp.setUseWifiOnly(isChecked);
            }
        });

        useNotiSwitch = fv(R.id.switch_notification);
        useNotiSwitch.setChecked(sp.isUseNotification());
        useNotiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sp.setUseNotification(isChecked);
            }
        });
        notiPopText = fv(R.id.text_noti_popup);
        notiPopText.setVisibility(TextUtils.isEmpty(notiPopText.getText().toString()) ? View.GONE : View.VISIBLE);

        usePwdSwitch = fv(R.id.switch_save_passwords);
        usePwdSwitch.setChecked(sp.isSavePasswords());
        usePwdSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sp.setSetUseSavePasswords(isChecked);
            }
        });

        engineLayout = fv(R.id.lay_btn_search_engine);
        engineLayout.setOnClickListener(this);
        enginText = fv(R.id.text_search_engine);
        enginText.setText(r.sa(R.array.settings_search_engine)[sp.getSearchEngine()]);
        maxDownloadsLayout = fv(R.id.lay_btn_max_downloads);
        maxDownloadsLayout.setOnClickListener(this);
        maxDownloadText = fv(R.id.text_max_downloads);
        maxDownloadText.setText(r.sa(R.array.settings_max_download)[sp.getMaxDownloads()]);
        timeFilterLayout = fv(R.id.lay_btn_use_time_filtering);
        timeFilterLayout.setOnClickListener(this);
        timeFilterText = fv(R.id.text_use_time_filtering);
        timeFilterText.setText(r.sa(R.array.settings_time_filter)[sp.getTimeFiltering()]);
        clearRecordButton = fv(R.id.btn_clear_records);
        clearRecordButton.setOnClickListener(this);

        hdfmLayout = fv(R.id.lay_btn_hdfm);
        hdfmLayout.setOnClickListener(this);
        howLayout = fv(R.id.lay_btn_how);
        howLayout.setOnClickListener(this);
        moreLayout = fv(R.id.lay_btn_more);
        moreLayout.setOnClickListener(this);
        feedbackLayout = fv(R.id.lay_btn_feedback);
        feedbackLayout.setOnClickListener(this);
    }

    private void deleteDownloadFolder() {
        if (fdf(Common.TAG_DIALOG_DELETE_DOWNLOAD_FOLDER) == null) {
            MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_DELETE_DOWNLOAD_FOLDER);
            dlg.setMessage(r.s(R.string.dlg_msg_delete_folder));
            dlg.setNegativeLabel(r.s(R.string.cancel));
            dlg.setPositiveLabel(r.s(R.string.delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    (new DeleteDownloadFilesTask()).execute();
                }
            });
            sdf(dlg);
        }
    }

    private void lauchHDF() {
        try {
            if (getPackageList()) {
                Intent intent = this.getPackageManager()
                        .getLaunchIntentForPackage("myfilemanager.jiran.com.myfilemanager");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("path", Common.getVideoDir(this));
                startActivity(intent);
            } else {
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=myfilemanager.jiran.com.myfilemanager");
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Log.e("jirana ", e.toString());
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=myfilemanager.jiran.com.myfilemanager");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    private void clearApplicationCache(File dir) {
        if (dir == null)
            dir = app.getCacheDir();

        if (dir == null)
            return;

        File[] child = dir.listFiles();
        try {
            for (int i = 0; i < child.length; i++) {
                if (child[i].isDirectory())
                    clearApplicationCache(child[i]);
                else
                    child[i].delete();
            }
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    public boolean getPackageList() {
        boolean isExist = false;

        PackageManager pkgMgr = this.getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith("myfilemanager.jiran.com.myfilemanager")) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    private class DeleteDownloadFilesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            FileUtil.delete(new File(Common.getVideoDir(SettingActivity.this)));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(SettingActivity.this, r.s(R.string.msg_delete_complete), Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }
    }

    class ClearRecordTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            clearApplicationCache(null);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(SettingActivity.this, r.s(R.string.msg_delete_complete), Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);

        }
    }
}
