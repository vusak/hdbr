package io.jmobile.browser.ui.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.data.FavoriteItem;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.image.ImageUtil;

public class FavoriteAdapter extends ReSelectableAdapter<FavoriteItem, FavoriteAdapter.ReHistoryHolder> {

    private Context context;
    private boolean deleteMode = false;

    public FavoriteAdapter(Context context, int layoutId, List<FavoriteItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);

        this.context = context;
        this.listener = listener;

    }

    @Override
    public void onBindViewHolder(ReHistoryHolder holder, int position) {
        FavoriteItem item = items.get(position);

        String url = item.getFavoriteUrl().replace("http://", "");
        url = url.replace("https://", "");
//        holder.title.setText(item.getFavoriteCount() + item.getFavoriteTitle());
        holder.title.setText(item.getFavoriteTitle());
        holder.title.setTextColor(ContextCompat.getColor(context, isDeleteMode() ? R.color.main_text_dim : R.color.gray7_100));
        holder.dim.setVisibility(isDeleteMode() ? View.VISIBLE : View.GONE);
        if (item.getFavoriteIcon() != null) {
            holder.icon.setImageBitmap(ImageUtil.getImage(item.getFavoriteIcon()));
            int padding = (int) Util.convertDpToPixel(2, context);
            holder.icon.setPadding(padding, padding, padding, padding);
        } else {
            holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_default_app_50));
            holder.icon.setPadding(0, 0, 0, 0);

        }
        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position)
                    listener.OnItemClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
    }

    @Override
    public ReHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReHistoryHolder holder = new ReHistoryHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.dim = holder.fv(R.id.view_dim);
        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        return holder;
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean mode) {
        this.deleteMode = mode;
    }

    public class ReHistoryHolder extends ReAbstractViewHolder {

        View dim;
        ImageView icon;
        TextView title;

        public ReHistoryHolder(View itemView) {
            super(itemView);
        }

    }
}
