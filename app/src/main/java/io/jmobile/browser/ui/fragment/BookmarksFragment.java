package io.jmobile.browser.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.ui.adapter.BookmarkAdapter;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.ui.dialog.EditBookmarkDialog;

public class BookmarksFragment extends BaseFragment {

    RecyclerView lv;
    LinearLayoutManager manager;
    BookmarkAdapter adapter;
    //    MoPubRecyclerAdapter adAdapter;
    ArrayList<BookmarkItem> list;
    LinearLayout emptyLayout;
    private BookmarksFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_bookmarks;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        emptyLayout = fv(R.id.layout_empty);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new BookmarkAdapter(getActivity(), R.layout.item_bookmark, list, new BookmarkAdapter.BookmarkAdapterListener() {
            @Override
            public void onEditBookmarkClick(final int position, BookmarkItem item) {
//                final int rePosition = adAdapter.getOriginalPosition(position);
//                item = adapter.getItem(rePosition);
                if (fdf(EditBookmarkDialog.TAG) == null) {
                    EditBookmarkDialog dlg = EditBookmarkDialog.newInstance(EditBookmarkDialog.TAG, getActivity(), item);
                    sdf(dlg);
                    dlg.setListener(new EditBookmarkDialog.EditBookmarkDialogListener() {
                        @Override
                        public void onSaveButtonClick(BookmarkItem item) {
                            db.insertOrUpdateBookmarkItem(item);
                            adapter.notifyItemChanged(position);
//                            adapter.notifyItemChanged(rePosition);
                        }

                        @Override
                        public void onSaveButtonClick(QuickDialItem item) {

                        }

                        @Override
                        public void onCancelButtonClick() {

                        }
                    });
                }
            }

            @Override
            public void OnItemClick(int position, BookmarkItem item) {
//                position = adAdapter.getOriginalPosition(position);
//                item = adapter.getItem(position);
                if (adapter.isCheckMode()) {
                    adapter.select(position);

                    if (listener != null)
                        listener.onCheckChanged(adapter.getSelectedList().size());
                } else if (adapter.getMode() == BookmarkAdapter.MODE_EDIT) {
                    return;
                } else {
                    if (listener != null)
                        listener.onBookmarkItemClick((item).getBookmarkUrl());
                }
            }

            @Override
            public void OnItemLongClick(int position, BookmarkItem item) {

            }
        });
        lv.setAdapter(adapter);
//        adAdapter = NativeAdFactory.getMoPubRecyclerAdapter(getActivity(), adapter, Common.ADX_LIST_BOOKMARK_NATIVE);
//        lv.setAdapter(adAdapter);
//        adAdapter.loadAds(Common.ADX_LIST_BOOKMARK_NATIVE);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        refreshLIst();
        super.onResume();
    }

    public void deleteItems(boolean isAll) {
        if (isAll)
            db.deleteAllBookmarkItems();
        else {
            for (BookmarkItem item : adapter.getSelectedList()) {
                db.deleteBookmarkItem(item);
            }
            adapter.clearSelection();
            listener.onCheckChanged(0);
        }

        refreshLIst();
    }

    public void refreshLIst() {
        if (list == null)
            return;

        list.clear();
        list.addAll(db.getBookmarkItems());
        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        emptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);

        if (listener != null)
            listener.notiEmptyBookmarkList(empty);
    }

    public void selectModeClick(boolean mode) {
        if (adapter != null)
            adapter.setCheckMode(mode);
    }

    public void setMode(int mode) {
        if (adapter != null)
            adapter.setMode(mode);

//        if (adAdapter == null)
//            return;
//        if (mode == BookmarkAdapter.MODE_EDIT) {
//            adAdapter.clearAds();
//        } else {
//            adAdapter.loadAds(Common.ADX_LIST_BOOKMARK_NATIVE);
//        }
    }

    public void setListener(BookmarksFragmentListener listener) {
        this.listener = listener;
    }

    private void moveItem(int oldPos, int newPos) {
        BookmarkItem item = list.get(oldPos);
        list.remove(oldPos);
        list.add(newPos, item);
        adapter.notifyItemMoved(oldPos, newPos);

        for (int i = 0; i < list.size(); i++) {
            item = list.get(i);
            item.setBookmarkOrder(i);
            db.updateBookmarkOrder(item);
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return adapter.getMode() == BookmarkAdapter.MODE_EDIT;
            }
        };

        return simpleCallback;
    }

    public static interface BookmarksFragmentListener {
        public void onCheckChanged(int count);

        public void onBookmarkItemClick(String url);

        public void notiEmptyBookmarkList(boolean empty);
    }
}
