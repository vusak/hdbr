package io.jmobile.browser.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.ui.adapter.BookmarkAdapter;
import io.jmobile.browser.ui.base.BaseFragment;

//import com.mopub.nativeads.MoPubRecyclerAdapter;
//import com.mopub.nativeads.NativeAdFactory;

public class FromBookmarkFragment extends BaseFragment {

    RecyclerView lv;
    LinearLayoutManager manager;
    BookmarkAdapter adapter;
    //    MoPubRecyclerAdapter adAdapter;
    ArrayList<BookmarkItem> list;
    LinearLayout emptyLayout;
    private BookmarksFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_bookmarks;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        emptyLayout = fv(R.id.layout_empty);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new BookmarkAdapter(getActivity(), R.layout.item_bookmark, list, new BookmarkAdapter.BookmarkAdapterListener() {
            @Override
            public void onEditBookmarkClick(final int position, BookmarkItem item) {
            }

            @Override
            public void OnItemClick(int position, BookmarkItem item) {
                if (!checkQuickDialLimit() && !item.isBookmarkShowing()) {
                    if (listener != null)
                        listener.onShowToast(r.s(R.string.toast_message_limit_quick));
                } else {
                    item.setBookmarkShowing(!item.isBookmarkShowing());
                    db.updateBookmarkShowing(item);
                    refreshLIst();

                    if (listener != null)
                        listener.onBookmarkItemClick(item);
                }
            }

            @Override
            public void OnItemLongClick(int position, BookmarkItem item) {

            }
        });
        adapter.setMode(BookmarkAdapter.MODE_ADD_QUICK);
        lv.setAdapter(adapter);
//        adAdapter = NativeAdFactory.getMoPubRecyclerAdapter(getActivity(), adapter, Common.ADX_LIST_QUICKMARK_NATIVE);
//        lv.setAdapter(adAdapter);
//        adAdapter.loadAds(Common.ADX_LIST_QUICKMARK_NATIVE);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        refreshLIst();
        super.onResume();
    }

    public void refreshLIst() {
        list.clear();
        list.addAll(db.getBookmarkItems());
        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        emptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);
    }

    public void selectModeClick(boolean mode) {
        if (adapter != null)
            adapter.setCheckMode(mode);
    }

    public void setMode(int mode) {
        if (adapter != null)
            adapter.setMode(mode);
    }

    public void setListener(BookmarksFragmentListener listener) {
        this.listener = listener;
    }

    private void moveItem(int oldPos, int newPos) {
        BookmarkItem item = list.get(oldPos);
        list.remove(oldPos);
        list.add(newPos, item);
        adapter.notifyItemMoved(oldPos, newPos);

        for (int i = 0; i < list.size(); i++) {
            item = list.get(i);
            item.setBookmarkOrder(i);
            db.updateBookmarkOrder(item);
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return adapter.getMode() == BookmarkAdapter.MODE_EDIT;
            }
        };

        return simpleCallback;
    }

    private boolean checkQuickDialLimit() {
        return db.getQuickDialItems() == null || db.getQuickDialItems().size() < QuickDialItem.MAX_SHOWING;
    }

    public static interface BookmarksFragmentListener {
        public void onBookmarkItemClick(BookmarkItem item);

        public void onShowToast(String message);
    }

//    private void showLimitMessage() {
//        if (fdf(MessageDialog.TAG) == null) {
//            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//            dlg.setMessage("No more Quick Dial can be added.");
//            dlg.setPositiveLabel(r.s(R.string.ok));
//            sdf(dlg);
//        }
//    }
}
