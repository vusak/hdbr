package io.jmobile.browser.ui.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.image.ImageUtil;

public class QuickDialAdapter extends ReSelectableAdapter<QuickDialItem, QuickDialAdapter.ReQuickDialHolder> {

    private Context context;
    private boolean deleteMode = false;
//    private final static int TYPE_AD = 0;
//    private final static int TYPE_CONTENT = 1;

    public QuickDialAdapter(Context context, int layoutId, List<QuickDialItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);

        this.context = context;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ReQuickDialHolder holder, int position) {
//        if (position % 9 != 3) {
        QuickDialItem item = items.get(position);

        if (item.getQuickDialUrl() != null && item.getQuickDialUrl().equalsIgnoreCase(QuickDialItem.ID_HOW_TO_USE))
            holder.title.setText(context.getString(R.string.settings_title_how));
        else
            holder.title.setText(item.getQuickDialName());
        holder.title.setTextColor(ContextCompat.getColor(context, isDeleteMode() ? R.color.main_text_dim : R.color.white));
        if (item.getQuickDialIcon() != null)
            holder.icon.setImageBitmap(ImageUtil.getImage(item.getQuickDialIcon()));
        else
            holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_default_app_50));

        if (item.getRowId() == -1 || item.getQuickDialFrom() == QuickDialItem.ADD_RECOMMEND || item.getQuickDialIcon() == null)
            holder.icon.setPadding(0, 0, 0, 0);
        else {
            int padding = (int) Util.convertDpToPixel(5, context);
            holder.icon.setPadding(padding, padding, padding, padding);
        }

        holder.deleteButton.setVisibility(deleteMode ? View.VISIBLE : View.GONE);
        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position)
                    listener.OnItemClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                setDeleteMode(true);
                if (listener != null && items.size() > position) {
                    listener.OnItemLongClick(position, items.get(position));
                }
                return true;
            }
        });
//        }
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position % 9 == 3)
//            return TYPE_AD;
//        else
//            return TYPE_CONTENT;
//    }

    @Override
    public ReQuickDialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReQuickDialHolder holder = new ReQuickDialHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

//        if (viewType == TYPE_AD) {
//            adview = new AdView(context);
//            adview.setAdSize(AdSize.BANNER);
//            adview.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_BANNER_OPT_TEST : Common.ADMOB_MAIN_BANNER_OPT);
//            float density = context.getResources().getDisplayMetrics().density;
//            int height = Math.round(AdSize.BANNER.getHeight() * density);
//            AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.FILL_PARENT, height);
//            adview.setLayoutParams(params);
//            AdRequest request = new AdRequest.Builder().build();
//            adview.loadAd(request);
//            holder = new ReQuickDialHolder(adview);
//        }
//        else {

        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        holder.deleteButton = holder.fv(R.id.button_del);
//        }
        return holder;
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean mode) {
        this.deleteMode = mode;
    }

    public class ReQuickDialHolder extends ReAbstractViewHolder {

        ImageView icon;
        TextView title;
        ImageButton deleteButton;

        public ReQuickDialHolder(View itemView) {
            super(itemView);
        }
    }


}
