package io.jmobile.browser.ui.base;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import io.jmobile.browser.BaseApplication;
import io.jmobile.browser.R;
import io.jmobile.browser.download.JDownloadManager;
import io.jmobile.browser.download.JNetworkMonitor;
import io.jmobile.browser.storage.DBController;
import io.jmobile.browser.storage.SPController;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;

/**
 * Created by Cecil on 2017-05-30.
 */

public class BaseActivity extends FragmentActivity {

    protected BaseApplication app;
    protected SPController sp;
    protected DBController db;
    protected BaseApplication.ResourceWrapper r;
    protected JDownloadManager downloadManager;
    protected JNetworkMonitor networkMonitor;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.set_title_background));
        }
        super.onCreate(savedInstanceState);

        app = (BaseApplication) getApplication();
        sp = app.getSPController();
        db = app.getDBController();
        r = app.getResourceWrapper();
        downloadManager = app.getDownloadManager();
        networkMonitor = app.getNetworkMonitor();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    protected <T extends BaseFragment> T addf(String tag, BaseFragment.BaseFragmentCreator<T> creator) {
        return Util.addf(getSupportFragmentManager(), tag, creator);
    }

    protected <T extends BaseFragment> T ff(String tag) {
        return Util.ff(getSupportFragmentManager(), tag);
    }

    protected <T extends BaseDialogFragment> T fdf(String tag) {
        return Util.fdf(getSupportFragmentManager(), tag);
    }

    protected void hdf(String tag) {
        Util.hdf(getSupportFragmentManager(), tag);
    }

    protected void sdf(BaseDialogFragment d) {
        Util.sdf(getSupportFragmentManager(), d);
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T fv(int id) {
        try {
            return (T) findViewById(id);
        } catch (ClassCastException e) {
            return null;
        }
    }

    protected void log(String msg) {
        LogUtil.log(getClass().getSimpleName(), msg);
    }

    protected void log(Throwable tr) {
        LogUtil.log(getClass().getSimpleName(), tr);
    }
}
