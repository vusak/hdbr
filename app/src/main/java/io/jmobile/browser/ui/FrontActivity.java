package io.jmobile.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BrowserItem;
import io.jmobile.browser.data.DashboardItem;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.data.FavoriteItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.download.JDownloadManager;
import io.jmobile.browser.ui.adapter.CarouselPagerAdapter;
import io.jmobile.browser.ui.adapter.FavoriteAdapter;
import io.jmobile.browser.ui.adapter.QuickDialAdapter;
import io.jmobile.browser.ui.adapter.ReAdapter;
import io.jmobile.browser.ui.adapter.UrlAdapter;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.ui.base.BaseClickableViewPager;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.ExpandLayout;
import io.jmobile.browser.ui.dialog.DownloadDialog;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.ui.fragment.BrowserFragment;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.file.FileUtil;
import io.jmobile.browser.utils.image.ImageUtil;

public class FrontActivity extends BaseActivity implements View.OnClickListener, BrowserFragment.BrowserFragmentListsner,
        JDownloadManager.DownloadListener {
    private final Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (toastLayout.isExpanded())
                toastLayout.collapse(true);
        }
    };

    //    MobVistaSDK mobVistaSDK;
    LinearLayout rootLayout, urlLayout, editModeLayout, newTabLayout;
    TextView urlButton;
    EditText urlEdit;
    TextView urlText, doneText, visitText, newText, bottomNewText;
    ImageButton urlClearButton;
    View editModeView, editModeView2, editModeView3;
    ImageButton editModeButton, editCloseButton, clearVisitButton;
    TextView emptyText;
    RecyclerView quickLv, mostlyLv;
    LinearLayoutManager quickManager, mostlyManager;
    QuickDialAdapter quickDialAdapter;
    ArrayList<QuickDialItem> quickList;
    FavoriteAdapter favoriteAdapter;
    ArrayList<FavoriteItem> favoriteList;
    //    MultiAutoCompleteTextView urlMultiText;
    ListView urlLV;
    UrlAdapter urlAdapter;
    ArrayList<String> urlList, favList;
    ExpandLayout submenuLayout, toastLayout;
    TextView toastText;
    View closeSubLayout;
    Button addBookmarkButton, addReadingButton, bookmarksButton, historyButton, readingButton;
    ImageButton prevButton, nextButton, shareButton, downloadButton, menuButton, homeButton, closeButton;
    LinearLayout settingButton, quitButton;
    FrameLayout downloadLayout;
    TextView downReadyCnt;
    //    BaseViewPager browserViewpager;
//    BrowserViewPagerAdapter browserViewAdapter;
    LinearLayout browserLayout;
    ArrayList<BrowserFragment> browserFragments;
    ArrayList<BrowserItem> browserList;
    BrowserFragment currBrowserFragment;
    //    RecyclerView browserLv;
//    LinearLayoutManager browserManager;
//    BrowserAdapter browserAdapter;
    CarouselPagerAdapter browserAdapter;
    BaseClickableViewPager pager;
    ImageButton clearAllButton, newButton;
    LinearLayout returnButton;
    TextView returnText;
    Bitmap homeBitmap = null;
    Button exitOKButton, exitCancelButton;
    LinearLayout exitLayout, exitAdLayout;
    FrameLayout nativeAdsLayout;
    NativeAd fanNativeAd;
    //    NativeAd mNativeAd;
//    MoPubView adExitSquare;
//    private View mAdView;
//    private LinearLayout mContentView;
    private Handler urlSearchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                urlList.clear();
                String keyword = urlEdit.getText().toString();
                for (String temp : favList) {
                    if (temp.contains(keyword))
                        urlList.add(temp);
                }
//                urlList = FileUtil.getSearcApihList(urlEdit.getText().toString());

                if (urlList == null)
                    urlList = new ArrayList<>();
                urlAdapter.setItems(urlList, keyword);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front);

        initView();
//        initAds();
//        initMobVista();
        downloadManager.addListener(this);

        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra(Common.INTENT_GO_TO_URL);
            if (!TextUtils.isEmpty(url)) {
                urlLayout.setVisibility(View.GONE);
                visibleNewTabLayout(false);
                visibleBrowser(true);

                Bundle bundle = new Bundle();
                bundle.putString(Common.INTENT_GO_TO_URL, url);
                currBrowserFragment.setArguments(bundle);
            }
        }

        if (sp.isFirstSetDashboard())
            new InitDashboardItem().execute();
    }

    @Override
    protected void onResume() {
        if (currBrowserFragment != null && browserLayout != null && browserLayout.getVisibility() == View.VISIBLE)
            currBrowserFragment.onResume();
        setCheckReadyDownloadItem();
        refreshMainList();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (currBrowserFragment != null)
            currBrowserFragment.onPause();

//        preLoadMobvista();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        adExitSquare.destroy();

    }

    @Override
    public void onBackPressed() {
        if (submenuLayout != null && submenuLayout.isExpanded())
            visibleSubmenu(false, true);
        else if (editModeLayout != null && editModeLayout.getVisibility() == View.VISIBLE) {
            setQuickEditMode(false);
        } else if (urlLayout != null && urlLayout.getVisibility() == View.VISIBLE) {
            urlLayout.setVisibility(View.GONE);
            if (currBrowserFragment != null)
                currBrowserFragment.onResume();

        } else if (newTabLayout != null && newTabLayout.getVisibility() == View.VISIBLE) {
            visibleNewTabLayout(false);
            if (currBrowserFragment != null)
                currBrowserFragment.onResume();
        } else if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE) {
            if (!currBrowserFragment.onPrevButtonClicked())
                visibleBrowser(false);
//                browserLayout.setVisibility(View.GONE);

        } else
            showExitDialog();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_done_edit_mode:
                setQuickEditMode(false);
                break;

            case R.id.button_mode_edit:
                setQuickEditMode(true);
                break;

            case R.id.button_mode_close:
                setQuickEditMode(false);
                break;

            case R.id.button_clear_visit:
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG)
                            .setMessage(r.s(R.string.dlg_del_visit_list_msg))
                            .setTitle(r.s(R.string.dlg_title_delete))
                            .setNegativeLabel(r.s(R.string.cancel))
                            .setPositiveLabel(r.s(R.string.delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            db.deleteAllFavoriteItem();
                            refreshMainList();
                        }
                    });
                    sdf(dlg);
                }

                break;

            case R.id.btn_menu_add_bookmark:
                visibleSubmenu(false, true);

                if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE
                        && urlLayout != null && urlLayout.getVisibility() == View.GONE) {
                    if (currBrowserFragment != null)
                        currBrowserFragment.addBookmarkItem();
                } else {
                    toastText.setText(r.s(R.string.toast_message_failed_bookmak));
                    toastLayout.expand(true);
                    toastHandler.sendEmptyMessageDelayed(0, 1500);
                }
                break;

            case R.id.btn_menu_add_reading:
                visibleSubmenu(false, true);

                if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE
                        && urlLayout != null && urlLayout.getVisibility() == View.GONE) {
                    if (currBrowserFragment != null)
                        currBrowserFragment.addReadingItem();
                } else {
                    toastText.setText(r.s(R.string.toast_message_failed_reading));
                    toastLayout.expand(true);
                    toastHandler.sendEmptyMessageDelayed(0, 1500);
                }
                break;

            case R.id.btn_menu_bookmarks:
                visibleSubmenu(false, true);
                Intent bIntent = new Intent(this, BookmarksActivity.class);
                bIntent.putExtra(Common.FRAG_INTENT_SELECTED_TAB, Common.ARG_TAB_BOOKMARKS);
                startActivity(bIntent);
                break;

            case R.id.btn_menu_reading:
                visibleSubmenu(false, true);
                Intent rIntent = new Intent(this, BookmarksActivity.class);
                rIntent.putExtra(Common.FRAG_INTENT_SELECTED_TAB, Common.ARG_TAB_READING);
                startActivity(rIntent);
                break;

            case R.id.btn_menu_history:
                visibleSubmenu(false, true);
                Intent hIntent = new Intent(this, BookmarksActivity.class);
                hIntent.putExtra(Common.FRAG_INTENT_SELECTED_TAB, Common.ARG_TAB_HISTORY);
                startActivity(hIntent);
                break;

//            case R.id.btn_menu_settings:
//                submenuLayout.collapse(true);
//                Intent sIntent = new Intent(this, SettingActivity.class);
//                startActivity(sIntent);
//                break;

            case R.id.btn_bottom_prev:
                if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE
                        && urlLayout != null && urlLayout.getVisibility() == View.GONE)
                    currBrowserFragment.onPrevButtonClicked();
                break;

            case R.id.btn_bottom_next:
                if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE
                        && urlLayout != null && urlLayout.getVisibility() == View.GONE)
                    currBrowserFragment.onNextButtonClicked();
                break;

            case R.id.btn_bottom_share:
                break;

            case R.id.btn_bottom_down:
                Intent intent = new Intent(FrontActivity.this, DownloadActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_bottom_menu:
                visibleSubmenu(true, true);
                break;

            case R.id.btn_bottom_close:
                visibleSubmenu(false, true);
                break;

            case R.id.btn_bottom_home:
                currBrowserFragment.setHomeData();
                visibleBrowser(false);
                if (urlLayout != null && urlLayout.getVisibility() == View.VISIBLE)
                    urlLayout.setVisibility(View.GONE);
                break;

            case R.id.layout_bottom_setting:
                visibleSubmenu(false, false);
                Intent sIntent = new Intent(this, SettingActivity.class);
                startActivity(sIntent);
                break;

            case R.id.layout_bottom_quit:
                visibleSubmenu(false, false);
                quitButton.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (browserLayout != null && browserLayout.getVisibility() == View.VISIBLE && currBrowserFragment != null
                                && urlLayout != null && urlLayout.getVisibility() == View.GONE)
                            currBrowserFragment.newTabButtonClicked();
                        else
                            visibleNewTabLayout(true);
                    }
                }, 50);


//                showExitDialog();
                break;

            default:
                break;
        }
    }

    @Override
    public void onUrlEditClick(String url) {
        if (urlLayout == null || urlEdit == null)
            return;

        urlLayout.setVisibility(View.VISIBLE);

        if (url != null && !TextUtils.isEmpty(url)) {
            urlEdit.setText(url);
            urlEdit.setSelection(0, url.length());
        }
        Util.showKeyBoard(urlEdit);

        favList.clear();
        ArrayList<FavoriteItem> fList = new ArrayList<>();
        fList.addAll(db.getFavoriteItems());
        for (FavoriteItem temp : fList) {
            favList.add(temp.getFavoriteSite());
        }
    }

    @Override
    public void onAccessYoutebe() {
        showYoutubeAlert();
    }

    @Override
    public void onAccessDownloadUrl(String site, String url) {
        showDownloadDialog(site, url);
    }

    @Override
    public void onWebviewScrolled() {
        if (submenuLayout.isExpanded())
            visibleSubmenu(false, false);
    }

    @Override
    public void onShowToast(String message) {
        toastText.setText(message);
        toastLayout.expand(true);
        toastHandler.sendEmptyMessageDelayed(0, 1500);
    }

    @Override
    public void onNewTabClick() {
        visibleNewTabLayout(true);
    }

    @Override
    public void onDownloadError(DownloadItem item, int errorCode) {

    }

    @Override
    public void onDownloadProgress(DownloadItem item, long progress, long total) {

    }

    @Override
    public void onDownloadStart(DownloadItem item) {

    }

    @Override
    public void onDownloadSuccess(DownloadItem item) {

    }

    @Override
    public void onClearReadyList() {
        if (videoNames == null)
            videoNames = new ArrayList<>();
        else
            videoNames.clear();
    }

    private void initView() {
        urlButton = fv(R.id.btn_input_url);
        urlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rootLayout != null) {
                    rootLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            homeBitmap = ImageUtil.getScreenShot(FrontActivity.this, rootLayout);
                            db.insertOrUpdateExtraItem(ImageUtil.getImageBytes(homeBitmap));
                        }
                    });
                }
                urlLayout.setVisibility(View.VISIBLE);
                Util.showKeyBoard(urlEdit);

                favList.clear();
                ArrayList<FavoriteItem> fList = new ArrayList<>();
                fList.addAll(db.getFavoriteItems());
                for (FavoriteItem temp : fList) {
                    favList.add(temp.getFavoriteSite());
                }
            }
        });

        urlLayout = fv(R.id.layout_edit_url);
        urlLayout.setVisibility(View.GONE);
        urlLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(urlEdit);
            }
        });

        newText = fv(R.id.text_new_tab);
        newText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleNewTabLayout(true);
            }
        });
        bottomNewText = fv(R.id.text_bottom_new_tab);

        initMainList();
        initEditUrl();
        initBrowser();


        submenuLayout = fv(R.id.layout_expend_submenu);
        submenuLayout.collapse(false);

        toastLayout = fv(R.id.layout_expend_toast);
        toastLayout.collapse(false);
        toastText = fv(R.id.text_toast);

        closeSubLayout = fv(R.id.view_close_sub);
        closeSubLayout.setVisibility(View.GONE);
        closeSubLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submenuLayout.isExpanded())
                    visibleSubmenu(false, true);
            }
        });

        addBookmarkButton = fv(R.id.btn_menu_add_bookmark);
        addBookmarkButton.setOnClickListener(this);
        addReadingButton = fv(R.id.btn_menu_add_reading);
        addReadingButton.setOnClickListener(this);
        bookmarksButton = fv(R.id.btn_menu_bookmarks);
        bookmarksButton.setOnClickListener(this);
        readingButton = fv(R.id.btn_menu_reading);
        readingButton.setOnClickListener(this);
        historyButton = fv(R.id.btn_menu_history);
        historyButton.setOnClickListener(this);
//        settingButton = fv(R.id.btn_menu_settings);
//        settingButton.setOnClickListener(this);

        prevButton = fv(R.id.btn_bottom_prev);
        prevButton.setOnClickListener(this);
        nextButton = fv(R.id.btn_bottom_next);
        nextButton.setOnClickListener(this);
        shareButton = fv(R.id.btn_bottom_share);
        shareButton.setOnClickListener(this);
        downloadButton = fv(R.id.btn_bottom_down);
        downloadButton.setOnClickListener(this);
        menuButton = fv(R.id.btn_bottom_menu);
        menuButton.setOnClickListener(this);
        homeButton = fv(R.id.btn_bottom_home);
        homeButton.setOnClickListener(this);
        closeButton = fv(R.id.btn_bottom_close);
        closeButton.setOnClickListener(this);
        settingButton = fv(R.id.layout_bottom_setting);
        settingButton.setOnClickListener(this);
        quitButton = fv(R.id.layout_bottom_quit);
        quitButton.setOnClickListener(this);
        downloadLayout = fv(R.id.layout_bottom_download);
        downReadyCnt = fv(R.id.text_bottom_down_count);
        visibleSubmenu(false, false);

        urlLV = fv(R.id.list_url);
        urlLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (urlList == null || urlList.size() < position)
                    return;

                String url = urlList.get(position);
                urlEdit.setText(url);
                goToBrowser(url);
            }
        });
        urlList = new ArrayList<>();
        favList = new ArrayList<>();
        urlAdapter = new UrlAdapter(this, urlList);
        urlLV.setAdapter(urlAdapter);

        exitLayout = fv(R.id.layout_black);
        exitOKButton = fv(R.id.layout_exit_ok);
        exitOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        exitCancelButton = fv(R.id.layout_exit_cancel);
        exitCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitLayout.setVisibility(View.GONE);
            }
        });
    }

    private void initEditUrl() {
        urlClearButton = fv(R.id.button_close);
        urlClearButton.setVisibility(View.GONE);
        urlClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlEdit.setText("");
            }
        });

        urlText = fv(R.id.text_url);
        urlText.setVisibility(View.GONE);
        urlEdit = fv(R.id.edit);
        urlEdit.setHint(r.s(R.string.hint_edit_url));
//        urlEdit.setText(sp.getBrowserUrl());
        urlEdit.setText("");

//        if (!TextUtils.isEmpty(sp.getBrowserUrl()))
//            urlClearButton.setVisibility(View.VISIBLE);
        urlEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                urlEdit.selectAll();
//                if (hasFocus) {
//                    urlEdit.selectAll();
//                } else {
//                    urlEdit.setText(sp.getBrowserUrl());
//                }
            }
        });

        urlEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                urlText.setText(s.toString());
                urlSearchHandler.removeCallbacksAndMessages(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                urlClearButton.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlText.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlLV.setVisibility(empty ? View.GONE : View.VISIBLE);

                if (!empty)
                    urlSearchHandler.sendEmptyMessage(0);
            }
        });

        urlEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    Util.hideKeyBoard(FrontActivity.this);

                    String url = urlEdit.getText().toString();
//                    if (url.matches("(?i).*youtube.*")) { // URL에 youtube 입력시
//                        showYoutubeAlert();
//                    } else
//                    if (url.matches("(?i).*dailymotion.*")){ // URL에 dailymotion 입력시
//                        showWarningAlert();
//                    } else {
//                    {
                    String SCHEME = "http://";
                    if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
//                            String googleSearch = "http://www.google.com/m?q=";
                        sp.setBrowserUrl(getSearchEnging() + url);
                    } else if (url.matches("http.*")) { // http로 시작
                        sp.setBrowserUrl(url);
                    } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
                            || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
                            || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
                            || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
                            || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
                        sp.setBrowserUrl(SCHEME + url);
                    } else {
//                            String googleSearch = "http://www.google.com/m?q=";
                        sp.setBrowserUrl(getSearchEnging() + url);

                    }

//                        btnBack.setVisibility(View.VISIBLE);
                    goToBrowser(sp.getBrowserUrl());
//                    }
                }
                return false;
            }
        });
    }

    private String getSearchEnging() {
        String engine = "http://www.google.com/m?q=";
        switch (sp.getSearchEngine()) {
            case 0: // Google
                engine = "http://www.google.com/m?q=";
                break;

            case 1: // Naver
                engine = "http://m.search.naver.com/search.naver?query=";
                break;

            case 2: // bing
                engine = "http://www.bing.com/search?q=";
                break;
        }

        return engine;
    }

    private void initMainList() {
        rootLayout = fv(R.id.layout_root);
        editModeLayout = fv(R.id.layout_edit_mode);
        editModeLayout.setVisibility(View.GONE);
        editModeView = fv(R.id.view_edit_mode);
        editModeView2 = fv(R.id.view_edit_mode2);
        editModeView3 = fv(R.id.view_edit_mode3);
        editModeView.setVisibility(View.GONE);
        editModeView2.setVisibility(View.GONE);
        editModeView3.setVisibility(View.GONE);
        visitText = fv(R.id.text_visit);
        doneText = fv(R.id.text_done_edit_mode);
        doneText.setOnClickListener(this);
        editModeButton = fv(R.id.button_mode_edit);
        editModeButton.setOnClickListener(this);
        editCloseButton = fv(R.id.button_mode_close);
        editCloseButton.setOnClickListener(this);
        clearVisitButton = fv(R.id.button_clear_visit);
        clearVisitButton.setOnClickListener(this);
        emptyText = fv(R.id.text_empty_mostly_visit);

        quickLv = fv(R.id.lv_bookmark);
        quickManager = new GridLayoutManager(this, 5);
        quickLv.setLayoutManager(quickManager);
        quickList = new ArrayList<>();
        quickDialAdapter = new QuickDialAdapter(this, R.layout.item_quick_dial, quickList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, final Object item) {
                if (item != null && currBrowserFragment != null) {
                    if (quickDialAdapter.isDeleteMode()) {
                        if (fdf(MessageDialog.TAG) == null) {
                            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                            dlg.setTitle(r.s(R.string.dlg_del_quick_title));
                            dlg.setMessage(r.s(R.string.dlg_del_quick_msg));
                            dlg.setNegativeLabel(r.s(R.string.cancel));
                            dlg.setPositiveLabel(r.s(R.string.delete));
                            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                                @Override
                                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                                    db.deleteQuickDialItem((QuickDialItem) item);
                                    refreshMainList();
                                }
                            });
                            sdf(dlg);
                        }
                    } else {
                        if (((QuickDialItem) item).getQuickDialUrl() != null && ((QuickDialItem) item).getQuickDialUrl().equalsIgnoreCase(QuickDialItem.ID_HOW_TO_USE)) {
                            Intent intent = new Intent(FrontActivity.this, HelpActivity.class);
                            startActivity(intent);
                        } else if (((QuickDialItem) item).getQuickDialId().equals(r.s(R.string.main_bookmark_add))) {
                            Intent intent = new Intent(FrontActivity.this, QuickDialActivity.class);
                            startActivity(intent);
//                        } else if (((QuickDialItem) item).getQuickDialId().equals(r.s(R.string.main_popular_apps))) {
//                            try {
//                                Class<?> aClass = Class.forName("com.mobvista.msdk.shell.MVActivity");
//                                Intent intent = new Intent(FrontActivity.this, aClass);
//                                intent.putExtra(MobVistaConstans.PROPERTIES_UNIT_ID, Common.MOBVISTA_APP_WALL);
//                                startActivity(intent);
//                            } catch (Exception e) {
//                                LogUtil.log(e.getMessage());
//                            }
                        } else if (((QuickDialItem) item).getQuickDialId().equals(r.s(R.string.main_jmobile))) {
                            goToBrowser(Common.MAIN_JMOBILE_APP);
                        } else {
                            String url = ((QuickDialItem) item).getQuickDialUrl();
                            if (!url.contains("http://") && !url.contains("https://"))
                                url = "http://" + url;
                            goToBrowser(url);
                        }
                    }
                }

            }

            @Override
            public void OnItemLongClick(int position, Object item) {
                if (((QuickDialItem) item).getRowId() == -1)
                    return;
                setQuickEditMode(true);

            }
        });
        quickLv.setAdapter(quickDialAdapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(quickLv);

        mostlyLv = fv(R.id.lv_mostly_visit);
        mostlyManager = new GridLayoutManager(this, 2);
        mostlyLv.setLayoutManager(mostlyManager);
        favoriteList = new ArrayList<>();
        favoriteAdapter = new FavoriteAdapter(this, R.layout.item_favorite, favoriteList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                if (item != null && currBrowserFragment != null)
                    goToBrowser(((FavoriteItem) item).getFavoriteSite());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        mostlyLv.setAdapter(favoriteAdapter);

        refreshMainList();
    }

    private void initBrowser() {
        newTabLayout = fv(R.id.layout_new_tab);
        newTabLayout.setVisibility(View.GONE);

        pager = fv(R.id.viewpager_new_tab);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        final int pageMargin = ((metrics.widthPixels / 8) * 2);
        pager.setPageMargin(-pageMargin);
        pager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = pager.getMeasuredWidth() - pager.getPaddingLeft() - pager.getPaddingRight();
                int pageHeight = pager.getHeight();
                int paddingLeft = pager.getPaddingLeft();
                float transformPos = (float) (page.getLeft() - (pager.getScrollX() + paddingLeft)) / pageWidth;

                final float normalizedposition = Math.abs(Math.abs(transformPos) - 1);
                page.setAlpha(normalizedposition + 0.5f);

                float upTranslation = -pageHeight / 10f;
                if (transformPos < -1) {// [-Infinity,-1)
                    page.setTranslationY(0);
                } else if (transformPos < 0) { // [-1,0)
                    float translationY = upTranslation * (transformPos + 1f);
                    page.setTranslationY(translationY);
                } else if (transformPos == 0) { // 0
                    page.setTranslationY(upTranslation);
                } else if (transformPos <= 1) { // (0,1]
                    float translationY = upTranslation * (1f - transformPos);
                    page.setTranslationY(translationY);
                } else { // (1,+Infinity]
                    page.setTranslationY(0);
                }
            }
        });
//        pager.setOnItemClickLIstener(new BaseClickableViewPager.OnItemClickListener() {
//            @Override
//            public void onItemClick(int position) {
//
//            }
//        });

        browserFragments = new ArrayList<>();

        clearAllButton = fv(R.id.button_clear_all);
        clearAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.dlg_msg_delete_all_tab));
                    dlg.setNegativeLabel(r.s(R.string.cancel));
                    dlg.setPositiveLabel(r.s(R.string.close));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            db.deleteAllBrowserItems();
                            refreshBrowserList();
                            visibleNewTabLayout(false);
                        }
                    });

                    sdf(dlg);
                }

            }
        });

        newButton = fv(R.id.button_new);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BrowserItem item = new BrowserItem();
                item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                item.setBrowserTitle("HOME");
                item.setBrowserUrl("HOME");
                item.setBrowserPrivacy(false);
                item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
                item.setBrowserAt(System.currentTimeMillis());
                currBrowserFragment = new BrowserFragment();
                currBrowserFragment.setListener(FrontActivity.this);
                currBrowserFragment.setBrowserItem(item);
                browserFragments.add(currBrowserFragment);
//                browserFragments.add(currBrowserFragment);
//                browserViewAdapter.addFrag(currBrowserFragment, item.getBrowserId());
//                browserViewAdapter.notifyDataSetChanged();
                browserList.add(item);
                sp.setCurrentBrowserIndex(browserList.size() - 1);
                browserAdapter.notifyDataSetChanged();
                pager.setCurrentItem(sp.getCurrentBrowserIndex(), true);
                db.insertOrUpdateBrowserItem(item);


//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
//                transaction.commit();
//                browserViewpager.setCurrentItem(sp.getCurrentBrowserIndex());
                visibleNewTabLayout(false);
            }
        });
        returnText = fv(R.id.text_returen);
        returnButton = fv(R.id.button_returen);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.setCurrentBrowserIndex(pager.getCurrentItem());
                visibleNewTabLayout(false);
            }
        });
        currBrowserFragment = new BrowserFragment();
        currBrowserFragment.setListener(FrontActivity.this);
        browserLayout = fv(R.id.layout_fragment_browser);
        browserLayout.setVisibility(View.INVISIBLE);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.add(R.id.layout_fragment_browser, currBrowserFragment);
//        transaction.commit();
//        browserViewpager = fv(R.id.viewpager_browser);
//        browserViewpager.setEnableSwipe(false);
//        browserViewpager.setVisibility(View.INVISIBLE);
//
//        browserViewAdapter = new BrowserViewPagerAdapter(getSupportFragmentManager());
//        browserViewpager.setAdapter(browserViewAdapter);

        browserList = new ArrayList<>();
        browserList.addAll(db.getBrowserItems());

        browserAdapter = new CarouselPagerAdapter(this, getSupportFragmentManager(), browserList);
        pager.setAdapter(browserAdapter);

        pager.addOnPageChangeListener(browserAdapter);
        pager.setCurrentItem(sp.getCurrentBrowserIndex());
        pager.setOffscreenPageLimit(2);

//        browserFragments = new ArrayList<>();
//
        refreshBrowserList();
    }

    public BaseClickableViewPager getViewPager() {
        return pager;
    }

    public void pagerDelete(BrowserItem item, final int position) {
        if (sp.getCurrentBrowserIndex() == position)
            sp.setCurrentBrowserIndex(position == 0 ? 0 : position - 1);
        db.deleteBrowserItem(item);

        browserList.remove(position);
        browserFragments.remove(position);
        browserAdapter.notifyDataSetChanged();
        if (browserList.size() > 0) {

            pager.setCurrentItem(position == 0 ? 0 : position - 1);
            sp.setBrowserTabCount(browserList.size());
            bottomNewText.setText("" + browserList.size());
            newText.setText("" + browserList.size());
            returnText.setText("" + browserList.size());
        } else {
            refreshBrowserList();
            visibleNewTabLayout(false);
        }
    }

    public void selectTab(int position) {
        sp.setCurrentBrowserIndex(position);
        currBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        BrowserItem item = currBrowserFragment.getBrowserItem();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
        transaction.commit();


    }

    public void selectedTab(int position) {
        sp.setBrowserTabCount(browserList.size());
        bottomNewText.setText("" + browserList.size());
        newText.setText("" + browserList.size());
        returnText.setText("" + browserList.size());

        if (pager.getCurrentItem() == sp.getCurrentBrowserIndex())
            pager.setCurrentItem(pager.getCurrentItem() == 0 ? 1 : pager.getCurrentItem() - 1);
        newTabLayout.setVisibility(View.GONE);
        BrowserItem item = currBrowserFragment.getBrowserItem();
        if (item != null && item.getBrowserTitle().equals("HOME"))
            visibleBrowser(false);
        else
            visibleBrowser(true);
    }

    private void refreshMainList() {
        quickList.clear();
        favoriteList.clear();

        if (sp.isFirstSetHowToUse()) {
            QuickDialItem item = new QuickDialItem();
            item.setQuickDialId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setQuickDialShowing(false);
            item.setQuickDialOrder((int) db.getNextDBId(QuickDialItem.TABLE_NAME));
            item.setQuickDialAt(System.currentTimeMillis());
            item.setQuickDialFrom(QuickDialItem.ADD_RECOMMEND);
            item.setQuickDialFromId(QuickDialItem.ID_HOW_TO_USE);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.btn_how_to_use_50);
            item.setQuickDialIcon(ImageUtil.getImageBytes(bitmap));
            item.setQuickDialName(getString(R.string.settings_title_how));
            item.setquickUrl(QuickDialItem.ID_HOW_TO_USE);
            db.insertOrUpdateQuickDialItem(item);
            sp.setFirstSetHowToUse(false);

            if (rootLayout != null) {
                rootLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        homeBitmap = ImageUtil.getScreenShot(FrontActivity.this, rootLayout);
                        if (homeBitmap != null) {
                            byte[] bHomeBitmap = ImageUtil.getImageBytes(homeBitmap);
                            if (bHomeBitmap != null)
                                db.insertOrUpdateExtraItem(bHomeBitmap);
                        }
                    }
                });
            }
        }

        ArrayList<QuickDialItem> list = new ArrayList<>();
        list.addAll(db.getQuickDialItems());
        if (list != null && list.size() > 0) {
            for (QuickDialItem item : list)
//                if (item.isQuickDialShowing())
                quickList.add(item);
        }

        if (list.size() <= 0 && quickDialAdapter != null && quickDialAdapter.isDeleteMode())
            quickDialAdapter.setDeleteMode(false);

        editModeButton.setVisibility(list.size() > 0 && !quickDialAdapter.isDeleteMode() ? View.VISIBLE : View.GONE);
        editCloseButton.setVisibility(list.size() > 0 && quickDialAdapter.isDeleteMode() ? View.VISIBLE : View.GONE);

        if (quickDialAdapter != null && !quickDialAdapter.isDeleteMode()) {
            quickList.add(new QuickDialItem().setQuickDialName(r.s(R.string.main_jmobile))
                    .setRowId(-1)
                    .setQuickDialIcon(ImageUtil.getImageBytes(((BitmapDrawable) r.d(this, R.drawable.icon_jmobile_50)).getBitmap()))
                    .setQuickDialId(r.s(R.string.main_jmobile)));

//            quickList.add(new QuickDialItem().setQuickDialName(r.s(R.string.main_popular_apps))
//                    .setRowId(-1)
//                    .setQuickDialIcon(ImageUtil.getImageBytes(((BitmapDrawable) r.d(this, R.drawable.btn_popular_apps)).getBitmap()))
//                    .setQuickDialId(r.s(R.string.main_popular_apps)));
        }

        if (quickList.size() < QuickDialItem.MAX_SHOWING + 2 && quickDialAdapter != null && !quickDialAdapter.isDeleteMode())
            quickList.add(new QuickDialItem().setRowId(-1)
                    .setQuickDialIcon(ImageUtil.getImageBytes(((BitmapDrawable) r.d(this, R.drawable.btn_add_new_app_50)).getBitmap()))
                    .setQuickDialId(r.s(R.string.main_bookmark_add)));

        ArrayList<FavoriteItem> hList = new ArrayList<>();
        hList.addAll(db.getFavoriteItems());
        if (hList != null && hList.size() > 0) {
            for (FavoriteItem item : hList) {
                if (favoriteList.size() < 10)
                    favoriteList.add(item);
            }
        }
        clearVisitButton.setVisibility(hList.size() > 0 ? View.VISIBLE : View.GONE);
        emptyText.setVisibility(hList.size() > 0 ? View.GONE : View.VISIBLE);
        quickDialAdapter.notifyDataSetChanged();
        favoriteAdapter.notifyDataSetChanged();
    }

    private void refreshBrowserList() {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        browserList.clear();
        browserFragments.clear();
        browserList.addAll(db.getBrowserItems());
//        browserViewAdapter.clear();
//        browserFragments = new ArrayList<>();

        if (browserList == null || browserList.size() <= 0) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            browserList.add(item);

            currBrowserFragment = new BrowserFragment();
            currBrowserFragment.setListener(this);
            currBrowserFragment.setBrowserItem(item);
            browserFragments.add(currBrowserFragment);
            sp.setCurrentBrowserIndex(0);

//            transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
//            transaction.hide(currBrowserFragment);
//            transaction.commit();
//            browserFragments.add(currBrowserFragment);
//            browserViewAdapter.addFrag(currBrowserFragment, item.getBrowserId());

            db.insertOrUpdateBrowserItem(item);
        } else {
            for (BrowserItem item : browserList) {
                BrowserFragment bFragment = new BrowserFragment();
                bFragment.setBrowserItem(item);
                bFragment.setListener(this);
                browserFragments.add(bFragment);
            }

            int index = sp.getCurrentBrowserIndex();
            if (index < 0 || index >= browserList.size()) {
                sp.setCurrentBrowserIndex(0);
            }

            currBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, browserList.get(sp.getCurrentBrowserIndex()).getBrowserId());
        transaction.commit();
//        browserViewAdapter.notifyDataSetChanged();
        browserAdapter.notifyDataSetChanged();
        pager.setCurrentItem(sp.getCurrentBrowserIndex());
//        browserViewpager.setCurrentItem(sp.getCurrentBrowserIndex());
        sp.setBrowserTabCount(browserList.size());
        bottomNewText.setText("" + browserList.size());
        newText.setText("" + browserList.size());
        returnText.setText("" + browserList.size());
//        if (currBrowserFragment != null)
//            currBrowserFragment.setNewTabCount(browserList.size());
    }

    @SuppressLint("NewApi")
    private void setQuickEditMode(boolean mode) {
        if (quickDialAdapter != null)
            quickDialAdapter.setDeleteMode(mode);

        if (favoriteAdapter != null)
            favoriteAdapter.setDeleteMode(mode);

        refreshMainList();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, mode ? R.color.url_state_dim : R.color.set_title_background));
        }

        rootLayout.setBackgroundResource(mode ? R.color.main_dim : R.color.main);
        editModeView.setVisibility(mode ? View.VISIBLE : View.GONE);
        editModeView2.setVisibility(mode ? View.VISIBLE : View.GONE);
        editModeView3.setVisibility(mode ? View.VISIBLE : View.GONE);

        editModeLayout.setVisibility(mode ? View.VISIBLE : View.GONE);

        visitText.setTextColor(ContextCompat.getColor(this, mode ? R.color.main_text_dim : R.color.gray7_100));
    }

    private void showYoutubeAlert() {
        if (fdf(Common.TAG_DIALOG_ALERT_YOUTUBE) == null) {
            MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_ALERT_YOUTUBE);
            dlg.setTitle(r.s(R.string.warning));
            dlg.setMessage(r.s(R.string.alert_youtube));
            dlg.setPositiveLabel(r.s(R.string.ok));
            sdf(dlg);
        }
    }

    private void showWarningAlert() {
        if (fdf(Common.TAG_DIALOG_ALERT_WARNING) == null) {
            MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_ALERT_WARNING);
            dlg.setTitle(r.s(R.string.warning));
            dlg.setMessage(r.s(R.string.alert_warning));
            dlg.setPositiveLabel(r.s(R.string.ok));
            sdf(dlg);
        }
    }

    String checkUrl = "";
    ArrayList<String> videoNames = new ArrayList<>();
    private void showDownloadDialog(final String site, final String url) {
        if (url == null || TextUtils.isEmpty(url) || url.matches("(?i).*youtube.*"))
            return;

        if (checkUrl.equalsIgnoreCase(url))
            return;

        String fUrl = url;
        String fName = URLUtil.guessFileName(url, "https", "video/mp4");
        if (TextUtils.isEmpty(fName))
            return;

        // facebook filtering
        if (url.contains("fbcdn.net")) {

            if (videoNames.contains(fName)) {
//                LogUtil.log("CONTAINS!!! " + filename);
                return;
            } else {
                videoNames.add(fName);
            }
            LogUtil.log("FILE NAME :: " + fName);

            if (url.contains("&bytestart=")) {
                int index = url.indexOf("&bytestart=");
                fUrl = url.substring(0, index);
            } else
                fUrl = url;
        }

        checkUrl = fUrl;
        LogUtil.log(checkUrl);

        String filename = Common.PREFIX_DOWNLOAD_FILE + System.currentTimeMillis() + Common.checkVideoExtension(url);

        if (!sp.isUseNotification()) {
            if (url.contains("fbcdn.net")) {
                Bitmap bitmap = null;
                MediaMetadataRetriever mediaMetadataRetriever = null;
                try {
                    mediaMetadataRetriever = new MediaMetadataRetriever();
                    if (Build.VERSION.SDK_INT >= 14)
                        mediaMetadataRetriever.setDataSource(fUrl, new HashMap<String, String>());
                    else
                        mediaMetadataRetriever.setDataSource(fUrl);

                    if (url.contains("fbcdn.net"))
                        bitmap = mediaMetadataRetriever.getFrameAtTime(2, MediaMetadataRetriever.OPTION_CLOSEST);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    {
                        if (mediaMetadataRetriever != null)
                            mediaMetadataRetriever.release();
                    }
                }

                if (url.contains("fbcdn.net") && bitmap == null)
                    return;
            }

            sp.setCheckedDownloadUrl("");
            addDownloadList(site, fUrl, filename);
        } else {
            Bitmap bitmap = null;
            long time = 0;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(fUrl, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(fUrl);

                if (url.contains("fbcdn.net"))
                    bitmap = mediaMetadataRetriever.getFrameAtTime(2, MediaMetadataRetriever.OPTION_CLOSEST);
                else
                    bitmap = mediaMetadataRetriever.getFrameAtTime();

                String temp = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                if (!TextUtils.isEmpty(temp)) {
                    time = Long.parseLong(temp);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }

            if (url.contains("fbcdn.net") && bitmap == null)
                return;

            DownloadDialog dlg = DownloadDialog.newInstance(Common.TAG_DIALOG_ADD_DOWNLOAD, filename, bitmap, time);
            sdf(dlg);

            final String dUrl = fUrl;
            dlg.setListener(new DownloadDialog.DownloadDialogListener() {
                @Override
                public void onDownloadButtonClick(String name) {
                    sp.setCheckedDownloadUrl("");
                    addDownloadList(site, dUrl, name);
                }

                @Override
                public void onCancelButtonClick() {
                    sp.setCheckedDownloadUrl("");
                }
            });
        }
    }

    private void addDownloadList(String site, String url, String filename) {
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(filename))
            return;

        DownloadItem item = new DownloadItem();
        item.setDownloadId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setDownloadUrl(url);
        item.setDownloadSite(site);
        item.setDownloadFileName(filename);
        item.setDownloadState(DownloadItem.DOWNLOAD_STATE_NONE);
        item.setDownloadFileRemainSize(0);
        item.setDownloadFilePath(Common.getVideoDir(app.getApplicationContext()) + item.getDownloadFileName() + FileUtil.TEMP_FILE_EXTENSION);
        item.setDownloadFileThumbnailPath(Common.getVideoDir(app.getApplicationContext()) + "." + FileUtil.getFileName(item.getDownloadFileName()) + FileUtil.TEMP_THUMBNAIL_EXTENSION);

        new AddDownloadItem().execute(item);

    }

    private void setCheckReadyDownloadItem() {
        int cnt = db.getReadyDownloadItemCount();
        downReadyCnt.setText(String.valueOf(cnt));
        downReadyCnt.setVisibility(cnt > 0 ? View.VISIBLE : View.GONE);

        int random = (new Random().nextInt(16));
        downReadyCnt.setBackgroundResource(getResources().obtainTypedArray(R.array.down_count_bg).getResourceId(random, 0));

        if (cnt > 0) {
            downloadLayout.post(new Runnable() {
                @Override
                public void run() {
                    int left = downReadyCnt.getMeasuredWidth() / 2;
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) downReadyCnt.getLayoutParams();
                    params.setMargins(left, (int) Util.convertDpToPixel(7, FrontActivity.this), 0, 0);
                    downReadyCnt.setLayoutParams(params);
                }
            });

        }
    }

    private void goToBrowser(String url) {
        Util.hideKeyBoard(urlEdit);
        urlLayout.setVisibility(View.GONE);
        if (url.matches("(?i).*youtube.*")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return;
        }

        visibleBrowser(true);
        currBrowserFragment.goUrl(url);
    }

    @SuppressLint("NewApi")
    private void visibleBrowser(boolean show) {
        browserLayout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            getWindow().setStatusBarColor(ContextCompat.getColor(this, show ? R.color.set_title_background : R.color.main_state_background));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.set_title_background));
        }

        if (!show) {
            if (currBrowserFragment != null) {
                currBrowserFragment.stopWebViewLoading();
            }
            currBrowserFragment.clearCurrData();
            refreshMainList();
        } else {
            if (currBrowserFragment != null) {
                currBrowserFragment.onResume();
            }
        }
    }

    private void visibleSubmenu(boolean show, boolean anim) {
        if (show)
            submenuLayout.expand(anim);
        else
            submenuLayout.collapse(anim);

        closeSubLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        prevButton.setVisibility(show ? View.GONE : View.VISIBLE);
        nextButton.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        settingButton.setVisibility(show ? View.VISIBLE : View.GONE);
        menuButton.setVisibility(show ? View.GONE : View.VISIBLE);
        closeButton.setVisibility(show ? View.VISIBLE : View.GONE);
        downloadLayout.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        homeButton.setVisibility(show ? View.GONE : View.VISIBLE);
        quitButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @SuppressLint("NewApi")
    private void visibleNewTabLayout(boolean show) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (show)
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }

//        refreshBrowserList();

        currBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        BrowserItem item = currBrowserFragment.getBrowserItem();
        if (show) {
            if (currBrowserFragment != null) {
                currBrowserFragment.stopWebViewLoading();
            }
            currBrowserFragment.clearCurrData();
            if (browserLayout.getVisibility() != View.VISIBLE && urlLayout.getVisibility() != View.VISIBLE) {
                homeBitmap = ImageUtil.getScreenShot(this, rootLayout);
                db.insertOrUpdateExtraItem(ImageUtil.getImageBytes(homeBitmap));
            }
            browserAdapter.setHomeBitmap(homeBitmap);
            pager.setCurrentItem(sp.getCurrentBrowserIndex());

            newTabLayout.setVisibility(View.VISIBLE);
            newTabLayout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up));

        } else {
            if (item != null && item.getBrowserTitle().equals("HOME"))
                visibleBrowser(false);
            else
                visibleBrowser(true);


            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
            transaction.commit();

            sp.setBrowserTabCount(browserList.size());
            bottomNewText.setText("" + browserList.size());
            newText.setText("" + browserList.size());
            returnText.setText("" + browserList.size());

            if (pager.getCurrentItem() == sp.getCurrentBrowserIndex())
                pager.setCurrentItem(pager.getCurrentItem() == 0 ? 1 : pager.getCurrentItem() - 1);
            newTabLayout.setVisibility(View.GONE);

        }

    }

    @SuppressLint("NewApi")
    private void showExitDialog() {
        if (exitLayout == null)
            return;

        boolean show = exitLayout.getVisibility() == View.GONE;
        exitLayout.setVisibility(show ? View.VISIBLE : View.GONE);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, show ? R.color.exit_state_dim : R.color.set_title_background));
        }
//        if (fdf(MessageDialog.TAG) == null) {
//            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//            dlg.setMessage(r.s(R.string.msg_confirm_finish));
//            dlg.setNegativeLabel(r.s(R.string.cancel));
//            dlg.setPositiveLabel(r.s(R.string.exit));
//            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
//                @Override
//                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
//                    finish();
//                }
//            });
//            sdf(dlg);
//        }
    }

    private void moveItem(int oldPos, int newPos) {
        QuickDialItem item = quickList.get(oldPos);
        quickList.remove(oldPos);
        quickList.add(newPos, item);
        quickDialAdapter.notifyItemMoved(oldPos, newPos);

        for (int i = 0; i < quickList.size(); i++) {
            item = quickList.get(i);
            item.setQuickDialOrder(i);
            db.updateQuickDialOrder(item);
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return quickDialAdapter != null && quickDialAdapter.isDeleteMode();
            }
        };

        return simpleCallback;
    }

    private ItemTouchHelper.Callback createBrowserHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.UP | ItemTouchHelper.DOWN) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

//    private void initMobVista() {
//        mobVistaSDK = MobVistaSDKFactory.getMobVistaSDK();
//        String appId = "33479";
//        String appKey = "543d2bdd4ad5a4cb493a93fbc4e33ed6";
//        Map<String, String> map = mobVistaSDK.getMVConfigurationMap(appId, appKey);
//        mobVistaSDK.init(map, this);
//    }
////
//    private void preLoadMobvista() {
//        String unitId = "10738";
//        Map<String, Object> preloadMap = new HashMap<String, Object>();
//        preloadMap.put(MobVistaConstans.PROPERTIES_LAYOUT_TYPE, MobVistaConstans.LAYOUT_APPWALL);
//        preloadMap.put(MobVistaConstans.PROPERTIES_UNIT_ID, unitId);
//        mobVistaSDK.preload(preloadMap);
//    }

    private void initAds() {
//        adExitSquare = fv(R.id.adview_exit);
//        adExitSquare.setAdUnitId(Common.ADX_EXIT_SQUARE);
//        adExitSquare.setBannerAdListener(new MoPubView.BannerAdListener() {
//            @Override
//            public void onBannerLoaded(MoPubView banner) {
//
//            }
//
//            @Override
//            public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
//
//            }
//
//            @Override
//            public void onBannerClicked(MoPubView banner) {
//
//            }
//
//            @Override
//            public void onBannerExpanded(MoPubView banner) {
//
//            }
//
//            @Override
//            public void onBannerCollapsed(MoPubView banner) {
//
//            }
//        });
//        adExitSquare.loadAd();
        exitAdLayout = fv(R.id.layout_exit_ads);
        AdView admobExitBanner = new AdView(FrontActivity.this);
        admobExitBanner.setAdSize(AdSize.MEDIUM_RECTANGLE);
        admobExitBanner.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_EXIT_BANNER_TEST : Common.ADMOB_EXIT_BANNER);
//        admobExitBanner.setBackground(getResources().getDrawable(R.drawable.bg_none));
        admobExitBanner.setBackgroundColor(Color.TRANSPARENT);
        if (LogUtil.DEBUG_MODE) {
            AdRequest request = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
                    .addTestDevice("AC98C820A50B4AD8A2106EDE96FB87D4")  // An example device ID
                    .build();
            admobExitBanner.loadAd(request);
        } else
            admobExitBanner.loadAd(new AdRequest.Builder().build());
        exitAdLayout.addView(admobExitBanner);

//        mContentView = fv(R.id.adView_native_main);
//        NativeAdFactory.addListener(new NativeAdFactory.NativeAdListener() {
//            @Override
//            public void onSuccess(String s, NativeAd nativeAd) {
//                if (Common.ADX_MAIN_NATIVE.equals(s)) {
//                    mNativeAd = nativeAd;
//                    mAdView = NativeAdFactory.getNativeAdView(FrontActivity.this, Common.ADX_MAIN_NATIVE, mContentView, null);
//                    mContentView.addView(mAdView);
//                }
//            }
//
//            @Override
//            public void onFailure(String s) {
//
//            }
//        });
//        NativeAdFactory.loadAd(Common.ADX_MAIN_NATIVE);

        nativeAdsLayout = fv(R.id.layout_native_ads);
//        fanNativeAd = new NativeAd(this, Common.FAN_MAIN_NATIVE);
////        if (LogUtil.DEBUG_MODE)
////            AdSettings.addTestDevice("HASHED ID");
//        fanNativeAd.setAdListener(new com.facebook.ads.AdListener() {
//
//            @Override
//            public void onError(Ad ad, AdError error) {
//                LogUtil.log("fanNativeAd onError()" + error.getErrorMessage().toString());
//                if (fanNativeAd != null)
//                    fanNativeAd.destroy();
//
//                nativeAdsLayout.removeAllViews();
//
//                // Ad error callback
//                onLoadAdmobAdvanced();
////                NativeExpressAdView nativeExpressAdView = new NativeExpressAdView(FrontActivity.this);
////                nativeExpressAdView.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_NATIVE_MID_TEST : Common.ADMOB_MAIN_NATIVE_MID);
////                nativeExpressAdView.setAdSize(new AdSize(360,132));
////                nativeExpressAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
////                    @Override
////                    public void onAdOpened() {
////                        LogUtil.log("fanNativeAd onAdOpened()");
////                        super.onAdOpened();
////                    }
////
////                    @Override
////                    public void onAdLoaded() {
////                        LogUtil.log("fanNativeAd onAdLoaded()");
////                        super.onAdLoaded();
////                    }
////
////                    @Override
////                    public void onAdFailedToLoad(int i) {
////                        LogUtil.log("fanNativeAd onAdFailedToLoad()" + i);
////                        super.onAdFailedToLoad(i);
////                    }
////                });
////
////                nativeExpressAdView.loadAd(new AdRequest.Builder().build());
////                nativeAdsLayout.addView(nativeExpressAdView);
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                LogUtil.log("fanNativeAd onAdLoaded()");
//                if (fanNativeAd.isAdLoaded()) {
//                    fanNativeAd.unregisterView();
//                }
//
//                // Add the Ad view into the ad container.
//                LayoutInflater inflater = LayoutInflater.from(FrontActivity.this);
//                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
//                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.fan_native_main, nativeAdsLayout, false);
//                nativeAdsLayout.addView(adView);
//
//                // Create native UI using the ad metadata.
//                ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
//                TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
//                MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
//                TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
//                Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);
//
//                // Set the Text.
//                nativeAdTitle.setText(fanNativeAd.getAdTitle());
//                nativeAdBody.setText(fanNativeAd.getAdBody());
//                nativeAdCallToAction.setText(fanNativeAd.getAdCallToAction());
//
//                // Download and display the ad icon.
//                NativeAd.Image adIcon = fanNativeAd.getAdIcon();
//                NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);
//
//                // Download and display the cover image.
//                nativeAdMedia.setNativeAd(fanNativeAd);
//
//                // Add the AdChoices icon
//                LinearLayout adChoicesContainer = fv(R.id.ad_choices_container);
//                AdChoicesView adChoicesView = new AdChoicesView(FrontActivity.this, fanNativeAd, true);
//                adChoicesContainer.addView(adChoicesView);
//
//                // Register the Title and CTA button to listen for clicks.
//                List<View> clickableViews = new ArrayList<>();
//                clickableViews.add(nativeAdTitle);
//                clickableViews.add(nativeAdCallToAction);
//                fanNativeAd.registerViewForInteraction(nativeAdsLayout, clickableViews);
//
//
//            }
//
//
//            @Override
//            public void onAdClicked(Ad ad) {
//                // Ad clicked callback
//                LogUtil.log("fanNativeAd onAdClicked()");
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//                LogUtil.log("fanNativeAd onLoggingImpression()");
//            }
//        });
//
//        fanNativeAd.loadAd(NativeAd.MediaCacheFlag.ALL);
    }

    private void onLoadAdmobAdvanced() {
        AdLoader adLoader = new AdLoader.Builder(this, LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_NATIVE_ADVANCED_TEST : Common.ADMOB_MAIN_NATIVE_ADVANCED)
                .forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
                    @Override
                    public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                        if (nativeAppInstallAd == null || getLayoutInflater() == null)
                            return;
                        NativeAppInstallAdView adView = (NativeAppInstallAdView) getLayoutInflater().inflate(R.layout.admob_app_install_main, null);
                        populateAppInstallAdView(nativeAppInstallAd, adView);
                        nativeAdsLayout.removeAllViews();
                        ;
                        nativeAdsLayout.addView(adView);
                    }
                })
                .forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
                    @Override
                    public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                        if (nativeContentAd == null || getLayoutInflater() == null)
                            return;

                        NativeContentAdView adView = (NativeContentAdView) getLayoutInflater()
                                .inflate(R.layout.admob_content_main, null);
                        populateContentAdView(nativeContentAd, adView);
                        nativeAdsLayout.removeAllViews();
                        nativeAdsLayout.addView(adView);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder().build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAppInstallAd.getVideoController();

        // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
        // VideoController will call methods on this object when events occur in the video
        // lifecycle.
//        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
//            public void onVideoEnd() {
//                // Publishers should allow native ads to complete video playback before refreshing
//                // or replacing them with another ad in the same UI location.
//                mRefresh.setEnabled(true);
//                mVideoStatus.setText("Video status: Video playback has ended.");
//                super.onVideoEnd();
//            }
//        });

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
//        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));
//        adView.setImageView(adView.findViewById(R.id.bg_image));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());
//        ((ImageView) adView.getImageView()).setImageDrawable(nativeAppInstallAd.getImages().get(0).getDrawable());

        // Apps can check the VideoController's hasVideoContent property to determine if the
        // NativeAppInstallAd has a video asset.
//        if (vc.hasVideoContent()) {
//            mVideoStatus.setText(String.format(Locale.getDefault(),
//                    "Video status: Ad contains a %.2f:1 video asset.",
//                    vc.getAspectRatio()));
//        } else {
//            mRefresh.setEnabled(true);
//            mVideoStatus.setText("Video status: Ad does not contain a video asset.");
//        }

        // These assets aren't guaranteed to be in every NativeAppInstallAd, so it's important to
        // check before trying to display them.
//        if (nativeAppInstallAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAppInstallAd.getPrice());
//        }

//        if (nativeAppInstallAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAppInstallAd.getStore());
//        }

//        if (nativeAppInstallAd.getStarRating() == null) {
//            adView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) adView.getStarRatingView())
//                    .setRating(nativeAppInstallAd.getStarRating().floatValue());
//            adView.getStarRatingView().setVisibility(View.VISIBLE);
//        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        if (nativeContentAd.getImages().size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    private class InitDashboardItem extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<DownloadItem> list = new ArrayList<>();
            list.addAll(db.getTotalDownloadItems());

            for (DownloadItem item : list) {
                if (item.getDownloadStartAt() > 0 && item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DONE) {
                    DashboardItem dItem = new DashboardItem(item);
                    if (dItem != null)
                        db.insertOrUpdateDashboardItem(dItem);
                }
            }
            sp.setFirstSetDashboard(false);
            return null;
        }
    }

    private class AddDownloadItem extends AsyncTask<DownloadItem, Void, DownloadItem> {
        @Override
        protected DownloadItem doInBackground(DownloadItem... params) {
            DownloadItem item = params[0];
            String path = item.getDownloadUrl();
            if (!TextUtils.isEmpty(path)) {
                long totalSize = -1;
                long time = -1;
                HttpURLConnection conn = null;
                try {
                    URL url = new URL(path);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(1000);
                    conn.setReadTimeout(1000);
                    conn.setRequestMethod("HEAD");
                    totalSize = (long) conn.getContentLength();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null)
                        conn.disconnect();
                }
                item.setDownloadFileTotalSize(totalSize);
            }

            Bitmap bitmap = null;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            String tag = null;
            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(path, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(path);
                bitmap = mediaMetadataRetriever.getFrameAtTime();

                String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                if (TextUtils.isEmpty(time))
                    item.setDownloadFileTime(0);
                else {
                    long timeInmillisec = Long.parseLong(time);
                    item.setDownloadFileTime(timeInmillisec);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }
            if (bitmap != null)
                item.setDownloadThumbnail(ImageUtil.getImageBytes(bitmap));

            return item;
        }

        @Override
        protected void onPostExecute(DownloadItem result) {
            if (!sp.isUseNotification() && sp.getTimeFiltering() > 0) {
                long limit = 15 * sp.getTimeFiltering() * 1000;
                if (result.getDownloadFileTime() < limit)
                    return;
            }

            db.insertOrUpdateDownloadItem(result);
            setCheckReadyDownloadItem();
        }
    }

    class BrowserViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public BrowserViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void clear() {
            mFragmentList.clear();
            mFragmentTitleList.clear();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
