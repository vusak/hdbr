package io.jmobile.browser.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.jmobile.browser.R;
import io.jmobile.browser.data.HistoryItem;
import io.jmobile.browser.ui.adapter.HistoryAdapter;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.utils.Util;

public class HistoryFragment extends BaseFragment {

    ExpandableListView lv;
    ArrayList<HistoryItem> list;
    Map<String, List<HistoryItem>> map;
    ArrayList<String> groupNameList;
    HistoryAdapter adapter;

    LinearLayout emptyLayout;

    private HistoryFragmentListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_history;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }

    private void initView() {
        emptyLayout = fv(R.id.layout_empty);

        lv = fv(R.id.lv);
        lv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String group = groupNameList.get(groupPosition);
                String url = map.get(group).get(childPosition).getHistoryUrl();

                if (listener != null)
                    listener.onHistoryItemClick(url);
                return false;
            }
        });
        list = new ArrayList<>();
        map = new HashMap<>();
        groupNameList = new ArrayList<>();

        adapter = new HistoryAdapter(getActivity(), map, groupNameList);
        lv.setAdapter(adapter);
    }

    public void refreshList() {
        if (list == null)
            return;

        list.clear();
        list.addAll(db.getHistoryItems());

        groupNameList.clear();
        map.clear();

        for (HistoryItem item : list) {
            long temp = item.getHistoryAt();
            String date = Util.getDateString(temp);
            if (groupNameList.indexOf(date) < 0) {
                groupNameList.add(date);
                map.put(date, new ArrayList<HistoryItem>());
            }
            map.get(date).add(item);
        }

        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        emptyLayout.setVisibility(empty ? View.VISIBLE : View.GONE);

        for (int i = 0; i < groupNameList.size(); i++)
            lv.expandGroup(i);

        if (listener != null)
            listener.notiEmptyHistoryList(empty);
    }

    public void setLIstener(HistoryFragmentListener listener) {
        this.listener = listener;
    }

    public static interface HistoryFragmentListener {
        public void onHistoryItemClick(String url);

        public void notiEmptyHistoryList(boolean empty);
    }
}
