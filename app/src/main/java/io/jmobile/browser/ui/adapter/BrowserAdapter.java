package io.jmobile.browser.ui.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BrowserItem;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.image.ImageUtil;

public class BrowserAdapter extends ReSelectableAdapter<BrowserItem, BrowserAdapter.BrowserHolder> {

    private final static int TYPE_AD = 0;
    private final static int TYPE_CONTENT = 1;
    private final static int TYPE_NONE = 2;
    public static int MODE_NONE = 0;
    public static int MODE_ADS = 1;
    private Context context;
    private ArrayList<BrowserItem> items = new ArrayList<>();
    private Bitmap homeBitmap;
    private int selectedIndex = 0;
    private int mode = MODE_NONE;

    public BrowserAdapter(Context context, int layoutId, ArrayList<BrowserItem> items, BrowserAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(BrowserHolder holder, int position) {
        if (position % 9 != 1) {
            BrowserItem item = items.get(position);

//        holder.layout.setBackgroundColor(ContextCompat.getColor(context, isCheckMode() && item.isSelected() ? R.color.bookmarks_list_background_sel : R.color.bookmarks_list_background));

            holder.title.setText(item.getBrowserTitle());
            if (item.getBrowserTitle().equals("HOME")) {
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_home_n));
                holder.thumbnail.setImageBitmap(homeBitmap);
            } else if (item.getBrowserThumbnail() != null) {
                holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getBrowserThumbnail()));
                if (item.getBrowserIcon() != null)
                    holder.icon.setImageBitmap(ImageUtil.getImage(item.getBrowserIcon()));
                else
                    holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_globe_n));
            }

            if (holder.layout != null) {
                holder.layout.setBackgroundColor(ContextCompat.getColor(context, position == selectedIndex ? R.color.colorAccent : R.color.transparent));
            }
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position % 9 != 1)
            return TYPE_AD;
        else
            return TYPE_CONTENT;
    }

    @Override
    public BrowserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final AdView adAdvancedView;
        BrowserHolder holder = new BrowserHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        if (viewType == TYPE_AD) {
            adAdvancedView = new AdView(context);
            adAdvancedView.setAdSize(AdSize.FULL_BANNER);
            AdLoader adLoader = new AdLoader.Builder(context, LogUtil.DEBUG_MODE ? Common.ADMOB_LIST_NATIVE_TEST : Common.ADMOB_TAB_NATIVE_ADVANCED)
                    .forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
                        @Override
                        public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                            if (nativeAppInstallAd == null || context == null || ((Activity) context).getLayoutInflater() == null)
                                return;
                            NativeAppInstallAdView adView = (NativeAppInstallAdView) ((Activity) context).getLayoutInflater().inflate(R.layout.admob_app_install_tab, null);
                            populateAppInstallAdView(nativeAppInstallAd, adView);
                            adAdvancedView.removeAllViews();
                            ;
                            adAdvancedView.addView(adView);
                        }
                    })
                    .forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
                        @Override
                        public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                            if (nativeContentAd == null || context == null || ((Activity) context).getLayoutInflater() == null)
                                return;

                            NativeContentAdView adView = (NativeContentAdView) ((Activity) context).getLayoutInflater()
                                    .inflate(R.layout.admob_content_tab, null);
                            populateContentAdView(nativeContentAd, adView);
                            adAdvancedView.removeAllViews();
                            adAdvancedView.addView(adView);
                        }
                    })
                    .withAdListener(new com.google.android.gms.ads.AdListener() {
                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder().build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
            holder = new BrowserHolder(adAdvancedView);
        } else {
            holder.layout = holder.fv(R.id.layout_root);
            holder.thumbnail = holder.fv(R.id.image_browser_thumbnail);
            holder.icon = holder.fv(R.id.image_browser_icon);
            holder.title = holder.fv(R.id.text_browser_name);
            holder.deleteButton = holder.fv(R.id.button_browser_delete);
            holder.setOnBrowserHolderClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position) {
                        ((BrowserAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
                    }
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });
            holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position) {
                        listener.OnItemClick(position, items.get(position));
                    }
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });
        }
        return holder;
    }

    public void setSelectedIndex(int index) {
        this.selectedIndex = index;
        notifyDataSetChanged();
    }

    public void setHomeBitmap(Bitmap home) {
        this.homeBitmap = home;
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
//        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));
//        adView.setImageView(adView.findViewById(R.id.bg_image));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        if (nativeContentAd.getImages().size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    public static interface BrowserAdapterListener extends ReOnItemClickListener<BrowserItem> {
        public void onDeleteButtonClick(int position, BrowserItem item);
    }

    public class BrowserHolder extends ReAbstractViewHolder {
        LinearLayout layout;
        ImageView icon;
        TextView title;
        ImageView thumbnail;
        ImageButton deleteButton;
        private OnItemViewClickListener browserClickListenr;

        public BrowserHolder(View itemView) {
            super(itemView);
        }

        public void setOnBrowserHolderClickListener(OnItemViewClickListener listener) {
            browserClickListenr = listener;
            if (listener != null) {
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        browserClickListenr.onItemViewClick(getPosition(), BrowserHolder.this);
                    }
                });
            }
        }
    }
}
