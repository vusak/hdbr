package io.jmobile.browser.ui.adapter;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.BaseApplication;
import io.jmobile.browser.data.BrowserItem;
import io.jmobile.browser.storage.DBController;
import io.jmobile.browser.storage.SPController;
import io.jmobile.browser.ui.FrontActivity;
import io.jmobile.browser.ui.fragment.BrowserItemFragment;
import io.jmobile.browser.utils.image.ImageUtil;

public class CarouselPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {
    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.7f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private final List<BrowserItem> items;
    private FrontActivity context;
    private FragmentManager fragmentManager;
    private float scale;
    private SPController sp;
    private DBController db;
    private Bitmap homeBitmap = null;

    public CarouselPagerAdapter(FrontActivity context, FragmentManager fm, ArrayList<BrowserItem> items) {
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
        this.items = items;
        this.sp = ((BaseApplication) context.getApplication()).getSPController();
        this.db = ((BaseApplication) context.getApplication()).getDBController();
    }

//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        return super.instantiateItem(container, position);
//    }

    @Override
    public Fragment getItem(int position) {
        if (homeBitmap == null && db.getExtraHomeImage() != null)
            homeBitmap = ImageUtil.getImage(db.getExtraHomeImage());
        final BrowserItemFragment fragment = (BrowserItemFragment) BrowserItemFragment.newInstance(context, items.get(position), position, homeBitmap, false);

        return fragment;
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = items.size();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return items.size();
    }

    @Override
    public int getItemPosition(Object object) {
//        int position = items.indexOf(object);
//        return position == -1 ?  POSITION_NONE : position;
        return POSITION_NONE;
    }

//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
//        if(object instanceof Fragment){
//            Fragment fragment    = (Fragment)object;
////            FragmentManager fm    = fragment.getFragmentManager();
//            FragmentTransaction ft    = fragmentManager.beginTransaction();
//            ft.remove(fragment);
//            ft.commit();
//        }
//    }

//    public void remove(BaseClickableViewPager pager, int position) {
//        destroyItem(pager, position, getItem(position));
//    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private String getFragmentTag(int position) {
//        return "android:switcher:" + context.pager.getId() + ":" + position;
        return items.get(position).getBrowserId();
    }

    public void setHomeBitmap(Bitmap bmp) {
        this.homeBitmap = bmp;
        notifyDataSetChanged();
    }
}
