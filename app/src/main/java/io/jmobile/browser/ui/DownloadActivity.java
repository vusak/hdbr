package io.jmobile.browser.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.download.JDownloadManager;
import io.jmobile.browser.download.JNetworkMonitor;
import io.jmobile.browser.ui.base.BaseActivity;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.BaseViewPager;
import io.jmobile.browser.ui.base.ExpandLayout;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.ui.fragment.DownloadCompleteFragment;
import io.jmobile.browser.ui.fragment.DownloadReadyFragment;
import io.jmobile.browser.ui.fragment.DownloadingFragment;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.PermissionUtil;
import io.jmobile.browser.utils.file.FileUtil;

public class DownloadActivity extends BaseActivity implements JDownloadManager.DownloadListener,
        DownloadReadyFragment.DownloadReadyListener, DownloadingFragment.DownloadingListener, DownloadCompleteFragment.DownloadCompleteListener,
        JNetworkMonitor.OnChangeNetworkStatusListener {

    ImageButton backButton, hdfmButton, menuButton, menuCloseButton, closeButton, delButton, listModeButton, gridModeButton;
    ExpandLayout menuExpand;
    View closeMenuView;
    Button allDownButton, allCancelButton, allReDownButton, allPauseButton, allClearButton, selDelButton, allDelButton;
    Button sortSizeButton, sortDateButton;
    ImageView sortSizeImage, sortDateImage;
    LinearLayout sortSizeLayout, sortDateLayout;
    LinearLayout loadingLayout;
    TextView titleText, delCountText;

    DownloadReadyFragment readyFragment;
    DownloadingFragment ingFragment;
    DownloadCompleteFragment completeFragment;

    ImageButton dashboardButton;
    LinearLayout normalLayout, delLayout;
    private TabLayout tabLayout;
    private BaseViewPager viewPager;
//    TextView readyText, downloadingText, completeText;
//    LinearLayout downBanner;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(r.c(this, R.color.set_title_background));
        }
        initView();
//        initAds();
        setmenuVisible(0);

        checkExternalPermission();
        downloadManager.addListener(this);
        networkMonitor.setListener(this);
    }


    @Override
    public void onBackPressed() {
        if (menuExpand != null && menuExpand.isExpanded())
            visibleMenu(false);
        else {
//            super.onBackPressed();
//            Intent intent = new Intent(DownloadActivity.this, FrontActivity.class);
//            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onDownloadError(DownloadItem item, int errorCode) {
        if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1) {
            ingFragment.updateProgress(item);
        }
    }

    @Override
    public void onDownloadProgress(DownloadItem item, long progress, long total) {
        if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1) {
            ingFragment.updateProgress(item);
            ingFragment.updateDownloadProgressUI();
        }
    }

    @Override
    public void onDownloadStart(DownloadItem item) {
        if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1) {
            ingFragment.insertItem(item);
            ingFragment.updateDownloadProgressUI();
        }
    }

    @Override
    public void onDownloadSuccess(DownloadItem item) {
        if (item == null)
            return;

        if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1) {
            ingFragment.removeItem(item.getDownloadId());
            ingFragment.updateDownloadProgressUI();
        }

//        if (completeFragment != null && tabLayout.getSelectedTabPosition() == 2)
//            completeFragment.refreshLIst();
    }

    @Override
    public void onReadyItemClicked(DownloadItem item) {
        item.setDownloadState(DownloadItem.DOWNLOAD_STATE_READY);
        db.updateDownloadState(item);
        downloadManager.addToRequestList(item.getDownloadId());
    }

    @Override
    public void onShowAlertDialog(String message) {
        showAlertMessageDialog(message);
    }

    @Override
    public void onDownloadingItemClicked(DownloadItem item) {

        if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DOWNLOADING) {
            downloadManager.cancelDownload(item.getDownloadId(), item);
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_PAUSE);

        } else {
            downloadManager.addToRequestList(item.getDownloadId());
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_READY);

        }
        db.updateDownloadState(item);
    }

    @Override
    public void onDownloadedItemClicked(DownloadItem item) {
        String url = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);

        File video = new File(url);
        if (!video.exists())
            return;

        Uri uri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            uri = FileProvider.getUriForFile(this, "io.jmobile.browser.fileprovider", new File(url));
        else
            uri = Uri.fromFile(video);
        intent.setDataAndType(uri, "video/*");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException e) {
            Toast.makeText(getBaseContext(), "Failed...", Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Toast.makeText(getBaseContext(), "Failed...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void notiEmptyReadyList(boolean empty) {
        if (tabLayout.getSelectedTabPosition() != 0)
            return;

        menuButton.setVisibility(empty ? View.GONE : View.VISIBLE);
        hdfmButton.setVisibility(View.GONE);
        gridModeButton.setVisibility(View.GONE);
        listModeButton.setVisibility(View.GONE);
    }

    @Override
    public void notiEmptyDownloadingList(boolean empty) {
        if (tabLayout.getSelectedTabPosition() != 1)
            return;

        menuButton.setVisibility(empty ? View.GONE : View.VISIBLE);
        hdfmButton.setVisibility(View.GONE);
        gridModeButton.setVisibility(View.GONE);
        listModeButton.setVisibility(View.GONE);
    }

    @Override
    public void notiEmptyCompleteList(boolean empty) {
        if (tabLayout.getSelectedTabPosition() != 2)
            return;

        if (empty) {
            setDeleteMode(false);
        }
        menuButton.setVisibility(empty ? View.GONE : View.VISIBLE);
//        listModeButton.setVisibility(!empty && !completeFragment.isListMode()? View.VISIBLE : View.GONE);
//        gridModeButton.setVisibility(!empty && completeFragment.isListMode()? View.VISIBLE : View.GONE);
        listModeButton.setVisibility(!empty && !sp.isDownloadListMode() ? View.VISIBLE : View.GONE);
        gridModeButton.setVisibility(!empty && sp.isDownloadListMode() ? View.VISIBLE : View.GONE);
        hdfmButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCheckChanged(int count) {
        if (count <= 0)
            delCountText.setText("");
        else
            delCountText.setText(count + " " + r.s(R.string.del_selected_title));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.REQUEST_STORAGE) {
            if (PermissionUtil.verifyPermission(grantResults)) {
            } else {
                Toast.makeText(this, "권한을 받지 못했습니다", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onNetworkChanged(int status) {
        if (downloadManager == null || downloadManager.isEmpty())
            return;

        if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
            showAlertMessageDialog(r.s(R.string.dlg_msg_check_network));
            if (downloadManager != null)
                downloadManager.cancelAll(null);
            downloadManager.cancelAll(null);
            db.restateDownloadState();

            if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1)
                ingFragment.refreshLIst();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE && sp.isWifiOnly()) {
            showAlertMessageDialog(r.s(R.string.dlg_msg_check_wifi));
            if (downloadManager != null)
                downloadManager.cancelAll(null);
            downloadManager.cancelAll(null);
            db.restateDownloadState();

            if (ingFragment != null && tabLayout.getSelectedTabPosition() == 1)
                ingFragment.refreshLIst();
        }
    }

    private void initView() {
//        emptyLayout = fv(R.id.layout_empty);
//        downListLayout = fv(R.id.layout_list_down);

        titleText = fv(R.id.text_top_title);
        titleText.setText(r.s(R.string.down_text_title));

        dashboardButton = fv(R.id.btn_dashboard);
        dashboardButton.setVisibility(View.VISIBLE);
        dashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DownloadActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        backButton = fv(R.id.btn_top_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hdfmButton = fv(R.id.btn_top_hdfm);
        hdfmButton.setVisibility(View.VISIBLE);
        hdfmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHDF();
            }
        });

        listModeButton = fv(R.id.btn_top_list_mode);
        listModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listModeButton.setVisibility(View.GONE);
                gridModeButton.setVisibility(View.VISIBLE);
                if (completeFragment != null)
                    completeFragment.setViewMode(false);
            }
        });
        listModeButton.setVisibility(View.GONE);

        gridModeButton = fv(R.id.btn_top_grid_mode);
        gridModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridModeButton.setVisibility(View.GONE);
                listModeButton.setVisibility(View.VISIBLE);
                if (completeFragment != null)
                    completeFragment.setViewMode(true);
            }
        });
        gridModeButton.setVisibility(View.GONE);

        menuExpand = fv(R.id.layout_expend_menu);
        menuExpand.collapse(false);
        closeMenuView = fv(R.id.view_close_menu);
        closeMenuView.setVisibility(View.GONE);
        closeMenuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
            }
        });
        menuButton = fv(R.id.btn_top_menu);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(true);
            }
        });

        menuCloseButton = fv(R.id.btn_menu_close);
        menuCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
            }
        });

        allDownButton = fv(R.id.btn_menu_download_all);
        allDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);

                if (readyFragment != null) {
                    ArrayList<DownloadItem> ready = readyFragment.getReadyList();
                    if (ready != null && ready.size() > 0) {
                        for (DownloadItem item : ready) {
                            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_READY);
                            db.updateDownloadState(item);
                            downloadManager.addToRequestList(item.getDownloadId());
                        }
                        readyFragment.refreshLIst();
                    }
                }
            }
        });

        allClearButton = fv(R.id.btn_menu_clear_all);
        allClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);

                if (readyFragment != null) {
                    ArrayList<DownloadItem> ready = readyFragment.getReadyList();
                    if (ready != null && ready.size() > 0) {
                        db.deleteAllNonDownloadItems();
                        readyFragment.refreshLIst();
                    }
                }

            }
        });

        allReDownButton = fv(R.id.btn_menu_re_down_all);
        allReDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);

                if (ingFragment != null) {
                    ArrayList<DownloadItem> ingItems = ingFragment.getDownloadingItems();
                    if (ingItems != null && ingItems.size() > 0) {
                        for (DownloadItem item : ingItems) {
                            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_READY);
                            db.updateDownloadState(item);
                            downloadManager.addToRequestList(item.getDownloadId());
                        }
                        ingFragment.refreshLIst();
                    }
                }
            }
        });

        allPauseButton = fv(R.id.btn_menu_pause_all);
        allPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);

                downloadManager.cancelAll(null);
                db.restateDownloadState();
                if (ingFragment != null)
                    ingFragment.refreshLIst();
            }
        });

        allCancelButton = fv(R.id.btn_menu_cancel_all);
        allCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);

                downloadManager.cancelAll(null);
                if (ingFragment != null) {
                    ArrayList<DownloadItem> list = ingFragment.getDownloadingItems();
                    if (list != null && list.size() > 0)
                        db.deleteDownloadItems(list);
                    ingFragment.refreshLIst();
                }
            }
        });

        selDelButton = fv(R.id.btn_menu_del_sel);
        selDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDeleteMode(true);
            }
        });

        allDelButton = fv(R.id.btn_menu_del_all);
        allDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
                if (fdf(Common.TAG_DIALOG_DELETE_ALL_ITEMS) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_DELETE_ALL_ITEMS);
                    dlg.setTitle(r.s(R.string.dlg_title_delete));
                    dlg.setMessage(r.s(R.string.dlg_msg_delete_all_downloaded_list));
                    dlg.setNegativeLabel(r.s(R.string.cancel));
                    dlg.setPositiveLabel(r.s(R.string.delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            if (completeFragment != null) {
                                completeFragment.deleteItems(true);
                                completeFragment.refreshLIst();
                            }
                        }
                    });
                    sdf(dlg);
                }

            }
        });

        sortSizeImage = fv(R.id.image_menu_sort_size);
        sortSizeButton = fv(R.id.btn_menu_sort_size);
        sortSizeLayout = fv(R.id.layout_menu_sort_size);
        sortSizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
                if (sp.isDownloadListSortSizeAsc()) {
                    sp.setDownloadListSort(DownloadItem.DOWNLOAD_SORT_SIZE_DESC);
                    sortSizeImage.setImageResource(R.drawable.ic_arrow_upward_gray_18);
                } else {
                    sp.setDownloadListSort(DownloadItem.DOWNLOAD_SORT_SIZE_ASC);
                    sortSizeImage.setImageResource(R.drawable.ic_arrow_down_gray_18);
                }
                sp.setDownloadListSortSizeAsc(!sp.isDownloadListSortSizeAsc());
                readyFragment.sortDownloadList();
            }
        });

        sortDateImage = fv(R.id.image_menu_sort_date);
        sortDateButton = fv(R.id.btn_menu_sort_date);
        sortDateLayout = fv(R.id.layout_menu_sort_date);
        sortDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
                if (sp.isDownloadListSortDateAsc()) {
                    sp.setDownloadListSort(DownloadItem.DOWNLOAD_SORT_DATE_DESC);
                    sortDateImage.setImageResource(R.drawable.ic_arrow_upward_gray_18);
                } else {
                    sp.setDownloadListSort(DownloadItem.DOWNLOAD_SORT_DATE_ASC);
                    sortDateImage.setImageResource(R.drawable.ic_arrow_down_gray_18);
                }
                sp.setDownloadListSortDateAsc(!sp.isDownloadListSortDateAsc());
                readyFragment.sortDownloadList();
            }
        });

        closeButton = fv(R.id.btn_top_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delButton.getVisibility() == View.VISIBLE)
                    setDeleteMode(false);
            }
        });

        delCountText = fv(R.id.text_top_del_count);

        delButton = fv(R.id.btn_top_delete);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItems();
            }
        });

        normalLayout = fv(R.id.layout_normal);
        delLayout = fv(R.id.layout_delete);

        viewPager = fv(R.id.viewpager);
        viewPager.setEnableSwipe(false);
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);

        tabLayout = fv(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setmenuVisible(tab.getPosition());
                if (menuExpand.isExpanded())
                    menuExpand.collapse(false);
                setDeleteMode(false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        loadingLayout = fv(R.id.layout_loading);
//        loadingLayout.setVisibility(View.GONE);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        readyFragment = new DownloadReadyFragment();
        readyFragment.setListener(this);
        ingFragment = new DownloadingFragment();
        ingFragment.setListener(this);
        completeFragment = new DownloadCompleteFragment();
        completeFragment.setListener(this);
        adapter.addFrag(readyFragment, r.s(R.string.down_text_ready));
        adapter.addFrag(ingFragment, r.s(R.string.down_text_downloading));
        adapter.addFrag(completeFragment, r.s(R.string.down_text_completed));
        viewPager.setAdapter(adapter);

    }

    private void deleteItems() {
        if (delCountText.getText().toString().isEmpty()) {
            if (fdf("no_select") == null) {
                MessageDialog dlg = MessageDialog.newInstance("no_select");
                dlg.setTitle(r.s(R.string.dlg_title_delete));
                dlg.setMessage(r.s(R.string.dlg_msg_no_selected_item));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }
            return;
        }

        if (fdf("delete_selected_items") == null) {
            MessageDialog dlg = MessageDialog.newInstance("delete_selected_items");
            dlg.setTitle(r.s(R.string.dlg_title_delete));
            dlg.setMessage((r.s(R.string.dlg_msg_delete_item)));
            dlg.setNegativeLabel(r.s(R.string.cancel));
            dlg.setPositiveLabel(r.s(R.string.delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    completeFragment.deleteItems(false);
                }
            });
            sdf(dlg);
        }
    }

    private void setmenuVisible(int position) {
        boolean isReady = position == 0;
        boolean isIng = position == 1;
        boolean isComplete = position == 2;

        allDownButton.setVisibility(isReady ? View.VISIBLE : View.GONE);
        allClearButton.setVisibility(isReady ? View.VISIBLE : View.GONE);
        allReDownButton.setVisibility(isIng ? View.VISIBLE : View.GONE);
        allPauseButton.setVisibility(isIng ? View.VISIBLE : View.GONE);
        allCancelButton.setVisibility(isIng ? View.VISIBLE : View.GONE);
        selDelButton.setVisibility(isComplete ? View.VISIBLE : View.GONE);
        allDelButton.setVisibility(isComplete ? View.VISIBLE : View.GONE);
        sortSizeLayout.setVisibility(isReady ? View.VISIBLE : View.GONE);
        sortDateLayout.setVisibility(isReady ? View.VISIBLE : View.GONE);
        sortSizeImage.setImageResource(sp.isDownloadListSortSizeAsc() ? R.drawable.ic_arrow_down_gray_18 : R.drawable.ic_arrow_upward_gray_18);
        sortDateImage.setImageResource(sp.isDownloadListSortDateAsc() ? R.drawable.ic_arrow_down_gray_18 : R.drawable.ic_arrow_upward_gray_18);
//        downBanner.setVisibility(isComplete ? View.VISIBLE : View.GONE);
//        hdfmButton.setVisibility(isComplete ? View.VISIBLE : View.GONE);
//        listModeButton.setVisibility(isComplete && !completeFragment.isListMode()? View.VISIBLE : View.GONE);
//        gridModeButton.setVisibility(isComplete && completeFragment.isListMode()? View.VISIBLE : View.GONE);
//        if (isReady) readyFragment.refreshLIst();
//        if (isIng) ingFragment.refreshLIst();
//        if (isComplete) completeFragment.refreshLIst();
    }

    @SuppressLint("NewApi")
    private void setDeleteMode(boolean isMode) {
        normalLayout.setVisibility(isMode ? View.GONE : View.VISIBLE);
        delLayout.setVisibility(isMode ? View.VISIBLE : View.GONE);
        delLayout.setBackgroundColor(r.c(this, R.color.main_del));
        delCountText.setText("");
        delButton.setVisibility(View.VISIBLE);

        if (isMode) {
            menuExpand.collapse(true);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().setStatusBarColor(r.c(this, R.color.main_del));
            }
            tabLayout.setBackgroundColor(r.c(this, R.color.main_del));
            tabLayout.setTabTextColors(r.c(this, R.color.bookmarks_tab_del_text), r.c(this, R.color.white));

            if (completeFragment != null)
                completeFragment.selectModeClick(true);
        } else {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().setStatusBarColor(r.c(this, R.color.set_title_background));
            }
            tabLayout.setBackgroundColor(r.c(this, R.color.set_title_background));
            tabLayout.setTabTextColors(r.c(this, R.color.bookmarks_tab_text), r.c(this, R.color.colorAccent));

            if (completeFragment != null)
                completeFragment.selectModeClick(false);
        }
    }

    private void checkExternalPermission() {
        if (PermissionUtil.checkSelfPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || PermissionUtil.checkSelfPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
        } else
            PermissionUtil.requestExternalPermission(this);
    }

    private void visibleMenu(boolean show) {
        if (show)
            menuExpand.expand(true);
        else
            menuExpand.collapse(true);
        closeMenuView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void launchHDF() {
        try {
            if (getPackageList()) {
                Intent intent = getPackageManager()
                        .getLaunchIntentForPackage("myfilemanager.jiran.com.myfilemanager");
                intent.putExtra("path", Common.getVideoDir(this));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=myfilemanager.jiran.com.myfilemanager");
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=myfilemanager.jiran.com.myfilemanager");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    public boolean getPackageList() {
        boolean isExist = false;

        PackageManager pkgMgr = getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith("myfilemanager.jiran.com.myfilemanager")) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    public void showAlertMessageDialog(String msg) {
        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setTitle(r.s(R.string.download));
            dlg.setPositiveLabel(r.s(R.string.ok));
            dlg.setMessage(msg);

            sdf(dlg);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

//    private void initAds() {
//        downBanner = fv(R.id.layout_down_banner);
//        AdView admobBanner = new AdView(this);
//        admobBanner.setAdSize(AdSize.BANNER);
//        admobBanner.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_BANNER_OPT_TEST : Common.ADMOB_MAIN_BANNER_OPT);
//        admobBanner.setBackgroundColor(Color.TRANSPARENT);
//        admobBanner.loadAd(new AdRequest.Builder().build());
//        admobBanner.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                LogUtil.log("fanBanner DownloadActivity onAdLoaded()");
//            }
//
//            @Override
//            public void onAdFailedToLoad(int i) {
//                LogUtil.log("fanBanner DownloadActivity onAdFailedToLoad()" + 1);
//            }
//        });
//        downBanner.addView(admobBanner);
//    }

}
