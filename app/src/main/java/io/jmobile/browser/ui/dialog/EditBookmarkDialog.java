package io.jmobile.browser.ui.dialog;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.UUID;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.data.QuickDialItem;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.utils.Util;

public class EditBookmarkDialog extends BaseDialogFragment {
    public static final String TAG = EditBookmarkDialog.class.getSimpleName();

    Context context;
    BookmarkItem item;
    QuickDialItem quickItem;

    TextView dlgTitleText;
    EditText titleEdit, urlEdit;
    TextView tText, uText, titleText, urlText;
    ImageView titleImage, urlImage;
    ImageButton titleClearButton, urlClearButton;
    View titleLine, urlLine;

    Button cancelButton, saveButton;

    private EditBookmarkDialogListener listener;

    public static EditBookmarkDialog newInstance(String tag, Context context, BookmarkItem item) {
        EditBookmarkDialog d = new EditBookmarkDialog();

        d.context = context;
        d.item = item;
        d.createArguments(tag);
        return d;
    }

    public static EditBookmarkDialog newInstance(String tag, Context context, QuickDialItem item) {
        EditBookmarkDialog d = new EditBookmarkDialog();

        d.context = context;
        d.quickItem = item;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_bookmark_edit;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        dlgTitleText = fv(R.id.text_dlg_title);

        tText = fv(R.id.text_title_name);
        titleEdit = fv(R.id.edit_title);
        titleEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                titleText.setVisibility(empty ? View.VISIBLE : View.GONE);
                titleClearButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        titleEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                tText.setTextColor(r.c(context, hasFocus ? R.color.colorAccent : R.color.bookmarks_empty_sub));
                titleLine.setBackgroundColor(r.c(context, hasFocus ? R.color.colorAccent : R.color.bookmarks_empty_sub));
                titleText.setText(r.s(R.string.dlg_edit_bookmark_title_hint));
                titleText.setTextColor(r.c(context, R.color.bookmarks_empty_sub));
                titleText.setVisibility(TextUtils.isEmpty(titleEdit.getText().toString()) ? View.VISIBLE : View.GONE);
                titleImage.setVisibility(View.GONE);
                titleClearButton.setVisibility(TextUtils.isEmpty(titleEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        titleText = fv(R.id.text_warning_title);
        titleImage = fv(R.id.image_warning_title);
        titleLine = fv(R.id.line_title);
        titleClearButton = fv(R.id.button_clear_title);
        titleClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleEdit.setText("");
            }
        });

        uText = fv(R.id.text_url_name);
        urlEdit = fv(R.id.edit_url);
        urlEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                urlText.setVisibility(empty ? View.VISIBLE : View.GONE);
                urlClearButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        urlEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                uText.setTextColor(r.c(context, hasFocus ? R.color.colorAccent : R.color.bookmarks_empty_sub));
                urlLine.setBackgroundColor(r.c(context, hasFocus ? R.color.colorAccent : R.color.bookmarks_empty_sub));
                urlText.setText(r.s(R.string.dlg_edit_bookmark_url_hint));
                urlText.setTextColor(r.c(context, R.color.bookmarks_empty_sub));
                urlText.setVisibility(TextUtils.isEmpty(urlEdit.getText().toString()) ? View.VISIBLE : View.GONE);
                urlImage.setVisibility(View.GONE);
                urlClearButton.setVisibility(!hasFocus || TextUtils.isEmpty(urlEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });
        urlText = fv(R.id.text_warning_url);
        urlImage = fv(R.id.image_warning_url);
        urlLine = fv(R.id.line_url);
        urlClearButton = fv(R.id.button_clear_url);
        urlClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlEdit.setText("");
            }
        });

        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCancelButtonClick();

                Util.hideKeyBoard(urlEdit);
                Util.hideKeyBoard(titleEdit);
                dismiss();
            }
        });
        saveButton = fv(R.id.button_positive);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(titleEdit);
                Util.hideKeyBoard(urlEdit);

                if (TextUtils.isEmpty(titleEdit.getText().toString()) || TextUtils.isEmpty(urlEdit.getText().toString())) {
                    setWarning();
                    return;
                }
                if (listener != null) {
                    String url = urlEdit.getText().toString().trim();
                    if (!url.contains("http://") || !url.contains("https://"))
                        url = "http://" + url;

                    if (quickItem != null) {
                        quickItem.setQuickDialAt(System.currentTimeMillis());
                        quickItem.setquickUrl(url);
                        quickItem.setQuickDialName(titleEdit.getText().toString().trim());
                        listener.onSaveButtonClick(quickItem);
                    } else {
                        if (item == null) {
                            item = new BookmarkItem();
                            item.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                            item.setBookmarkShowing(false);
                            item.setBookmarkOrder((int) db.getNextDBId(BookmarkItem.TABLE_NAME));
                            item.setBookmarkAt(System.currentTimeMillis());
                        }

                        item.setBookmarkUrl(url);
                        item.setBookmarkName(titleEdit.getText().toString().trim());
                        listener.onSaveButtonClick(item);
                    }
                }
                dismiss();
            }
        });

        urlEdit.clearFocus();

        if (quickItem != null)
            dlgTitleText.setText(r.s(R.string.dlg_add_url_title));
        else
            dlgTitleText.setText(r.s(item == null ? R.string.dlg_add_bookmark_title : R.string.dlg_edit_bookmark_title));

        if (item != null) {
            titleEdit.setText(item.getBookmarkName());
            urlEdit.setText(item.getBookmarkUrl());
        }
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    public EditBookmarkDialog setListener(EditBookmarkDialogListener listener) {
        this.listener = listener;

        return this;
    }

    private void setWarning() {
        boolean tEmpty = TextUtils.isEmpty(titleEdit.getText().toString());
        boolean uEmpty = TextUtils.isEmpty(urlEdit.getText().toString());

        if (tEmpty) {
            titleText.setText(r.s(R.string.dlg_edit_bookmark_title_required));
            titleText.setTextColor(r.c(context, R.color.bookmark_requred_text));
            titleLine.setBackgroundColor(r.c(context, R.color.bookmark_requred_text));
            titleImage.setVisibility(View.VISIBLE);
            titleEdit.clearFocus();
        }

        if (uEmpty) {
            urlText.setText(r.s(R.string.dlg_edit_bookmark_url_required));
            urlText.setTextColor(r.c(context, R.color.bookmark_requred_text));
            urlLine.setBackgroundColor(r.c(context, R.color.bookmark_requred_text));
            urlImage.setVisibility(View.VISIBLE);
            urlEdit.clearFocus();
        }
    }

    public static interface EditBookmarkDialogListener {
        public void onSaveButtonClick(BookmarkItem item);

        public void onSaveButtonClick(QuickDialItem item);

        public void onCancelButtonClick();
    }
}
