package io.jmobile.browser.ui.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import io.jmobile.browser.BaseApplication;
import io.jmobile.browser.storage.DBController;
import io.jmobile.browser.storage.SPController;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;

/**
 * Created by Cecil on 2017-05-30.
 */

public abstract class BaseDialogFragment extends DialogFragment {
    protected static final String DIALOG_TAG = "DIALOG_TAG";
    private static final String DISPATCH_TO_ACTIVITY_ON_BACK_PRESSED = "dispatch_to_activity_on_back_pressed";

    protected BaseApplication app;
    protected SPController sp;
    protected DBController db;
    protected BaseApplication.ResourceWrapper r;

    protected DialogDismissListener dismissListener;
    protected DialogPositiveListener positiveListener;
    protected DialogNegativeListener negativeListener;

    protected String tag;

    protected TextView dialogTitleText;
    protected View dialogCloseButton;
    protected boolean byCloseButton = false;
    private View v;

    private boolean dispatchToActivityOnBackPressed = false;
    private boolean softCancel;

    public abstract int getLayoutId();

    public abstract void onCreateView(Bundle savedInstanceState);

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Fragment f = getTargetFragment();
        if (getTargetFragment() != null) {
            if (f instanceof DialogDismissListener && dismissListener == null)
                dismissListener = (DialogDismissListener) f;

            if (f instanceof DialogPositiveListener && positiveListener == null)
                positiveListener = (DialogPositiveListener) f;

            if (f instanceof DialogNegativeListener && negativeListener == null)
                negativeListener = (DialogNegativeListener) f;
        } else {
            if (activity instanceof DialogDismissListener && dismissListener == null)
                dismissListener = (DialogDismissListener) activity;

            if (activity instanceof DialogPositiveListener && positiveListener == null)
                positiveListener = (DialogPositiveListener) activity;

            if (activity instanceof DialogNegativeListener && negativeListener == null)
                negativeListener = (DialogNegativeListener) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (BaseApplication) getActivity().getApplication();
        sp = app.getSPController();
        db = app.getDBController();
        r = app.getResourceWrapper();

        Bundle arg = getArguments();

        if (savedInstanceState != null)
            dispatchToActivityOnBackPressed = savedInstanceState.getBoolean(DISPATCH_TO_ACTIVITY_ON_BACK_PRESSED, false);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(getLayoutId(), container, false);

        // dialogTitleText = fv(R.id.text_dialogTitle);
        //
        // dialogCloseButton = fv(R.id.button_closeDialog);
        // if (dialogCloseButton != null)
        // dialogCloseButton.setOnClickListener(new OnClickListener() {
        // @Override
        // public void onClick(View v) {
        // if (dispatchToActivityOnBackPressed) {
        // getActivity().onBackPressed();
        // } else {
        // byCloseButton = true;
        // dismiss();
        // }
        // }
        // });
        //
        onCreateView(savedInstanceState);

        return v;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                    if (dispatchToActivityOnBackPressed)
                        getActivity().onBackPressed();

                return false;
            }
        });

        return d;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(DISPATCH_TO_ACTIVITY_ON_BACK_PRESSED, dispatchToActivityOnBackPressed);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (dismissListener != null)
            dismissListener.onDialogDismiss(this, tag, byCloseButton);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        hideKeyboard();
        super.onCancel(dialog);
    }

    @Override
    public void dismiss() {
        hideKeyboard();
        try {
            super.dismiss();
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = 600;
        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public void dismissAllowingStateLoss() {
        hideKeyboard();

        try {
            super.dismissAllowingStateLoss();
        } catch (Exception e) {
        }
    }

    @Override
    public void setCancelable(boolean cancelable) {
        super.setCancelable(cancelable);
        dispatchToActivityOnBackPressed = !cancelable;
    }

    public void setCancelable(boolean cancelable, boolean dispatchToActivityOnBackPressed) {
        super.setCancelable(cancelable);
        this.dispatchToActivityOnBackPressed = dispatchToActivityOnBackPressed;
    }

    public void setPositiveListener(DialogPositiveListener listener) {
        positiveListener = listener;
    }

    public void setNegativeListener(DialogNegativeListener listener) {
        negativeListener = listener;
    }

    public void setDismissListener(DialogDismissListener listener) {
        dismissListener = listener;
    }

    public String getDialogTag() {
        return tag;
    }

    protected Bundle createArguments(String tag) {
        if (tag == null)
            tag = getDefaultTag();
        this.tag = tag;

        Bundle b = new Bundle();
        b.putString(DIALOG_TAG, tag);
        setArguments(b);

        return b;
    }

    protected <T extends BaseFragment> T ff(String tag) {
        return Util.ff(getFragmentManager(), tag);
    }

    protected <T extends BaseDialogFragment> T fdf(String tag) {
        return Util.fdf(getFragmentManager(), tag);
    }

    protected void hdf(String tag) {
        Util.hdf(getFragmentManager(), tag);
    }

    protected void sdf(BaseDialogFragment d) {
        Util.sdf(getFragmentManager(), d);
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T fv(int id) {
        try {
            return (T) v.findViewById(id);
        } catch (ClassCastException e) {
            return null;
        }
    }

    protected void log(String msg) {
        LogUtil.log(getClass().getSimpleName(), msg);
    }

    protected void log(Throwable tr) {
        LogUtil.log(getClass().getSimpleName(), tr);
    }

    protected EditText[] getEditTexts() {
        return null;
    }

    private String getDefaultTag() {
        return getClass().getSimpleName();
    }

    private void hideKeyboard() {
        EditText[] list = getEditTexts();
        if (list != null)
            for (EditText edit : list)
                if (edit != null)
                    Util.hideKeyBoard(edit);
    }

    public static interface DialogDismissListener {
        public void onDialogDismiss(BaseDialogFragment dialog, String tag, boolean byCloseButton);
    }

    public static interface DialogNegativeListener {
        public void onDialogNegative(BaseDialogFragment dialog, String tag);
    }

    public static interface DialogPositiveListener {
        public void onDialogPositive(BaseDialogFragment dialog, String tag);
    }
}
