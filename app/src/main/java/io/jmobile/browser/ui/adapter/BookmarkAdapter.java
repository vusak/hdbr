package io.jmobile.browser.ui.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.image.ImageUtil;

public class BookmarkAdapter extends ReSelectableAdapter<BookmarkItem, BookmarkAdapter.BookmarkHolder> {
    private final static int TYPE_AD = 0;
    private final static int TYPE_CONTENT = 1;
    private final static int TYPE_NONE = 2;
    public static int MODE_NONE = 0;
    public static int MODE_EDIT = 1;
    public static int MODE_ADD_QUICK = 2;
    public static int MODE_DELETE = 3;
    private Context context;
    private ArrayList<BookmarkItem> items = new ArrayList<>();
    private int mode = MODE_NONE;

    public BookmarkAdapter(Context context, int layoutId, ArrayList<BookmarkItem> items, BookmarkAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
    }

    public int getMode() {
        return this.mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public BookmarkItem getItem(int position) {
        return items.get(position);

    }

    @Override
    public void onBindViewHolder(BookmarkHolder holder, int position) {
//        if (mode != MODE_ADD_QUICK || position % 10 != 7) {
//        if (position % 10 != 7) {
            BookmarkItem item = items.get(position);
            if (isCheckMode())
                mode = MODE_NONE;

            holder.layout.setBackgroundColor(ContextCompat.getColor(context, isCheckMode() && item.isSelected() ? R.color.bookmarks_list_background_sel : R.color.bookmarks_list_background));
            holder.check.setVisibility(isCheckMode() ? View.VISIBLE : View.GONE);
            holder.check.setChecked(item.isSelected());

            holder.titleText.setText(item.getBookmarkName());

            if (item.getBookmarkIcon() != null)
                holder.icon.setImageBitmap(ImageUtil.getImage(item.getBookmarkIcon()));
            else
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_default_app_50));
            holder.editButton.setVisibility(mode == MODE_EDIT ? View.VISIBLE : View.GONE);
            holder.moveButton.setVisibility(mode == MODE_EDIT ? View.VISIBLE : View.GONE);
            holder.quickButton.setVisibility(mode == MODE_ADD_QUICK ? View.VISIBLE : View.GONE);
            holder.quickButton.setImageDrawable(ContextCompat.getDrawable(context, item.isBookmarkShowing() ? R.drawable.btn_add_check : R.drawable.btn_plus_add));
            holder.line.setVisibility(position == items.size() - 1 ? View.INVISIBLE : View.VISIBLE);
//        }
    }

    @Override
    public int getItemViewType(int position) {
//        if (mode == MODE_ADD_QUICK && position % 10 == 7)
//        if (position % 10 == 7)
//            return TYPE_AD;
//        else
            return TYPE_CONTENT;
    }

    @Override
    public BookmarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final AdView adAdvancedView;
        BookmarkHolder holder = new BookmarkHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        if (viewType == TYPE_AD) {
            adAdvancedView = new AdView(context);
            adAdvancedView.setAdSize(AdSize.SMART_BANNER);
            int height = (int) Util.convertDpToPixel(56, context);
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, height);
            adAdvancedView.setLayoutParams(params);
            AdLoader adLoader = new AdLoader.Builder(context, LogUtil.DEBUG_MODE ? Common.ADMOB_LIST_NATIVE_TEST : Common.ADMOB_LIST_QUICKMARK_NATIVE)
                    .forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
                        @Override
                        public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                            if (nativeAppInstallAd == null || context == null || ((Activity) context).getLayoutInflater() == null)
                                return;
                            NativeAppInstallAdView adView = (NativeAppInstallAdView) ((Activity) context).getLayoutInflater().inflate(R.layout.admob_app_install, null);
                            populateAppInstallAdView(nativeAppInstallAd, adView);
                            adAdvancedView.removeAllViews();
                            ;
                            adAdvancedView.addView(adView);
                        }
                    })
                    .forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
                        @Override
                        public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                            if (nativeContentAd == null || context == null || ((Activity) context).getLayoutInflater() == null)
                                return;

                            NativeContentAdView adView = (NativeContentAdView) ((Activity) context).getLayoutInflater()
                                    .inflate(R.layout.admob_content, null);
                            populateContentAdView(nativeContentAd, adView);
                            adAdvancedView.removeAllViews();
                            adAdvancedView.addView(adView);
                        }
                    })
                    .withAdListener(new com.google.android.gms.ads.AdListener() {
                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder().build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
            holder = new BookmarkHolder(adAdvancedView);
        } else {
            holder.layout = holder.fv(R.id.layout_list);
            holder.check = holder.fv(R.id.checkbox);
            holder.icon = holder.fv(R.id.image_icon);
            holder.titleText = holder.fv(R.id.text_title);
            holder.moveButton = holder.fv(R.id.button_move);
            holder.editButton = holder.fv(R.id.button_edit);
            holder.line = holder.fv(R.id.line_bookmark);
            holder.quickButton = holder.fv(R.id.button_quick);
            holder.setOnBookmarkClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position) {
                        ((BookmarkAdapterListener) listener).onEditBookmarkClick(position, items.get(position));
                    }
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });
            holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position) {
                        listener.OnItemClick(position, items.get(position));
                        if (isCheckMode())
                            notifyItemChanged(position);
                    }
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });
        }

//    @Override
//    public BookmarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        BookmarkHolder holder = new BookmarkHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));
//
//        holder.layout = holder.fv(R.id.layout_list);
//        holder.check = holder.fv(R.id.checkbox);
//        holder.icon = holder.fv(R.id.image_icon);
//        holder.titleText = holder.fv(R.id.text_title);
//        holder.moveButton = holder.fv(R.id.button_move);
//        holder.editButton = holder.fv(R.id.button_edit);
//        holder.line = holder.fv(R.id.line_bookmark);
//        holder.quickButton = holder.fv(R.id.button_quick);


        return holder;
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
//        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));
//        adView.setImageView(adView.findViewById(R.id.bg_image));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
//        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
//        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
//        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

//        if (nativeContentAd.getImages().size() > 0) {
//            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
//        }

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    public static interface BookmarkAdapterListener extends ReOnItemClickListener<BookmarkItem> {
        public void onEditBookmarkClick(int position, BookmarkItem item);
    }

    public class BookmarkHolder extends ReAbstractViewHolder {
        LinearLayout layout;
        CheckBox check;
        ImageView icon;
        TextView titleText;
        ImageButton quickButton;
        ImageButton editButton;
        ImageButton moveButton;
        View line;
        private OnItemViewClickListener bookmarkClickListener;

        public BookmarkHolder(View itemView) {
            super(itemView);
        }

        public void setOnBookmarkClickListener(OnItemViewClickListener listener) {
            bookmarkClickListener = listener;
            if (listener != null) {
                editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bookmarkClickListener.onItemViewClick(getPosition(), BookmarkHolder.this);
                    }
                });
            }
        }
    }
}
