package io.jmobile.browser.ui.dialog;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioGroup;

import io.jmobile.browser.R;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.utils.Util;

public class ClearHistoryDialog extends BaseDialogFragment {
    public static final int CLEAR_PAST_HOUR = R.id.rbtn_clear_hour;
    public static final int CLEAR_PAST_DAY = R.id.rbtn_clear_day;
    public static final int CLEAR_PAST_WEEK = R.id.rbtn_clear_week;
    public static final int CLEAR_PAST_MONTH = R.id.rbtn_clear_month;
    public static final int CLEAR_PAST_BEGINNING = R.id.rbtn_clear_beginning;

    public static final String TAG = ClearHistoryDialog.class.getSimpleName();

    Button cancelButton, deleteButton;
    RadioGroup radioGroup;

    private ClearHistoryDialogListener listener;

    public static ClearHistoryDialog newInstance(String tag) {
        ClearHistoryDialog d = new ClearHistoryDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_clear_history;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        cancelButton = fv(R.id.btn_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCancelButtonClick();

                dismiss();
            }
        });

        deleteButton = fv(R.id.btn_delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onDeleteButtonClick(radioGroup.getCheckedRadioButtonId());
                }

                dismiss();
            }
        });

        radioGroup = fv(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

            }
        });
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void setListener(ClearHistoryDialogListener listener) {
        this.listener = listener;
    }

    public static interface ClearHistoryDialogListener {
        public void onDeleteButtonClick(int past);

        public void onCancelButtonClick();
    }
}
