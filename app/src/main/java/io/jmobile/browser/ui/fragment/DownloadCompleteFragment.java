package io.jmobile.browser.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;

import java.io.File;
import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.ui.adapter.DownloadAdapter;
import io.jmobile.browser.ui.adapter.ReSelectableAdapter;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.ui.dialog.EditDownloadDialog;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.file.FileUtil;

//import com.mopub.mobileads.MoPubErrorCode;
//import com.mopub.mobileads.MoPubView;

public class DownloadCompleteFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView lv;
    LinearLayoutManager manager, gridManager;
    DownloadAdapter adapter, gridAdapter;
    ArrayList<DownloadItem> list;
    SwipeRefreshLayout refreshLayout;
    LinearLayout bannerAdsLayout;
    //    MoPubView adBanner;
    com.facebook.ads.AdView fanBanner;
    boolean isGridMode = true;
    private DownloadCompleteListener listener;
    private Paint p = new Paint();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_download_complete;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        refreshLayout = fv(R.id.layout_refresh);
        refreshLayout.setColorSchemeResources(R.color.download_count_background);
        refreshLayout.setOnRefreshListener(this);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        gridManager = new GridLayoutManager(getActivity(), 2);
        lv.setLayoutManager(manager);
        lv.getItemAnimator().setRemoveDuration(100);
        adapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownloadItem item) {
            }

            @Override
            public void onDeleteButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void OnItemClick(int position, DownloadItem item) {
                if (listener != null) {
                    if (adapter.isCheckMode()) {
                        adapter.select(position);

                        if (listener != null)
                            listener.onCheckChanged(adapter.getSelectedList().size());
                    } else
                        listener.onDownloadedItemClicked(item);
                }
            }

            @Override
            public void OnItemLongClick(int position, DownloadItem item) {

            }
        });
        adapter.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
        lv.setAdapter(adapter);

        gridAdapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod_grid, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownloadItem item) {
            }

            @Override
            public void onDeleteButtonClick(int position, DownloadItem item) {
                deleteItem(position);
            }

            @Override
            public void onEditButtonClick(int position, DownloadItem item) {
                editItem(position);
            }

            @Override
            public void onClearButtonClick(int position, DownloadItem item) {
                if (gridAdapter != null)
                    gridAdapter.setEditIndex(-1);
            }

            @Override
            public void OnItemClick(int position, DownloadItem item) {
                if (listener != null) {
                    if (gridAdapter.isCheckMode()) {
                        gridAdapter.select(position);

                        if (listener != null)
                            listener.onCheckChanged(gridAdapter.getSelectedList().size());
                    } else if (gridAdapter.getEcitIndex() == position) {

                    } else
                        listener.onDownloadedItemClicked(item);
                }
            }

            @Override
            public void OnItemLongClick(int position, DownloadItem item) {
                if (gridAdapter != null)
                    gridAdapter.setEditIndex(position);

            }
        });
        gridAdapter.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        isGridMode = !sp.isDownloadListMode();

//        initAds();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            refreshLIst();
        }
    }

    @Override
    public void onRefresh() {
        refreshLIst();
        refreshLayout.setRefreshing(false);
    }

    public ArrayList<DownloadItem> getCompleteList() {
        return list;
    }

    public void deleteItems(boolean isAll) {
        ArrayList<DownloadItem> deleteList = new ArrayList<>();
        if (isGridMode)
            deleteList.addAll(gridAdapter.getSelectedList());
        else
            deleteList.addAll(adapter.getSelectedList());

        if (isAll)
            db.deleteAllDownloadedItems();
        else {
            for (DownloadItem item : deleteList)
                db.deleteDownloadItem(item);
            adapter.clearSelection();
            gridAdapter.clearSelection();
            listener.onCheckChanged(0);
        }
        refreshLIst();
    }

    public void refreshLIst() {
        if (list == null)
            return;

        list.clear();
        ArrayList<DownloadItem> temp = new ArrayList<>();
        temp.addAll(db.getAllCDownloadItems());

        for (DownloadItem item : temp) {
            if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DONE) {
                File file = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                if (file.exists())
                    list.add(item);
                else
                    db.deleteDownloadItem(item);
            }
        }
        adapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();
        gridAdapter.setEditIndex(-1);

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        setViewMode(isGridMode);

        if (listener != null)
            listener.notiEmptyCompleteList(empty);
    }

    public void setViewMode(boolean _isGridMode) {
        gridAdapter.setEditIndex(-1);
        sp.setDownloadListMode(!_isGridMode);
        isGridMode = _isGridMode;
        lv.setLayoutManager(isGridMode ? gridManager : manager);
        lv.setAdapter(isGridMode ? gridAdapter : adapter);
        int padding = (int) Util.convertDpToPixel(10, app.getApplicationContext());
        if (isGridMode)
            lv.setPadding(padding, padding, padding, padding);
        else
            lv.setPadding(0, padding, 0, padding);
    }

    public boolean isListMode() {
        return !this.isGridMode;
    }

    public void selectModeClick(boolean mode) {
        if (adapter != null)
            adapter.setCheckMode(mode);

        if (gridAdapter != null) {
            gridAdapter.setCheckMode(mode);
            if (mode)
                gridAdapter.setEditIndex(-1);
        }
    }

    public void setListener(DownloadCompleteListener listener) {
        this.listener = listener;
    }

    private void moveItem(int oldPos, int newPos) {
//        BookmarkItem item = list.get(oldPos);
//        list.remove(oldPos);
//        list.add(newPos, item);
//        adapter.notifyItemMoved(oldPos, newPos);
//
//        for (int i = 0; i < list.size() ;i ++) {
//            item = list.get(i);
//            item.setBookmarkOrder(i);
//            db.updateBookmarkOrder(item);
//        }
    }

    private void deleteItem(final int position) {
        final DownloadItem item = list.get(position);
        if (item != null) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setTitle(r.s(R.string.dlg_title_delete))
                        .setMessage(r.s(R.string.dlg_msg_delete_item))
                        .setNegativeLabel(r.s(R.string.cancel))
                        .setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                    @Override
                    public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                        if (adapter != null)
                            adapter.removeDownloadItem(item.getDownloadId());

                        if (gridAdapter != null)
                            gridAdapter.removeDownloadItem(item.getDownloadId());
                        db.deleteDownloadItem(item);
                        refreshLIst();
                    }
                });
                dlg.setNegativeListener(new BaseDialogFragment.DialogNegativeListener() {
                    @Override
                    public void onDialogNegative(BaseDialogFragment dialog, String tag) {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);

                        if (gridAdapter != null) {
                            gridAdapter.notifyItemChanged(position);
                            gridAdapter.setEditIndex(-1);
                        }
                    }
                });
                sdf(dlg);
            }
        }
    }

    private void editItem(final int position) {
        DownloadItem item = list.get(position);
        if (item != null) {
            if (fdf(EditDownloadDialog.TAG) == null) {
                EditDownloadDialog dlg = EditDownloadDialog.newInstance(EditDownloadDialog.TAG, getActivity(), item);
                dlg.setListener(new EditDownloadDialog.EditDownloadDialogListener() {
                    @Override
                    public void onSaveButtonClick(DownloadItem item) {
                        String newPath = new File(item.getDownloadFilePath()).getParent() + "/" + item.getDownloadFileName();

                        try {
                            File oldF = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                            File newF = new File(newPath);
                            oldF.renameTo(newF);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        item.setDownloadFilePath(newPath);
                        db.insertOrUpdateDownloadItem(item);
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                        if (gridAdapter != null) {
                            gridAdapter.notifyItemChanged(position);
                            gridAdapter.setEditIndex(-1);
                        }

                    }

                    @Override
                    public void onCancelButtonClick() {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                        if (gridAdapter != null) {
                            gridAdapter.notifyItemChanged(position);
                            gridAdapter.setEditIndex(-1);
                        }
                    }
                });

                sdf(dlg);
            }
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    deleteItem(viewHolder.getAdapterPosition());
                } else {
                    editItem(viewHolder.getAdapterPosition());
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    Bitmap icon;
                    if (dX > 0) {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.set_del_button_color_n));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mode_edit_white_24);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.sub_menu_edit), icon_dest.right + width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    } else {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.download_count_background));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_w);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.delete), icon_dest.left - width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return !isGridMode && !adapter.isCheckMode();
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

    private void initAds() {
        bannerAdsLayout = fv(R.id.layout_banner_ads);
        if (getActivity() == null)
            return;

        fanBanner = new com.facebook.ads.AdView(getActivity(), Common.FAN_MAIN_BANNER, com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        bannerAdsLayout.addView(fanBanner);
        fanBanner.loadAd();

        fanBanner.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                LogUtil.log("fanBanner onError()" + adError.getErrorMessage().toString());
                if (fanBanner != null) {
                    fanBanner.destroy();
                }
                bannerAdsLayout.removeAllViews();

                if (getActivity() == null)
                    return;

                onLoadAdmobAdvanced();
//                AdView admobBanner = new AdView(getActivity());
//                admobBanner.setAdSize(AdSize.BANNER);
//                admobBanner.setAdUnitId(LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_BANNER_OPT_TEST : Common.ADMOB_MAIN_BANNER_OPT);
//                admobBanner.setBackgroundColor(Color.TRANSPARENT);
//                admobBanner.loadAd(new AdRequest.Builder().build());
//                bannerAdsLayout.addView(admobBanner);
//                admobBanner.setAdListener(new com.google.android.gms.ads.AdListener() {
//                    @Override
//                    public void onAdLoaded() {
//                        super.onAdLoaded();
//                        LogUtil.log("fanBanner admobBanner onAdLoaded()");
//                    }
//
//                    @Override
//                    public void onAdFailedToLoad(int i) {
//                        super.onAdFailedToLoad(i);
//                        LogUtil.log("fanBanner admobBanner onAdFailedToLoad()" + i);
//                    }
//
//                    @Override
//                    public void onAdLeftApplication() {
//                        super.onAdLeftApplication();
//                        LogUtil.log("fanBanner admobBanner onAdLeftApplication()");
//                    }
//                });
//                admobBanner.bringToFront();

            }

            @Override
            public void onAdLoaded(Ad ad) {
                LogUtil.log("fanBanner onAdLoaded()");
            }

            @Override
            public void onAdClicked(Ad ad) {
                LogUtil.log("fanBanner onAdClicked()");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                LogUtil.log("fanBanner onLoggingImpression()");
            }
        });
//        adBanner = fv(R.id.adview);
//        adBanner.setAdUnitId(Common.ADX_DOWNLOAD_BANNER);
//        adBanner.setBannerAdListener(new MoPubView.BannerAdListener() {
//            @Override
//            public void onBannerLoaded(MoPubView banner) {
//                LogUtil.log("adBanner onBannerLoaded()");
//            }
//
//            @Override
//            public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
//                LogUtil.log("adBanner onBannerFailed()" + errorCode.toString());
//            }
//
//            @Override
//            public void onBannerClicked(MoPubView banner) {
//                LogUtil.log("adBanner onBannerClicked()");
//            }
//
//            @Override
//            public void onBannerExpanded(MoPubView banner) {
//                LogUtil.log("adBanner onBannerExpanded()");
//            }
//
//            @Override
//            public void onBannerCollapsed(MoPubView banner) {
//                LogUtil.log("adBanner onBannerCollapsed()");
//            }
//        });
//        adBanner.loadAd();
//        LogUtil.log("adBanner LoadAd()");
    }

    public static interface DownloadCompleteListener {
        public void onCheckChanged(int count);

        public void onDownloadedItemClicked(DownloadItem item);

        public void notiEmptyCompleteList(boolean empty);
    }

    private void onLoadAdmobAdvanced() {
        AdLoader adLoader = new AdLoader.Builder(getActivity(), LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_NATIVE_ADVANCED_TEST : Common.ADMOB_DOWNLOAD_NATIVE_ADVANCED)
                .forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
                    @Override
                    public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                        if (nativeAppInstallAd == null || getActivity() == null || getActivity().getLayoutInflater() == null)
                            return;
                        NativeAppInstallAdView adView = (NativeAppInstallAdView) getActivity().getLayoutInflater().inflate(R.layout.admob_app_install, null);
                        populateAppInstallAdView(nativeAppInstallAd, adView);
                        bannerAdsLayout.removeAllViews();
                        bannerAdsLayout.addView(adView);
                    }
                })
                .forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
                    @Override
                    public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                        if (nativeContentAd == null || getActivity().getLayoutInflater() == null)
                            return;

                        NativeContentAdView adView = (NativeContentAdView) getActivity().getLayoutInflater()
                                .inflate(R.layout.admob_content, null);
                        populateContentAdView(nativeContentAd, adView);
                        bannerAdsLayout.removeAllViews();
                        bannerAdsLayout.addView(adView);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder().build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAppInstallAd.getVideoController();

        // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
        // VideoController will call methods on this object when events occur in the video
        // lifecycle.
//        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
//            public void onVideoEnd() {
//                // Publishers should allow native ads to complete video playback before refreshing
//                // or replacing them with another ad in the same UI location.
//                mRefresh.setEnabled(true);
//                mVideoStatus.setText("Video status: Video playback has ended.");
//                super.onVideoEnd();
//            }
//        });

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
//        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));
//        adView.setImageView(adView.findViewById(R.id.bg_image));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());
//        ((ImageView) adView.getImageView()).setImageDrawable(nativeAppInstallAd.getImages().get(0).getDrawable());

        // Apps can check the VideoController's hasVideoContent property to determine if the
        // NativeAppInstallAd has a video asset.
//        if (vc.hasVideoContent()) {
//            mVideoStatus.setText(String.format(Locale.getDefault(),
//                    "Video status: Ad contains a %.2f:1 video asset.",
//                    vc.getAspectRatio()));
//        } else {
//            mRefresh.setEnabled(true);
//            mVideoStatus.setText("Video status: Ad does not contain a video asset.");
//        }

        // These assets aren't guaranteed to be in every NativeAppInstallAd, so it's important to
        // check before trying to display them.
//        if (nativeAppInstallAd.getPrice() == null) {
//            adView.getPriceView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getPriceView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getPriceView()).setText(nativeAppInstallAd.getPrice());
//        }

//        if (nativeAppInstallAd.getStore() == null) {
//            adView.getStoreView().setVisibility(View.INVISIBLE);
//        } else {
//            adView.getStoreView().setVisibility(View.VISIBLE);
//            ((TextView) adView.getStoreView()).setText(nativeAppInstallAd.getStore());
//        }

//        if (nativeAppInstallAd.getStarRating() == null) {
//            adView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) adView.getStarRatingView())
//                    .setRating(nativeAppInstallAd.getStarRating().floatValue());
//            adView.getStarRatingView().setVisibility(View.VISIBLE);
//        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        if (nativeContentAd.getImages().size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }
}
