package io.jmobile.browser.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.ui.adapter.DownloadAdapter;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.file.FileUtil;

//import com.mopub.nativeads.MoPubRecyclerAdapter;
//import com.mopub.nativeads.NativeAdFactory;

public class DownloadingFragment extends BaseFragment {

    RecyclerView lv;
    LinearLayoutManager manager;
    DownloadAdapter adapter;
    //    MoPubRecyclerAdapter adAdapter;
    ArrayList<DownloadItem> list;
    FrameLayout totalLayout;
    TextView totalText;
    ProgressBar totalProgress;
    private DownloadingListener listener;
    private Paint p = new Paint();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_download_ing;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        totalLayout = fv(R.id.layout_down_total);
        totalText = fv(R.id.text_down_total);
        totalProgress = fv(R.id.progress_down_total);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        lv.getItemAnimator().setRemoveDuration(500);
        lv.getItemAnimator().setAddDuration(300);
        lv.getItemAnimator().setChangeDuration(0);
        adapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownloadItem item) {
//                position = adAdapter.getOriginalPosition(position);
//                item = adapter.getItem(position);

                if (!Util.isNetworkAbailable(getActivity())) {
                    if (listener != null)
                        listener.onShowAlertDialog(r.s(R.string.dlg_msg_check_network));
                    return;
                } else if (sp.isWifiOnly()) {
                    if (!Util.getNetworkConnectionType(getActivity()).equalsIgnoreCase("WIFI")) {
                        if (listener != null)
                            listener.onShowAlertDialog(r.s(R.string.dlg_msg_check_wifi));
                        return;
                    }
                }

                if (listener != null)
                    listener.onDownloadingItemClicked(item);

                if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DOWNLOADING) {
                    item.setDownloadState(DownloadItem.DOWNLOAD_STATE_PAUSE);
                } else {
                    item.setDownloadStartAt(DownloadItem.DOWNLOAD_STATE_READY);
                }
                adapter.notifyItemChanged(position);
            }

            @Override
            public void onDeleteButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void OnItemClick(int position, DownloadItem item) {

            }

            @Override
            public void OnItemLongClick(int position, DownloadItem item) {

            }
        });
        adapter.setMode(DownloadAdapter.MODE_ADS);
        lv.setAdapter(adapter);
//        adAdapter = NativeAdFactory.getMoPubRecyclerAdapter(getActivity(), adapter, Common.ADX_LIST_DOWNLOADING_NATIVE);
//        lv.setAdapter(adAdapter);
//        adAdapter.loadAds(Common.ADX_LIST_DOWNLOADING_NATIVE);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            refreshLIst();
        else {

        }
    }

    public ArrayList<DownloadItem> getDownloadingItems() {
        return list;
    }

    public void refreshLIst() {
        if (list == null)
            return;

        list.clear();

        ArrayList<DownloadItem> temp = new ArrayList<>();
        temp.addAll(db.getAllRDownloadItems());

        for (DownloadItem item : temp) {
            if (item.getDownloadState() != DownloadItem.DOWNLOAD_STATE_NONE && item.getDownloadState() != DownloadItem.DOWNLOAD_STATE_DONE)
                list.add(item);
        }

        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        updateDownloadProgressUI();

        if (listener != null)
            listener.notiEmptyDownloadingList(empty);
    }

    public void insertItem(DownloadItem item) {
        if (adapter != null)
            adapter.insertDownloadItem(item);

        if (lv == null)
            return;

        lv.setVisibility(View.VISIBLE);
    }

    public void updateProgress(DownloadItem item) {
        if (adapter != null)
            adapter.updateProgress(item);

        if (lv != null)
            lv.setVisibility(View.VISIBLE);
    }

    public void removeItem(String id) {
        if (adapter != null)
            adapter.removeDownloadItem(id);

        if (adapter.getItems().size() <= 0)
            lv.setVisibility(View.GONE);
    }

    public void updateDownloadProgressUI() {
        if (totalLayout == null)
            return;

        if (adapter == null || adapter.getItems().size() <= 0) {
            totalLayout.setVisibility(View.GONE);
            if (listener != null)
                listener.notiEmptyDownloadingList(true);
            return;
        }

        long total = adapter.getTotalFileSize();
        long remain = adapter.getRemainFileSize();

        if (remain <= 0)
            totalText.setText("Ready");
        else
            totalText.setText(FileUtil.convertToStringRepresentation(remain) + " / " + FileUtil.convertToStringRepresentation(total));
        totalProgress.setMax((int) total);
        totalProgress.setProgress((int) remain);
        totalLayout.setVisibility(View.VISIBLE);
    }

//    public void selectModeClick(boolean mode) {
//        if (adapter != null)
//            adapter.setCheckMode(mode);
//    }
//
//    public void setMode(int mode) {
//        if (adapter != null)
//            adapter.setMode(mode);
//    }

    public void setListener(DownloadingListener listener) {
        this.listener = listener;
    }

    private void deleteItem(final int position) {
        if (position % 7 == 4) {
            adapter.notifyDataSetChanged();
            return;
        }
        final DownloadItem item = list.get(position);
        if (item != null) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setTitle(r.s(R.string.dlg_title_delete))
                        .setMessage(r.s(R.string.dlg_msg_delete_item))
                        .setNegativeLabel(r.s(R.string.cancel))
                        .setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                    @Override
                    public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                        adapter.removeDownloadItem(item.getDownloadId());
                        db.deleteDownloadItem(item);
                    }
                });
                dlg.setNegativeListener(new BaseDialogFragment.DialogNegativeListener() {
                    @Override
                    public void onDialogNegative(BaseDialogFragment dialog, String tag) {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                    }
                });
                sdf(dlg);
            }
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    deleteItem(viewHolder.getAdapterPosition());
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    Bitmap icon;
                    if (dX > 0) {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.set_del_button_color_n));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mode_edit_white_24);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.sub_menu_edit), icon_dest.right + width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    } else {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.download_count_background));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_w);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.delete), icon_dest.left - width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

    public static interface DownloadingListener {
        public void onDownloadingItemClicked(DownloadItem item);

        public void onShowAlertDialog(String message);

        public void notiEmptyDownloadingList(boolean empty);
    }


}
