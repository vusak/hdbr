package io.jmobile.browser.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import io.jmobile.browser.R;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.ui.adapter.DownloadAdapter;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.ui.base.BaseFragment;
import io.jmobile.browser.ui.dialog.EditDownloadDialog;
import io.jmobile.browser.ui.dialog.MessageDialog;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.file.FileUtil;

//import com.mopub.nativeads.MoPubRecyclerAdapter;
//import com.mopub.nativeads.NativeAdFactory;

public class DownloadReadyFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView lv;
    LinearLayoutManager manager;
    DownloadAdapter adapter;
    //    MoPubRecyclerAdapter adAdapter;
    ArrayList<DownloadItem> list;
    SwipeRefreshLayout refreshLayout;
    private DownloadReadyListener listener;
    private Paint p = new Paint();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_download_ready;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        refreshLayout = fv(R.id.layout_refresh);
        refreshLayout.setColorSchemeResources(R.color.download_count_background);
        refreshLayout.setOnRefreshListener(this);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        lv.getItemAnimator().setRemoveDuration(100);
        adapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownloadItem item) {
//                position = adAdapter.getOriginalPosition(position);
//                item = adapter.getItem(position);

                if (!Util.isNetworkAbailable(getActivity())) {
                    if (listener != null)
                        listener.onShowAlertDialog(r.s(R.string.dlg_msg_check_network));
                    return;
                } else if (sp.isWifiOnly()) {
                    if (!Util.getNetworkConnectionType(getActivity()).equalsIgnoreCase("WIFI")) {
                        if (listener != null)
                            listener.onShowAlertDialog(r.s(R.string.dlg_msg_check_wifi));
                        return;
                    }
                }
                adapter.removeDownloadItem(item.getDownloadId());

                if (listener != null)
                    listener.onReadyItemClicked(item);
            }

            @Override
            public void onDeleteButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownloadItem item) {

            }

            @Override
            public void OnItemClick(int position, DownloadItem item) {

            }

            @Override
            public void OnItemLongClick(int position, DownloadItem item) {

            }
        });
        adapter.setMode(DownloadAdapter.MODE_ADS);
//        adAdapter = NativeAdFactory.getMoPubRecyclerAdapter(getActivity(), adapter, Common.ADX_LIST_DOWNLOAD_NATIVE);
//        lv.setAdapter(adAdapter);
//        adAdapter.loadAds(Common.ADX_LIST_DOWNLOAD_NATIVE);
        lv.setAdapter(adapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

    }

    @Override
    public void onResume() {
        refreshLIst();
        super.onResume();
    }
//
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser)
//            refreshLIst();
//        else {
//
//        }
//    }

    @Override
    public void onRefresh() {
        refreshLIst();
        refreshLayout.setRefreshing(false);
    }

    public ArrayList<DownloadItem> getReadyList() {
        return list;
    }

    public void refreshLIst() {
        if (list == null)
            return;

        list.clear();

        ArrayList<DownloadItem> temp = new ArrayList<>();
        temp.addAll(db.getAllDownloadItems());

        for (DownloadItem item : temp) {
            if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_NONE)
                list.add(item);
        }

        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);

        if (listener != null)
            listener.notiEmptyReadyList(empty);
    }

    public void selectModeClick(boolean mode) {
        if (adapter != null)
            adapter.setCheckMode(mode);
    }

    public void setListener(DownloadReadyListener listener) {
        this.listener = listener;
    }

    private void deleteItem(final int position) {
        if (position % 7 == 4) {
            adapter.notifyDataSetChanged();
            return;
        }
        final DownloadItem item = list.get(position);
        if (item != null) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setTitle(r.s(R.string.dlg_title_delete))
                        .setMessage(r.s(R.string.dlg_msg_delete_item))
                        .setNegativeLabel(r.s(R.string.cancel))
                        .setPositiveLabel(r.s(R.string.delete));
                dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                    @Override
                    public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                        adapter.removeDownloadItem(item.getDownloadId());
                        db.deleteDownloadItem(item);
                    }
                });
                dlg.setNegativeListener(new BaseDialogFragment.DialogNegativeListener() {
                    @Override
                    public void onDialogNegative(BaseDialogFragment dialog, String tag) {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                    }
                });
                sdf(dlg);
            }
        }
    }

    private void editItem(final int position) {
        if (position % 7 == 4) {
            adapter.notifyDataSetChanged();
            return;
        }
        DownloadItem item = list.get(position);
        if (item != null) {
            if (fdf(EditDownloadDialog.TAG) == null) {
                EditDownloadDialog dlg = EditDownloadDialog.newInstance(EditDownloadDialog.TAG, getActivity(), item);
                dlg.setListener(new EditDownloadDialog.EditDownloadDialogListener() {
                    @Override
                    public void onSaveButtonClick(DownloadItem item) {
                        String newPath = new File(item.getDownloadFilePath()).getParent() + "/" + item.getDownloadFileName() + FileUtil.TEMP_FILE_EXTENSION;
                        item.setDownloadFilePath(newPath);
                        db.insertOrUpdateDownloadItem(item);
                        if (adapter != null) {
                            adapter.updateProgress(item);
                        }
                    }

                    @Override
                    public void onCancelButtonClick() {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                    }
                });

                sdf(dlg);
            }
        }
    }

    public void sortDownloadList() {
        if (list == null || adapter == null)
            return;

        String current = sp.getDownloadListSort();
        if (current.equalsIgnoreCase(DownloadItem.DOWNLOAD_SORT_DATE_ASC))
            Collections.sort(list, DownloadItem.COMPARATOR_DATE_ASC);
        else if (current.equalsIgnoreCase(DownloadItem.DOWNLOAD_SORT_DATE_DESC))
            Collections.sort(list, DownloadItem.COMPARATOR_DATE_DESC);
        else if (current.equalsIgnoreCase(DownloadItem.DOWNLOAD_SORT_SIZE_ASC))
            Collections.sort(list, DownloadItem.COMPARATOR_SIZE_ASC);
        else if (current.equalsIgnoreCase(DownloadItem.DOWNLOAD_SORT_SIZE_DESC))
            Collections.sort(list, DownloadItem.COMPARATOR_SIZE_DESC);

        adapter.notifyDataSetChanged();
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
//                int position = adAdapter.getOriginalPosition(viewHolder.getAdapterPosition());
//                if (adAdapter.isAd(viewHolder.getAdapterPosition())) {
//                    adapter.notifyDataSetChanged();
//                    return;
//                }
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT) {
                    deleteItem(position);
                } else {
                    editItem(position);
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    Bitmap icon;
                    if (dX > 0) {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.set_del_button_color_n));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mode_edit_white_24);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.sub_menu_edit), icon_dest.right + width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    } else {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.download_count_background));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_w);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.delete), icon_dest.left - width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

    public static interface DownloadReadyListener {
        public void onReadyItemClicked(DownloadItem item);

        public void onShowAlertDialog(String message);

        public void notiEmptyReadyList(boolean empty);
    }
}
