package io.jmobile.browser.ui.dialog;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import java.util.ArrayList;

import io.jmobile.browser.R;
import io.jmobile.browser.data.BookmarkItem;
import io.jmobile.browser.ui.adapter.BookmarkAdapter;
import io.jmobile.browser.ui.adapter.ReSelectableAdapter;
import io.jmobile.browser.ui.base.BaseDialogFragment;
import io.jmobile.browser.utils.Util;

public class AddBookmarkDialog extends BaseDialogFragment {

    public static final String TAG = AddBookmarkDialog.class.getSimpleName();

    Button cancelButton, addButton;
    RecyclerView lv;
    LinearLayoutManager manager;
    ArrayList<BookmarkItem> list;
    BookmarkAdapter adapter;

    private AddBookmarkDialogListener listener;

    public static AddBookmarkDialog newInstance(String tag) {
        AddBookmarkDialog d = new AddBookmarkDialog();

        d.createArguments(tag);
        return d;
    }

    public void setListener(AddBookmarkDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_bookmark;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        cancelButton = fv(R.id.btn_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCancelButtonClick();

                dismiss();
            }
        });

        addButton = fv(R.id.btn_add);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    for (BookmarkItem item : adapter.getSelectedList()) {
                        item.setBookmarkShowing(true);
                        db.updateBookmarkShowing(item);
                    }
                    listener.onAddButtonClick();
                }
                dismiss();
            }
        });

        lv = fv(R.id.lv);
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        list = new ArrayList<>();
        list.addAll(db.getBookmarkItems());
//        adapter = new BookmarkAdapter(getActivity(), R.layout.item_bookmark, list, new ReAdapter.ReOnItemClickListener() {
//            @Override
//            public void OnItemClick(int position, Object item) {
//                adapter.select(position);
//            }
//        });
        adapter.setCheckMode(true);
        adapter.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
        lv.setAdapter(adapter);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public static interface AddBookmarkDialogListener {
        public void onAddButtonClick();

        public void onCancelButtonClick();
    }
}
