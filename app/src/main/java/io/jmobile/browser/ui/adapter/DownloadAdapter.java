package io.jmobile.browser.ui.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;

import java.util.List;

import io.jmobile.browser.R;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;
import io.jmobile.browser.utils.file.FileUtil;
import io.jmobile.browser.utils.image.ImageUtil;

public class DownloadAdapter extends ReSelectableAdapter<DownloadItem, ReDownloadHolder> {
    public static int MODE_NONE = 0;
    public static int MODE_ADS = 1;
    private static int TYPE_AD = 0;
    private static int TYPE_CONTENT = 1;
    private Context context;
    private int editIndex = -1;
    private int mode = MODE_NONE;

    public DownloadAdapter(Context context, int layoutId, List<DownloadItem> items, DownloadAdapterListener listener) {
        super(layoutId, items, context);

        this.listener = listener;
        this.context = context;
    }

    public int getMode() {
        return this.mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
        notifyDataSetChanged();
    }

    public DownloadItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public void onBindViewHolder(ReDownloadHolder holder, int position) {
//        if (mode != MODE_ADS || position % 7 != 4) {
            DownloadItem item = items.get(position);

            holder.title.setText(item.getDownloadFileName());
            holder.check.setVisibility(isCheckMode() ? View.VISIBLE : View.GONE);
            if (holder.selBg != null)
                holder.selBg.setVisibility(View.GONE);
            if (isCheckMode()) {
                holder.check.setChecked(item.isSelected());
                if (holder.selBg != null) {
                    holder.selBg.setVisibility(item.isSelected() ? View.VISIBLE : View.GONE);
                    holder.check.setVisibility(item.isSelected() ? View.VISIBLE : View.GONE);
                } else if (holder.root != null)
                    holder.root.setBackgroundColor(ContextCompat.getColor(context, item.isSelected() ? R.color.download_selected_bg : R.color.download_non_bg));
            } else {
                if (holder.root != null)
                    holder.root.setBackgroundColor(ContextCompat.getColor(context, R.color.download_non_bg));
            }

            if (holder.selBg != null && !isCheckMode())
                holder.selBg.setVisibility(position == editIndex ? View.VISIBLE : View.GONE);
            if (holder.editLayout != null)
                holder.editLayout.setVisibility(position == editIndex && !isCheckMode() ? View.VISIBLE : View.GONE);


            if (holder.url != null) {
                String url = "";
                if (!TextUtils.isEmpty(item.getDownloadSite())) {
                    url = item.getDownloadSite().replace("http://", "");
                    url = url.replace("https://", "");
                }
                holder.url.setText(url);
            }

            if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_NONE) {
                String sub = item.getDownloadFileTime() > 0 ? FileUtil.convertToStringTime(item.getDownloadFileTime()) + " / " : "";
                String sub2 = (item.getDownloadFileTotalSize() > 0 ? FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize()) : "");
                holder.subTitle.setText(sub);
                holder.subTitle2.setText(sub2);
                if (holder.progressBar != null)
                    holder.progressBar.setVisibility(View.GONE);
                if (holder.downloadButton != null)
                    holder.downloadButton.setImageResource(R.drawable.button_down);
            } else if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DONE) {
                if (holder.downloadButton != null)
                    holder.downloadButton.setVisibility(View.GONE);

                String sub = item.getDownloadFileTime() > 0 ? FileUtil.convertToStringTime(item.getDownloadFileTime()) + " / " : "";
                sub = sub + (item.getDownloadFileTotalSize() > 0 ? FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize()) : "");
                holder.subTitle.setText(sub);

                if (holder.progressBar != null)
                    holder.progressBar.setVisibility(View.GONE);
            } else {
                holder.progressBar.setVisibility(View.VISIBLE);
                long total = item.getDownloadFileTotalSize();
                long remain = item.getDownloadFileRemainSize();

                if (remain <= 0)
                    holder.subTitle.setText("Ready");
                else
                    holder.subTitle.setText(FileUtil.convertToStringRepresentation(remain) + " / " + FileUtil.convertToStringRepresentation(total));
                holder.progressBar.setMax((int) total);
                holder.progressBar.setProgress((int) remain);

                if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_PAUSE)
                    holder.downloadButton.setImageResource(R.drawable.button_refresh);
                else
                    holder.downloadButton.setImageResource(R.drawable.button_pause);
            }
            if (item.getDownloadThumbnail() != null) {
                holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getDownloadThumbnail()));
                holder.thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else {
                holder.thumbnail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_videocam_gray_36));
                holder.thumbnail.setScaleType(ImageView.ScaleType.CENTER);
            }
//        }
    }

    @Override
    public int getItemViewType(int position) {
//        if (mode == MODE_ADS && position % 7 == 4)
//            return TYPE_AD;
//        else
            return TYPE_CONTENT;
    }

    @Override
    public ReDownloadHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final AdView adAdvancedView;
        ReDownloadHolder holder = new ReDownloadHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        if (viewType == TYPE_AD) {
            adAdvancedView = new AdView(context);
            adAdvancedView.setAdSize(AdSize.SMART_BANNER);
            int height = (int) Util.convertDpToPixel(84, context);
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, height);
            adAdvancedView.setLayoutParams(params);
            AdLoader adLoader = new AdLoader.Builder(context, LogUtil.DEBUG_MODE ? Common.ADMOB_LIST_NATIVE_TEST : Common.ADMOB_LIST_DOWNLOAD_NATIVE)
                    .forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
                        @Override
                        public void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
                            if (nativeAppInstallAd == null || context == null || ((Activity) context).getLayoutInflater() == null)
                                return;
                            NativeAppInstallAdView adView = (NativeAppInstallAdView) ((Activity) context).getLayoutInflater().inflate(R.layout.admob_app_install_down, null);
                            populateAppInstallAdView(nativeAppInstallAd, adView);
                            adAdvancedView.removeAllViews();
                            ;
                            adAdvancedView.addView(adView);
                        }
                    })
                    .forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
                        @Override
                        public void onContentAdLoaded(NativeContentAd nativeContentAd) {
                            if (nativeContentAd == null || context == null || ((Activity) context).getLayoutInflater() == null)
                                return;

                            NativeContentAdView adView = (NativeContentAdView) ((Activity) context).getLayoutInflater()
                                    .inflate(R.layout.admob_content_down, null);
                            populateContentAdView(nativeContentAd, adView);
                            adAdvancedView.removeAllViews();
                            adAdvancedView.addView(adView);
                        }
                    })
                    .withAdListener(new com.google.android.gms.ads.AdListener() {
                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder().build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
            holder = new ReDownloadHolder(adAdvancedView);
        } else {
            holder.editLayout = holder.fv(R.id.layout_edit);
            holder.deleteButton = holder.fv(R.id.item_btn_delete);
            holder.editButton = holder.fv(R.id.item_btn_edit);
            holder.clearButton = holder.fv(R.id.item_btn_clear);
            holder.root = holder.fv(R.id.layout_download);
            holder.check = holder.fv(R.id.checkbox);
            holder.line = holder.fv(R.id.line);
            holder.selBg = holder.fv(R.id.view_sel);
            holder.thumbnail = holder.fv(R.id.item_iv);
            holder.title = holder.fv(R.id.item_text_title);
            holder.subTitle = holder.fv(R.id.item_text_sub);
            holder.subTitle2 = holder.fv(R.id.item_text_sub2);
            holder.url = holder.fv(R.id.item_text_url);
            holder.downloadButton = holder.fv(R.id.item_btn);
            holder.progressBar = holder.fv(R.id.item_progress);
            holder.sizeText = holder.fv(R.id.text_size);
            holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position && position >= 0)
                        listener.OnItemClick(position, items.get(position));
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position && position >= 0)
                        listener.OnItemLongClick(position, items.get(position));
                    return true;
                }
            });

            holder.setOnDownloadButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position && position >= 0)
                        ((DownloadAdapterListener) listener).onDownloadButtonClick(position, items.get(position));
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });

            holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position && position >= 0)
                        ((DownloadAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });

            holder.setOnEditdButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position && position >= 0)
                        ((DownloadAdapterListener) listener).onEditButtonClick(position, items.get(position));
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });

            holder.setOnClearButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
                @Override
                public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                    if (listener != null && items.size() > position && position >= 0)
                        ((DownloadAdapterListener) listener).onClearButtonClick(position, items.get(position));
                }

                @Override
                public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                    return false;
                }
            });
        }
        return holder;
    }

    public void updateProgress(DownloadItem item) {
        if (items == null || item == null)
            return;

        for (int i = 0; i < items.size(); i++)
            if (items.get(i).getDownloadId().equalsIgnoreCase(item.getDownloadId())) {
                items.get(i).setDownloadFileRemainSize(item.getDownloadFileRemainSize());
                items.get(i).setDownloadState(item.getDownloadState());
                notifyItemChanged(i);

                return;
            }
    }

    public void insertDownloadItem(DownloadItem item) {
        if (isContains(item))
            return;

        items.add(0, item);
        notifyItemInserted(0);
    }

    public void removeDownloadItem(String id) {
        if (items == null)
            return;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getDownloadId().equalsIgnoreCase(id)) {
                items.remove(i);
                notifyItemRemoved(i);

                return;
            }
        }
    }

    public long getRemainFileSize() {
        long result = 0;
        for (DownloadItem item : items)
            result += item.getDownloadFileRemainSize();

        return result;
    }

    public long getTotalFileSize() {
        long result = 0;
        for (DownloadItem item : items)
            result += item.getDownloadFileTotalSize();

        return result;

    }

    public void setEditIndex(int index) {
        this.editIndex = index;
        notifyDataSetChanged();
    }

    public int getEcitIndex() {
        return this.editIndex;
    }

    public boolean isContains(DownloadItem item) {
        for (DownloadItem temp : items)
            if (temp.getDownloadId().equalsIgnoreCase(item.getDownloadId()))
                return true;

        return false;
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
//        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
//        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));
//        adView.setImageView(adView.findViewById(R.id.bg_image));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
//        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
//        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
//        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

//        if (nativeContentAd.getImages().size() > 0) {
//            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
//        }

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    public static interface DownloadAdapterListener extends ReOnItemClickListener<DownloadItem> {
        public void onDownloadButtonClick(int position, DownloadItem item);

        public void onEditButtonClick(int position, DownloadItem item);

        public void onDeleteButtonClick(int position, DownloadItem item);

        public void onClearButtonClick(int position, DownloadItem item);
    }

}