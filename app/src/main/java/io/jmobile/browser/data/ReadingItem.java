package io.jmobile.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.utils.LogUtil;

/**
 * Created by 편효정 on 2017-05-31.
 */

public class ReadingItem extends TableObject {
    public static final String TABLE_NAME = "readings";

    public static final String ROW_ID = "_id";
    public static final String READING_ID = "reading_id";
    public static final String READING_URL = "reading_url";
    public static final String READING_SITE = "reading_site";
    public static final String READING_THUMBNAIL = "reading_thumbnail";
    public static final String READING_THUMBNAIL_PATH = "reading_thumbnail_path";
    public static final String READING_TITLE = "reading_title";
    public static final String READING_CONTENTS = "reading_contents";
    public static final String READING_DONE = "reading_done";
    public static final String READING_AT = "reading_at";
    public static final Parcelable.Creator<ReadingItem> CREATOR = new Creator<ReadingItem>() {
        @Override
        public ReadingItem createFromParcel(Parcel source) {
            return new ReadingItem(source);
        }

        @Override
        public ReadingItem[] newArray(int size) {
            return new ReadingItem[size];
        }
    };
    private long id;
    private String readingId;
    private String url;
    private String site;
    private byte[] thumbnail;
    private String thumbnailPath;
    private String title;
    private String contents;
    private long readingAt;
    private boolean done = false;

    public ReadingItem() {
        super();
    }

    public ReadingItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.readingId = in.readString();
        this.url = in.readString();
        this.site = in.readString();
        in.readByteArray(this.thumbnail);
        this.thumbnailPath = in.readString();
        this.title = in.readString();
        this.contents = in.readString();
        this.done = in.readInt() == 1;
        this.readingAt = in.readLong();
    }

    public static void createTableReadings(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(READING_ID + " text not null unique, ")
                .append(READING_URL + " text not null, ")
                .append(READING_SITE + " text, ")
                .append(READING_THUMBNAIL + " BLOB, ")
                .append(READING_THUMBNAIL_PATH + " text, ")
                .append(READING_TITLE + " text, ")
                .append(READING_CONTENTS + " text, ")
                .append(READING_DONE + " integer not null, ")
                .append(READING_AT + " integer not null) ")
                .toString());
    }

    public static ReadingItem getReadingItem(SQLiteDatabase db, String id) {
        List<ReadingItem> list = getReadingItems(db, null, READING_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<ReadingItem> getReadingItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<ReadingItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                ReadingItem item = new ReadingItem()
                        .setRowId(c)
                        .setReadingId(c)
                        .setReadingUrl(c)
                        .setReadingSite(c)
                        .setReadingThumbnail(c)
                        .setReadingThumbnailPath(c)
                        .setReadingTitle(c)
                        .setReadingContents(c)
                        .setReadingDone(c)
                        .setReadingAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateReadingItem(SQLiteDatabase db, ReadingItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putReadingId(v)
                    .putReadingUrl(v)
                    .putReadingSite(v)
                    .putReadingThumbnail(v)
                    .putReadingThumbnailPath(v)
                    .putReadingTitle(v)
                    .putReadingContents(v)
                    .putReadingDone(v)
                    .putReadingAt(v);

            int rowAffected = db.update(TABLE_NAME, v, READING_ID + " =? ", new String[]{item.getReadingId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getReadingTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteReadingItem(SQLiteDatabase db, ReadingItem item) {
        try {
            db.delete(TABLE_NAME, READING_ID + " = ? ", new String[]{item.getReadingId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteDoneReadingItem(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, READING_DONE + " = ? ", new String[]{"1"});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllReadingItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(readingId);
        out.writeString(url);
        out.writeString(site);
        out.writeByteArray(thumbnail);
        out.writeString(thumbnailPath);
        out.writeString(title);
        out.writeString(contents);
        out.writeInt(done ? 1 : 0);
        out.writeLong(readingAt);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ReadingItem && readingId == ((ReadingItem) obj).readingId;
    }

    public long getRowId() {
        return this.id;
    }

    public ReadingItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public ReadingItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public ReadingItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getReadingId() {
        return this.readingId;
    }

    public ReadingItem setReadingId(Cursor c) {
        this.readingId = s(c, READING_ID);

        return this;
    }

    public ReadingItem setReadingId(String id) {
        this.readingId = id;

        return this;
    }

    public ReadingItem putReadingId(ContentValues v) {
        v.put(READING_ID, readingId);

        return this;
    }

    public String getReadingUrl() {
        return this.url;
    }

    public ReadingItem setReadingUrl(Cursor c) {
        this.url = s(c, READING_URL);

        return this;
    }

    public ReadingItem setReadingUrl(String url) {
        this.url = url;

        return this;
    }

    public ReadingItem putReadingUrl(ContentValues v) {
        v.put(READING_URL, url);

        return this;
    }

    public String getReadingSite() {
        return this.site;
    }

    public ReadingItem setReadingSite(Cursor c) {
        this.site = s(c, READING_SITE);

        return this;
    }

    public ReadingItem setReadingSite(String site) {
        this.site = site;

        return this;
    }

    public ReadingItem putReadingSite(ContentValues v) {
        v.put(READING_SITE, site);

        return this;
    }

    public String getReadingTitle() {
        return this.title;
    }

    public ReadingItem setReadingTitle(Cursor c) {
        this.title = s(c, READING_TITLE);

        return this;
    }

    public ReadingItem setReadingTitle(String title) {
        this.title = title;

        return this;
    }

    public ReadingItem putReadingTitle(ContentValues v) {
        v.put(READING_TITLE, title);

        return this;
    }

    public String getReadingContents() {
        return this.contents;
    }

    public ReadingItem setReadingContents(Cursor c) {
        this.contents = s(c, READING_CONTENTS);

        return this;
    }

    public ReadingItem setReadingContents(String contents) {
        this.contents = contents;

        return this;
    }

    public ReadingItem putReadingContents(ContentValues v) {
        v.put(READING_CONTENTS, contents);

        return this;
    }

    public byte[] getReadingThumbnail() {
        return this.thumbnail;
    }

    public ReadingItem setReadingThumbnail(Cursor c) {
        this.thumbnail = blob(c, READING_THUMBNAIL);

        return this;
    }

    public ReadingItem setReadingThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public ReadingItem putReadingThumbnail(ContentValues v) {
        v.put(READING_THUMBNAIL, thumbnail);

        return this;
    }

    public String getReadingThumbnailPath() {
        return this.thumbnailPath;
    }

    public ReadingItem setReadingThumbnailPath(Cursor c) {
        this.thumbnailPath = s(c, READING_THUMBNAIL_PATH);

        return this;
    }

    public ReadingItem setReadingThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;

        return this;
    }

    public ReadingItem putReadingThumbnailPath(ContentValues v) {
        v.put(READING_THUMBNAIL_PATH, thumbnailPath);

        return this;
    }

    public boolean isReadingDone() {
        return this.done;
    }

    public ReadingItem setReadingDone(Cursor c) {
        this.done = i(c, READING_DONE) == 1;

        return this;
    }

    public ReadingItem setReadingDone(boolean done) {
        this.done = done;

        return this;
    }

    public ReadingItem putReadingDone(ContentValues v) {
        v.put(READING_DONE, done ? 1 : 0);

        return this;
    }

    public long getReadingAt() {
        return this.readingAt;
    }

    public ReadingItem setReadingAt(Cursor c) {
        this.readingAt = l(c, READING_AT);

        return this;
    }

    public ReadingItem setReadingAt(long at) {
        this.readingAt = at;

        return this;
    }

    public ReadingItem putReadingAt(ContentValues v) {
        v.put(READING_AT, readingAt);

        return this;
    }
}
