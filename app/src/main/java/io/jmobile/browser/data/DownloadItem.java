package io.jmobile.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import io.jmobile.browser.ui.base.BaseComparator;
import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.file.FileUtil;

public class DownloadItem extends TableObject {
    public static final String TABLE_NAME = "download";

    public static final String ROW_ID = "_id";
    public static final String DOWNLOAD_ID = "download_id";

    public static final String DOWNLOAD_URL = "download_url";
    public static final String DOWNLOAD_SITE = "download_site";
    public static final String DOWNLOAD_FILE_NAME = "download_file_name";
    public static final String DOWNLOAD_FILE_PATH = "download_file_path";
    public static final String DOWNLOAD_FILE_EXT = "download_file_ext";
    public static final String DOWNLOAD_FILE_TIME = "download_file_time";
    public static final String DOWNLOAD_FILE_TOTAL_SIZE = "download_file_total_size";
    public static final String DOWNLOAD_FILE_REMAIN_SIZE = "download_file_remain_size";
    public static final String DOWNLOAD_THUMBNAIL_PATH = "download_thumbnail_path";
    public static final String DOWNLOAD_THUMBNAIL = "download_thumbnail";

    public static final String DOWNLOAD_STATE = "download_state";
    public static final String DOWNLOAD_START_AT = "download_state_at";
    public static final String DOWNLOAD_AT = "download_at";

    public static final int DOWNLOAD_STATE_NONE = 0;
    public static final int DOWNLOAD_STATE_READY = 1;
    public static final int DOWNLOAD_STATE_DOWNLOADING = 2;
    public static final int DOWNLOAD_STATE_PAUSE = 3;
    public static final int DOWNLOAD_STATE_DONE = 4;

    public static final String DOWNLOAD_SORT_DATE_ASC = "download_sort_date_asc";
    public static final String DOWNLOAD_SORT_DATE_DESC = "download_sort_date_desc";
    public static final String DOWNLOAD_SORT_SIZE_ASC = "download_sort_time_asc";
    public static final String DOWNLOAD_SORT_SIZE_DESC = "download_sort_time_desc";
    public static final Parcelable.Creator<DownloadItem> CREATOR = new Creator<DownloadItem>() {
        @Override
        public DownloadItem createFromParcel(Parcel source) {
            return new DownloadItem(source);
        }

        @Override
        public DownloadItem[] newArray(int size) {
            return new DownloadItem[size];
        }
    };
    public static DownloadComparator COMPARATOR_SIZE_ASC = new DownloadSizeComparator(true);
    public static DownloadComparator COMPARATOR_SIZE_DESC = new DownloadSizeComparator(false);
    public static DownloadComparator COMPARATOR_DATE_ASC = new DownloadDateComparator(true);
    public static DownloadComparator COMPARATOR_DATE_DESC = new DownloadDateComparator(false);
    private long id;
    private String downId;
    private String downUrl;
    private String downSite;
    private String downFileName;
    private String downFilePath;
    private String downFileExt;
    private long downFileTime;
    private long downFileTotalSize;
    private long downFileRemainSize;
    private long before;
    private String downThumbnailPath;
    private byte[] thumbnail;
    private int downState = DOWNLOAD_STATE_DONE;
    private long downStartAt;
    private long downAt;

    public DownloadItem() {
        super();
    }

    public DownloadItem(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.downId = in.readString();
        this.downUrl = in.readString();
        this.downSite = in.readString();
        this.downFilePath = in.readString();
        this.downFileName = in.readString();
        this.downFileExt = in.readString();
        this.downFileTime = in.readLong();
        this.downFileTotalSize = in.readLong();
        this.downFileRemainSize = in.readLong();
        this.downThumbnailPath = in.readString();
        in.readByteArray(this.thumbnail);
        this.downState = in.readInt();
        this.downAt = in.readLong();
        this.downStartAt = in.readLong();
    }

    public static void createTableDownload(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(DOWNLOAD_ID + " text not null unique, ")
                .append(DOWNLOAD_URL + " text not null, ")
                .append(DOWNLOAD_SITE + " text, ")
                .append(DOWNLOAD_FILE_PATH + " text, ")
                .append(DOWNLOAD_FILE_NAME + " text, ")
                .append(DOWNLOAD_FILE_EXT + " text, ")
                .append(DOWNLOAD_FILE_TIME + " integer, ")
                .append(DOWNLOAD_FILE_TOTAL_SIZE + " integer, ")
                .append(DOWNLOAD_FILE_REMAIN_SIZE + " integer, ")
                .append(DOWNLOAD_THUMBNAIL_PATH + " text, ")
                .append(DOWNLOAD_THUMBNAIL + " BLOB, ")
                .append(DOWNLOAD_STATE + " integer not null, ")
                .append(DOWNLOAD_AT + " integer, ")
                .append(DOWNLOAD_START_AT + " integer) ")
                .toString());
    }

    public static boolean isDownloadFileName(SQLiteDatabase db, String name) {
        List<DownloadItem> list = getDownloadItems(db, null, DOWNLOAD_FILE_NAME + " = ?", new String[]{name}, null, null, null, "1");

        if (list.size() <= 0)
            return false;

        boolean result = false;
        for (DownloadItem item : list)
            if (item.getDownloadState() != DOWNLOAD_STATE_DONE) {
                result = true;
                break;
            }

        return result;
    }

    public static DownloadItem getDownloadItem(SQLiteDatabase db, String id) {
        List<DownloadItem> list = getDownloadItems(db, null, DOWNLOAD_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<DownloadItem> getDownloadItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<DownloadItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                DownloadItem item = new DownloadItem()
                        .setRowId(c)
                        .setDownloadId(c)
                        .setDownloadUrl(c)
                        .setDownloadSite(c)
                        .setDownloadFilePath(c)
                        .setDownloadFileName(c)
                        .setDownloadFileExt(c)
                        .setDownloadFileTime(c)
                        .setDownloadFileTotalSize(c)
                        .setDownloadFileRemainSize(c)
                        .setDownloadFileThumbnailPath(c)
                        .setDownloadThumbnail(c)
                        .setDownloadState(c)
                        .setDownloadAt(c)
                        .setDownloadStartAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean restateDownloadState(SQLiteDatabase db) {
        try {
            db.execSQL("update " + TABLE_NAME + " set " + DOWNLOAD_STATE + " = '" + DOWNLOAD_STATE_PAUSE + "' where " + DOWNLOAD_STATE + " = '" +
                    DOWNLOAD_STATE_DOWNLOADING + "'");

            db.execSQL("update " + TABLE_NAME + " set " + DOWNLOAD_STATE + " = '" + DOWNLOAD_STATE_PAUSE + "' where " + DOWNLOAD_STATE + " = '" +
                    DOWNLOAD_STATE_READY + "'");

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean cancelAllDownloadItems(SQLiteDatabase db) {
        List<DownloadItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + " <> " + DOWNLOAD_STATE_DONE, null, null, null, null, null);
        if (list != null && list.size() > 0)
            for (DownloadItem item : list) {
                deleteDownloadItem(db, item);
            }

        return false;
    }

    public static boolean insertOrUpdateDownloadItem(SQLiteDatabase db, DownloadItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putDownloadId(v)
                    .putDownloadUrl(v)
                    .putDownloadSite(v)
                    .putDownloadFilePath(v)
                    .putDownloadFIleName(v)
                    .putDownloadFileExt(v)
                    .putDownloadFileTime(v)
                    .putDownloadFileTotalSize(v)
                    .putDownloadFileRemainSize(v)
                    .putDownloadFileThumbnailPath(v)
                    .putDownloadThumbnail(v)
                    .putDownloadState(v)
                    .putDownloadAt(v)
                    .putDownloadStartAt(v);

            int rowAffected = db.update(TABLE_NAME, v, DOWNLOAD_ID + " =? ", new String[]{item.getDownloadId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getDownloadFileName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean updateDownloadState(SQLiteDatabase db, DownloadItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putDownloadState(v)
                    .putDownloadFileTotalSize(v)
                    .putDownloadFileRemainSize(v)
                    .putDownloadAt(v)
                    .putDownloadStartAt(v);

            db.update(TABLE_NAME, v, DOWNLOAD_ID + " = ?", new String[]{item.getDownloadId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteDownloadItem(SQLiteDatabase db, DownloadItem item) {
        try {
            db.delete(TABLE_NAME, DOWNLOAD_ID + " = ? ", new String[]{item.getDownloadId()});

            try {
                File cFile = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                if (cFile.exists())
                    FileUtil.delete(cFile);
                File vFile = new File(item.getDownloadFilePath());
                if (vFile.exists())
                    FileUtil.delete(vFile);
                File tFile = new File(item.getDownloadFileThumbnailPath());
                if (tFile.exists())
                    FileUtil.delete(tFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllNonDownloadItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_NONE)});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteAllDownloadedItems(SQLiteDatabase db) {
        List<DownloadItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_DONE)}, null, null, null, null);
        try {
            db.delete(TABLE_NAME, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_DONE)});

            for (DownloadItem item : list) {
                File vFile = new File(item.getDownloadFilePath());
                if (vFile.exists())
                    FileUtil.delete(vFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteDownloadedItems(SQLiteDatabase db, ArrayList<DownloadItem> list) {
        for (DownloadItem item : list)
            deleteDownloadItem(db, item);
        return false;
    }

    public static int readyDownloadItemCount(SQLiteDatabase db) {
        List<DownloadItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_NONE)}, null, null, null, null);

        return list.size();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(downId);
        out.writeString(downUrl);
        out.writeString(downSite);
        out.writeString(downFilePath);
        out.writeString(downFileName);
        out.writeString(downFileExt);
        out.writeLong(downFileTime);
        out.writeLong(downFileTotalSize);
        out.writeLong(downFileRemainSize);
        out.writeString(downThumbnailPath);
        out.writeByteArray(thumbnail);
        out.writeInt(downState);
        out.writeLong(downAt);
        out.writeLong(downStartAt);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DownloadItem && downId == ((DownloadItem) obj).downId;
    }

    public long getRowId() {
        return this.id;
    }

    public DownloadItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public DownloadItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public DownloadItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getDownloadId() {
        return this.downId;
    }

    public DownloadItem setDownloadId(Cursor c) {
        this.downId = s(c, DOWNLOAD_ID);

        return this;
    }

    public DownloadItem setDownloadId(String downId) {
        this.downId = downId;

        return this;
    }

    public DownloadItem putDownloadId(ContentValues v) {
        v.put(DOWNLOAD_ID, downId);

        return this;
    }

    public String getDownloadUrl() {
        return this.downUrl;
    }

    public DownloadItem setDownloadUrl(Cursor c) {
        this.downUrl = s(c, DOWNLOAD_URL);

        return this;
    }

    public DownloadItem setDownloadUrl(String url) {
        this.downUrl = url;

        return this;
    }

    public DownloadItem putDownloadUrl(ContentValues v) {
        v.put(DOWNLOAD_URL, downUrl);

        return this;
    }

    public String getDownloadSite() {
        return this.downSite;
    }

    public DownloadItem setDownloadSite(Cursor c) {
        this.downSite = s(c, DOWNLOAD_SITE);

        return this;
    }

    public DownloadItem setDownloadSite(String site) {
        this.downSite = site;

        return this;
    }

    public DownloadItem putDownloadSite(ContentValues v) {
        v.put(DOWNLOAD_SITE, downSite);

        return this;
    }

    public String getDownloadFilePath() {
        return this.downFilePath;
    }

    public DownloadItem setDownloadFilePath(Cursor c) {
        this.downFilePath = s(c, DOWNLOAD_FILE_PATH);

        return this;
    }

    public DownloadItem setDownloadFilePath(String filePath) {
        this.downFilePath = filePath;

        return this;
    }

    public DownloadItem putDownloadFilePath(ContentValues v) {
        v.put(DOWNLOAD_FILE_PATH, downFilePath);

        return this;
    }

    public String getDownloadFileName() {
        return this.downFileName;
    }

    public DownloadItem setDownloadFileName(Cursor c) {
        this.downFileName = s(c, DOWNLOAD_FILE_NAME);

        return this;
    }

    public DownloadItem setDownloadFileName(String fileName) {
        this.downFileName = fileName;

        return this;
    }

    public DownloadItem putDownloadFIleName(ContentValues v) {
        v.put(DOWNLOAD_FILE_NAME, downFileName);

        return this;
    }

    public String getDownloadFileExt() {
        return this.downFileExt;
    }

    public DownloadItem setDownloadFileExt(Cursor c) {
        this.downFileExt = s(c, DOWNLOAD_FILE_EXT);

        return this;
    }

    public DownloadItem setDownloadFileExt(String fileExt) {
        this.downFileExt = fileExt;

        return this;
    }

    public DownloadItem putDownloadFileExt(ContentValues v) {
        v.put(DOWNLOAD_FILE_EXT, downFileExt);

        return this;
    }

    public long getDownloadFileTime() {
        return this.downFileTime;
    }

    public DownloadItem setDownloadFileTime(Cursor c) {
        this.downFileTime = l(c, DOWNLOAD_FILE_TIME);

        return this;
    }

    public DownloadItem setDownloadFileTime(long time) {
        this.downFileTime = time;

        return this;
    }

    public DownloadItem putDownloadFileTime(ContentValues v) {
        v.put(DOWNLOAD_FILE_TIME, downFileTime);

        return this;
    }

    public long getDownloadFileTotalSize() {
        return this.downFileTotalSize;
    }

    public DownloadItem setDownloadFileTotalSize(Cursor c) {
        this.downFileTotalSize = l(c, DOWNLOAD_FILE_TOTAL_SIZE);

        return this;
    }

    public DownloadItem setDownloadFileTotalSize(long totalSize) {
        this.downFileTotalSize = totalSize;

        return this;
    }

    public DownloadItem putDownloadFileTotalSize(ContentValues v) {
        v.put(DOWNLOAD_FILE_TOTAL_SIZE, downFileTotalSize);

        return this;
    }

    public long getDownloadFileRemainSize() {
        return this.downFileRemainSize;
    }

    public DownloadItem setDownloadFileRemainSize(Cursor c) {
        this.downFileRemainSize = l(c, DOWNLOAD_FILE_REMAIN_SIZE);

        if (downFileRemainSize < before)
            downFileRemainSize = before;
        else
            before = downFileRemainSize;
        return this;
    }

    public DownloadItem setDownloadFileRemainSize(long remainSize) {
        this.downFileRemainSize = remainSize;

        if (downFileRemainSize < before)
            downFileRemainSize = before;
        else
            before = downFileRemainSize;

        return this;
    }

    public DownloadItem putDownloadFileRemainSize(ContentValues v) {
        v.put(DOWNLOAD_FILE_REMAIN_SIZE, downFileRemainSize);

        return this;
    }

    public String getDownloadFileThumbnailPath() {
        return this.downThumbnailPath;
    }

    public DownloadItem setDownloadFileThumbnailPath(Cursor c) {
        this.downThumbnailPath = s(c, DOWNLOAD_THUMBNAIL_PATH);

        return this;
    }

    public DownloadItem setDownloadFileThumbnailPath(String path) {
        this.downThumbnailPath = path;

        return this;
    }

    public DownloadItem putDownloadFileThumbnailPath(ContentValues v) {
        v.put(DOWNLOAD_THUMBNAIL_PATH, downThumbnailPath);

        return this;
    }

    public byte[] getDownloadThumbnail() {
        return this.thumbnail;
    }

    public DownloadItem setDownloadThumbnail(Cursor c) {
        this.thumbnail = blob(c, DOWNLOAD_THUMBNAIL);

        return this;
    }

    public DownloadItem setDownloadThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public DownloadItem putDownloadThumbnail(ContentValues v) {
        v.put(DOWNLOAD_THUMBNAIL, thumbnail);

        return this;
    }

    public int getDownloadState() {
        return this.downState;
    }

    public DownloadItem setDownloadState(Cursor c) {
        this.downState = i(c, DOWNLOAD_STATE);

        return this;
    }

    public DownloadItem setDownloadState(int state) {
        this.downState = state;

        return this;
    }

    public DownloadItem putDownloadState(ContentValues v) {
        v.put(DOWNLOAD_STATE, downState);

        return this;
    }

    public long getDownloadAt() {
        return this.downAt;
    }

    public DownloadItem setDownloadAt(Cursor c) {
        this.downAt = l(c, DOWNLOAD_AT);

        return this;
    }

    public DownloadItem setDownloadAt(long downloadAt) {
        this.downAt = downloadAt;

        return this;
    }

    public DownloadItem putDownloadAt(ContentValues v) {
        v.put(DOWNLOAD_AT, downAt);

        return this;
    }

    public long getDownloadStartAt() {
        return this.downStartAt;
    }

    public DownloadItem setDownloadStartAt(Cursor c) {
        this.downStartAt = l(c, DOWNLOAD_START_AT);

        return this;
    }

    public DownloadItem setDownloadStartAt(long downloadStartAt) {
        this.downStartAt = downloadStartAt;

        return this;
    }

    public DownloadItem putDownloadStartAt(ContentValues v) {
        v.put(DOWNLOAD_START_AT, downStartAt);

        return this;
    }

    private static class DownloadComparator extends BaseComparator implements Comparator<DownloadItem> {
        public DownloadComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(DownloadItem lhs, DownloadItem rhs) {
            if (lhs == null && rhs != null)
                return 1;
            else if (rhs == null && lhs != null)
                return -1;
            else
                return 0;
        }

        protected final int compareId(DownloadItem lhs, DownloadItem rhs) {
            return compareLong(lhs.getRowId(), rhs.getRowId(), false, true);
        }
    }

    private static class DownloadSizeComparator extends DownloadComparator {
        public DownloadSizeComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(DownloadItem lhs, DownloadItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            result = compareLong(lhs.getDownloadFileTotalSize(), rhs.getDownloadFileTotalSize(), true);
            if (result != 0)
                return result;
            else
                return compareLong(lhs.getDownloadFileTime(), rhs.getDownloadFileTime(), false);
        }
    }

    private static class DownloadDateComparator extends DownloadComparator {
        public DownloadDateComparator(boolean asc) {
            super(asc);
        }

        @Override
        public int compare(DownloadItem lhs, DownloadItem rhs) {
            int result = super.compare(lhs, rhs);
            if (result != 0)
                return result;

            return compareLong(lhs.getRowId(), rhs.getRowId(), false);
        }
    }
}
