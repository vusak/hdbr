package io.jmobile.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

public class ExtraItem extends TableObject {
    public static final String TABLE_NAME = "extra";

    public static final String ROW_ID = "_id";
    public static final String EXTRA_HOME_IMG = "extra_home_img";
    public static final String EXTRA_TOTAL_VIDEOS = "extra_total_videos";
    public static final String EXTRA_TOTAL_SIZE = "extra_total_size";
    public static final String EXTRA_TOTAL_TIME = "extra_total_time";
    public static final String EXTRA_INT = "extra_int";
    public static final String EXTRA_TEXT = "extra_text";
    public static final Creator<ExtraItem> CREATOR = new Creator<ExtraItem>() {
        @Override
        public ExtraItem createFromParcel(Parcel source) {
            return new ExtraItem(source);
        }

        @Override
        public ExtraItem[] newArray(int size) {
            return new ExtraItem[size];
        }
    };
    private long id;
    private byte[] homeImage;
    private int totalCount;
    private long totalSize, totalTime;
    private String extraText;
    private int extraInt;

    public ExtraItem() {
        super();
    }

    public ExtraItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        in.readByteArray(this.homeImage);
        this.totalCount = in.readInt();
        this.totalSize = in.readLong();
        this.totalTime = in.readLong();
        this.extraInt = in.readInt();
        this.extraText = in.readString();
    }

    public static void createTableExtraItems(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(EXTRA_HOME_IMG + " BLOB, ")
                .append(EXTRA_TOTAL_VIDEOS + " integer, ")
                .append(EXTRA_TOTAL_SIZE + " integer, ")
                .append(EXTRA_TOTAL_TIME + " integer, ")
                .append(EXTRA_INT + " integer, ")
                .append(EXTRA_TEXT + " text) ")
                .toString());
    }

    public static byte[] getExtraHomeImage(SQLiteDatabase db) {
        ExtraItem item = getExtraItem(db);
        return item == null ? null : item.getHomeImage();
    }

    public static ExtraItem getExtraItem(SQLiteDatabase db) {
        List<ExtraItem> list = getExtraItems(db, null, null, null, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<ExtraItem> getExtraItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<ExtraItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
//            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                ExtraItem item = new ExtraItem()
                        .setRowId(c)
                        .setHomeImage(c)
                        .setExtraTotalCount(c)
                        .setExtraTotalSize(c)
                        .setExtraTotalTime(c)
                        .setExtraInt(c)
                        .setExtraText(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateExtraItem(SQLiteDatabase db, ExtraItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putHomeImage(v)
                    .putExtraTotalCount(v)
                    .putExtraTotalTime(v)
                    .putExtraTotalSize(v)
                    .putExtraInt(v)
                    .putExtraText(v);

            if (getExtraItem(db) == null)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean insertOrUpdateExtraItem(SQLiteDatabase db, byte[] homeImage) {
        ExtraItem item = new ExtraItem();
        item.setHomeImage(homeImage);

        try {
            ContentValues v = new ContentValues();
            item.putHomeImage(v)
                    .putExtraTotalCount(v)
                    .putExtraTotalTime(v)
                    .putExtraTotalSize(v)
                    .putExtraInt(v)
                    .putExtraText(v);

            if (getExtraItem(db) == null)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeByteArray(homeImage);
        out.writeInt(totalCount);
        out.writeLong(totalSize);
        out.writeLong(totalTime);
        out.writeInt(extraInt);
        out.writeString(extraText);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ExtraItem && id == ((ExtraItem) obj).id;
    }

    public long getRowId() {
        return this.id;
    }

    public ExtraItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public ExtraItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public ExtraItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }


    public byte[] getHomeImage() {
        return this.homeImage;
    }

    public ExtraItem setHomeImage(Cursor c) {
        this.homeImage = blob(c, EXTRA_HOME_IMG);

        return this;
    }

    public ExtraItem setHomeImage(byte[] homeImage) {
        this.homeImage = homeImage;
        return this;
    }

    public ExtraItem putHomeImage(ContentValues v) {
        v.put(EXTRA_HOME_IMG, homeImage);

        return this;
    }

    public int getExtraTotalCount() {
        return this.totalCount;
    }

    public ExtraItem setExtraTotalCount(Cursor c) {
        this.totalCount = i(c, EXTRA_TOTAL_VIDEOS);

        return this;
    }

    public ExtraItem setExtraTotalCount(int totalCount) {
        this.totalCount = totalCount;

        return this;
    }

    public ExtraItem putExtraTotalCount(ContentValues v) {
        v.put(EXTRA_TOTAL_VIDEOS, totalCount);

        return this;
    }

    public long getExtraTotalSize() {
        return this.totalSize;
    }

    public ExtraItem setExtraTotalSize(Cursor c) {
        this.totalSize = l(c, EXTRA_TOTAL_SIZE);

        return this;
    }

    public ExtraItem setExtraTotalSize(long totalSize) {
        this.totalSize = totalSize;

        return this;
    }

    public ExtraItem putExtraTotalSize(ContentValues v) {
        v.put(EXTRA_TOTAL_SIZE, totalSize);

        return this;
    }

    public long getExtraTotalTime() {
        return this.totalTime;
    }

    public ExtraItem setExtraTotalTime(Cursor c) {
        this.totalTime = l(c, EXTRA_TOTAL_TIME);

        return this;
    }

    public ExtraItem setExtraTotalTime(long totalTime) {
        this.totalTime = totalTime;

        return this;
    }

    public ExtraItem putExtraTotalTime(ContentValues v) {
        v.put(EXTRA_TOTAL_TIME, totalTime);

        return this;
    }

    public int getExtraInt() {
        return this.extraInt;
    }

    public ExtraItem setExtraInt(Cursor c) {
        this.extraInt = i(c, EXTRA_INT);

        return this;
    }

    public ExtraItem setExtraInt(int extraInt) {
        this.extraInt = extraInt;

        return this;
    }

    public ExtraItem putExtraInt(ContentValues v) {
        v.put(EXTRA_INT, extraInt);

        return this;
    }


    public String getExtraText() {
        return this.extraText;
    }

    public ExtraItem setExtraText(Cursor c) {
        this.extraText = s(c, EXTRA_TEXT);

        return this;
    }

    public ExtraItem setExtraText(String extraText) {
        this.extraText = extraText;

        return this;
    }

    public ExtraItem putExtraText(ContentValues v) {
        v.put(EXTRA_TEXT, extraText);

        return this;
    }


}
