package io.jmobile.browser.data;

import android.database.Cursor;
import android.os.Parcel;

/**
 * Created by Cecil on 2017-05-30.
 */

public class TableObject extends Selectable {
    public TableObject() {
        super();
    }

    public TableObject(Parcel parcel) {
        super(parcel);
    }

    protected static boolean b(Cursor c, String columnName) {
        return c.getInt(c.getColumnIndex(columnName)) != 0;
    }

    protected static float f(Cursor c, String columnName) {
        return c.getFloat(c.getColumnIndex(columnName));
    }

    protected static int i(Cursor c, String columnName) {
        return c.getInt(c.getColumnIndex(columnName));
    }

    protected static long l(Cursor c, String columnName) {
        return c.getLong(c.getColumnIndex(columnName));
    }

    protected static String s(Cursor c, String columnName) {
        return c.getString(c.getColumnIndex(columnName));
    }

    protected static byte[] blob(Cursor c, String columnName) {
        return c.getBlob(c.getColumnIndex(columnName));
    }
}
