package io.jmobile.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.jmobile.browser.utils.LogUtil;
import io.jmobile.browser.utils.Util;

public class DashboardItem extends TableObject {
    public static final String TABLE_NAME = "dashboard";

    public static final String ROW_ID = "_id";
    public static final String DASH_DOWN_ID = "download_id";
    public static final String DASH_TIME = "dash_time";
    public static final String DASH_SIZE = "dash_size";
    public static final String DASH_COUNT = "dash_count";
    public static final String DASH_DATE = "dash_date";
    public static final String DASH_AT = "dash_at";
    public static final Creator<DashboardItem> CREATOR = new Creator<DashboardItem>() {
        @Override
        public DashboardItem createFromParcel(Parcel source) {
            return new DashboardItem(source);
        }

        @Override
        public DashboardItem[] newArray(int size) {
            return new DashboardItem[size];
        }
    };
    private static SimpleDateFormat dayFormat = new SimpleDateFormat("d MMM", Locale.US);
    private static SimpleDateFormat hourFormat = new SimpleDateFormat("kk", Locale.getDefault());
    private static SimpleDateFormat monthFormat = new SimpleDateFormat("MMM, yyyy", Locale.getDefault());
    public long dashAt;
    private long id;
    private long time;
    private long size;
    private int count;
    private String downloadId;
    private String date;

    public DashboardItem() {
        super();
    }

    public DashboardItem(DownloadItem item) {
        this.time = item.getDownloadFileTime();
        this.size = item.getDownloadFileTotalSize();
        this.count = 1;
        this.downloadId = item.getDownloadId();
        this.dashAt = item.getDownloadStartAt();
        this.date = Util.getDateString(dashAt);
    }

//    public static boolean deleteDashboardItem(SQLiteDatabase db, DashboardItem item) {
//        try {
//            db.delete(TABLE_NAME, FAVORITE_URL  + " =? ", new String[] { item.getFavoriteUrl() });
//            return true;
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }
//
//    public static boolean deleteAllDashboardItems(SQLiteDatabase db) {
//        try {
//            db.delete(TABLE_NAME, null, null);
//            return true;
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }

    public DashboardItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.downloadId = in.readString();
        this.time = in.readLong();
        this.size = in.readLong();
        this.count = in.readInt();
        this.date = in.readString();
        this.dashAt = in.readLong();
    }

    public static void createTableDashboard(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(DASH_DOWN_ID + " text not null unique, ")
                .append(DASH_TIME + " integer not null, ")
                .append(DASH_SIZE + " integer not null, ")
                .append(DASH_COUNT + " integer not null, ")
                .append(DASH_DATE + " text not null, ")
                .append(DASH_AT + " integer not null) ")
                .toString());
    }

    public static DashboardItem getDashboardItem(SQLiteDatabase db, int dashAt) {
        List<DashboardItem> list = getDashboardItems(db, null, DASH_AT + " = ?", new String[]{"" + dashAt}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<DashboardItem> getDashboardItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<DashboardItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                DashboardItem item = new DashboardItem()
                        .setRowId(c)
                        .setDashDownId(c)
                        .setDashTime(c)
                        .setDashSize(c)
                        .setDashCount(c)
                        .setDashDate(c)
                        .setDashAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateDashboardItem(SQLiteDatabase db, DashboardItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putDashTime(v)
                    .putDashDownId(v)
                    .putDashSize(v)
                    .putDashCount(v)
                    .putDashDate(v)
                    .putDashAt(v);

            int rowAffected = db.update(TABLE_NAME, v, DASH_DOWN_ID + " =? ", new String[]{item.getDashDownId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getDashDate());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllDashboardItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(downloadId);
        out.writeLong(time);
        out.writeLong(size);
        out.writeInt(count);
        out.writeString(date);
        out.writeLong(dashAt);
    }

    public long getRowId() {
        return this.id;
    }

    public DashboardItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public DashboardItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public DashboardItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getDashDownId() {
        return this.downloadId;
    }

    public DashboardItem setDashDownId(Cursor c) {
        this.downloadId = s(c, DASH_DOWN_ID);

        return this;
    }

    public DashboardItem setDashDownId(String downId) {
        this.downloadId = downId;

        return this;
    }

    public DashboardItem putDashDownId(ContentValues v) {
        v.put(DASH_DOWN_ID, downloadId);

        return this;
    }

    public long getDashTime() {
        return this.time;
    }

    public DashboardItem setDashTime(Cursor c) {
        this.time = l(c, DASH_TIME);

        return this;
    }

    public DashboardItem setDashTime(long time) {
        this.time = time;

        return this;
    }

    public DashboardItem putDashTime(ContentValues v) {
        v.put(DASH_TIME, time);

        return this;
    }

    public long getDashSize() {
        return this.size;
    }

    public DashboardItem setDashSize(Cursor c) {
        this.size = l(c, DASH_SIZE);

        return this;
    }

    public DashboardItem setDashSize(long size) {
        this.size = size;

        return this;
    }

    public DashboardItem putDashSize(ContentValues v) {
        v.put(DASH_SIZE, size);

        return this;
    }

    public int getDashCount() {
        return this.count;
    }

    public DashboardItem setDashCount(Cursor c) {
        this.count = i(c, DASH_COUNT);

        return this;
    }

    public DashboardItem setDashCount(int count) {
        this.count = count;

        return this;
    }

    public DashboardItem putDashCount(ContentValues v) {
        v.put(DASH_COUNT, count);

        return this;
    }

    public String getDashDate() {
        return this.date;
    }

    public DashboardItem setDashDate(Cursor c) {
        this.date = s(c, DASH_DATE);

        return this;
    }

    public DashboardItem setDashDate(String date) {
        this.date = date;

        return this;
    }

    public DashboardItem putDashDate(ContentValues v) {
        v.put(DASH_DATE, date);

        return this;
    }

    public long getDashAt() {
        return this.dashAt;
    }

    public DashboardItem setDashAt(Cursor c) {
        this.dashAt = l(c, DASH_AT);

        return this;
    }

    public DashboardItem setDashAt(long at) {
        this.dashAt = at;

        return this;
    }

    public DashboardItem putDashAt(ContentValues v) {
        v.put(DASH_AT, dashAt);

        return this;
    }

    public String getDashDay() {
        return dayFormat.format(new Date(dashAt));
    }

    public String getDashHour() {
        return hourFormat.format(new Date(dashAt));
    }

    public String getDashMonth() {
        return monthFormat.format(new Date(dashAt));
    }

}
