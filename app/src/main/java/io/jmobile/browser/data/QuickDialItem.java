package io.jmobile.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.browser.utils.LogUtil;

public class QuickDialItem extends TableObject {
    public static final String TABLE_NAME = "quick_dial";
    public static final int MAX_SHOWING = 18;
    public static final String ID_HOW_TO_USE = "HowToUse";
    public static final int ADD_NEW = 1;
    public static final int ADD_RECOMMEND = 2;
    public static final int ADD_BOOKMARK = 3;
    public static final int ADD_TEAM_BOOKMARK = 4;
    public static final int TYPE_SITE = 1;
    public static final int TYPE_APP = 2;

    public static final String ROW_ID = "_id";
    public static final String QUICKDIAL_ID = "quick_dial_id";
    public static final String QUICKDIAL_URL = "quick_dial_url";
    public static final String QUICKDIAL_PACKAGE = "quick_dial_package";
    public static final String QUICKDIAL_NAME = "quick_dial_name";
    public static final String QUICKDIAL_ICON = "quick_dial_icon";
    public static final String QUICKDIAL_AT = "quick_dial_at";
    public static final String QUICKDIAL_SHOW_MAIN = "quick_dial_show_main";
    public static final String QUICKDIAL_ORDER = "quick_dial_order";
    public static final String QUICKDIAL_FROM = "quick_dial_from";
    public static final String QUICKDIAL_FROM_ID = "quick_dial_from_id";
    public static final String QUICKDIAL_TYPE = "quick_dial_type";
    public static final Creator<QuickDialItem> CREATOR = new Creator<QuickDialItem>() {
        @Override
        public QuickDialItem createFromParcel(Parcel source) {
            return new QuickDialItem(source);
        }

        @Override
        public QuickDialItem[] newArray(int size) {
            return new QuickDialItem[size];
        }
    };
    private long id;
    private String quickId;
    private String url;
    private String packageName;
    private String name;
    private byte[] icon;
    private long quickAt;
    private long quickOrder;
    private boolean show = false;
    private int from;
    private String fromId;
    private int type;

    public QuickDialItem() {
        super();
    }

    public QuickDialItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.quickId = in.readString();
        this.packageName = in.readString();
        this.url = in.readString();
        this.name = in.readString();
        in.readByteArray(this.icon);
        this.show = in.readInt() == 1;
        this.quickAt = in.readLong();
        this.from = in.readInt();
        this.fromId = in.readString();
        this.type = in.readInt();
        this.quickOrder = in.readLong();
    }

    public static void createTableQuickDials(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(QUICKDIAL_ID + " text not null unique, ")
                .append(QUICKDIAL_URL + " text, ")
                .append(QUICKDIAL_PACKAGE + " text, ")
                .append(QUICKDIAL_NAME + " text, ")
                .append(QUICKDIAL_ICON + " BLOB, ")
                .append(QUICKDIAL_SHOW_MAIN + " integer not null, ")
                .append(QUICKDIAL_AT + " integer not null, ")
                .append(QUICKDIAL_FROM + " integer not null, ")
                .append(QUICKDIAL_FROM_ID + " text, ")
                .append(QUICKDIAL_TYPE + " integer not null, ")
                .append(QUICKDIAL_ORDER + " integer not null) ")
                .toString());
    }

    public static QuickDialItem getQuickDialItem(SQLiteDatabase db, String id) {
        List<QuickDialItem> list = getQuickDialItems(db, null, QUICKDIAL_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<QuickDialItem> getQuickDialItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<QuickDialItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                QuickDialItem item = new QuickDialItem()
                        .setRowId(c)
                        .setQuickDialId(c)
                        .setQuickDialUrl(c)
                        .setQuickDialPackage(c)
                        .setQuickDialName(c)
                        .setQuickDialIcon(c)
                        .setQuickDialShowing(c)
                        .setQuickDialAt(c)
                        .setQuickDialFrom(c)
                        .setQuickDialFromId(c)
                        .setQuickDialType(c)
                        .setQuickDialOrder(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateQuickDialItem(SQLiteDatabase db, QuickDialItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putQuickDialId(v)
                    .putQuickDialUrl(v)
                    .putQuickDialPackage(v)
                    .putQuickDialName(v)
                    .putQuickDialIcon(v)
                    .putQuickDialShowing(v)
                    .putQuickDialAt(v)
                    .putQuickDialFrom(v)
                    .putQuickDialFromId(v)
                    .putQuickDialType(v)
                    .putQuickDialOrder(v);

            int rowAffected = db.update(TABLE_NAME, v, QUICKDIAL_ID + " =? ", new String[]{item.getQuickDialId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getQuickDialName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean updateQuickDialOrder(SQLiteDatabase db, QuickDialItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putQuickDialOrder(v);

            db.update(TABLE_NAME, v, QUICKDIAL_ID + " = ?", new String[]{item.getQuickDialId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateQuickDialShowing(SQLiteDatabase db, QuickDialItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putQuickDialShowing(v);

            db.update(TABLE_NAME, v, QUICKDIAL_ID + " = ?", new String[]{item.getQuickDialId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateQuickDial(SQLiteDatabase db, QuickDialItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putQuickDialName(v);
            item.putQuickDialUrl(v);
            item.putQuickDialOrder(v);

            db.update(TABLE_NAME, v, QUICKDIAL_ID + " = ?", new String[]{item.getQuickDialId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteQuickDialItem(SQLiteDatabase db, QuickDialItem item) {
        try {
            db.delete(TABLE_NAME, QUICKDIAL_ID + " = ? ", new String[]{item.getQuickDialId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteQuickDialItem(SQLiteDatabase db, BookmarkItem item) {
        try {
            db.delete(TABLE_NAME, QUICKDIAL_FROM_ID + " = ? ", new String[]{item.getBookmarkId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllQuickDialItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(quickId);
        out.writeString(url);
        out.writeString(packageName);
        out.writeString(name);
        out.writeByteArray(icon);
        out.writeInt(show ? 1 : 0);
        out.writeLong(quickAt);
        out.writeInt(from);
        out.writeString(fromId);
        out.writeInt(type);
        out.writeLong(quickOrder);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof QuickDialItem && quickId == ((QuickDialItem) obj).quickId;
    }

    public long getRowId() {
        return this.id;
    }

    public QuickDialItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public QuickDialItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public QuickDialItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getQuickDialId() {
        return this.quickId;
    }

    public QuickDialItem setQuickDialId(Cursor c) {
        this.quickId = s(c, QUICKDIAL_ID);

        return this;
    }

    public QuickDialItem setQuickDialId(String id) {
        this.quickId = id;

        return this;
    }

    public QuickDialItem putQuickDialId(ContentValues v) {
        v.put(QUICKDIAL_ID, quickId);

        return this;
    }

    public String getQuickDialUrl() {
        return this.url;
    }

    public QuickDialItem setQuickDialUrl(Cursor c) {
        this.url = s(c, QUICKDIAL_URL);

        return this;
    }

    public QuickDialItem setquickUrl(String url) {
        this.url = url;

        return this;
    }

    public QuickDialItem putQuickDialUrl(ContentValues v) {
        v.put(QUICKDIAL_URL, url);

        return this;
    }

    public String getQuickdialPackage() {
        return this.packageName;
    }

    public QuickDialItem setQuickDialPackage(String packageName) {
        this.packageName = packageName;

        return this;
    }

    public QuickDialItem setQuickDialPackage(Cursor c) {
        this.packageName = s(c, QUICKDIAL_PACKAGE);

        return this;
    }

    public QuickDialItem putQuickDialPackage(ContentValues v) {
        v.put(QUICKDIAL_PACKAGE, packageName);

        return this;
    }

    public String getQuickDialName() {
        return this.name;
    }

    public QuickDialItem setQuickDialName(Cursor c) {
        this.name = s(c, QUICKDIAL_NAME);

        return this;
    }

    public QuickDialItem setQuickDialName(String name) {
        this.name = name;

        return this;
    }

    public QuickDialItem putQuickDialName(ContentValues v) {
        v.put(QUICKDIAL_NAME, name);

        return this;
    }

    public boolean isQuickDialShowing() {
        return this.show;
    }

    public QuickDialItem setQuickDialShowing(Cursor c) {
        this.show = i(c, QUICKDIAL_SHOW_MAIN) == 1;

        return this;
    }

    public QuickDialItem setQuickDialShowing(boolean show) {
        this.show = show;

        return this;
    }

    public QuickDialItem putQuickDialShowing(ContentValues v) {
        v.put(QUICKDIAL_SHOW_MAIN, show ? 1 : 0);

        return this;
    }

    public byte[] getQuickDialIcon() {
        return this.icon;
    }

    public QuickDialItem setQuickDialIcon(Cursor c) {
        this.icon = blob(c, QUICKDIAL_ICON);

        return this;
    }

    public QuickDialItem setQuickDialIcon(byte[] icon) {
        this.icon = icon;
        return this;
    }

    public QuickDialItem putQuickDialIcon(ContentValues v) {
        v.put(QUICKDIAL_ICON, icon);

        return this;
    }

    public long getQuickDialAt() {
        return this.quickAt;
    }

    public QuickDialItem setQuickDialAt(Cursor c) {
        this.quickAt = l(c, QUICKDIAL_AT);

        return this;
    }

    public QuickDialItem setQuickDialAt(long at) {
        this.quickAt = at;

        return this;
    }

    public QuickDialItem putQuickDialAt(ContentValues v) {
        v.put(QUICKDIAL_AT, quickAt);

        return this;
    }

    public long getQuickDialOrder() {
        return this.quickOrder;
    }

    public QuickDialItem setQuickDialOrder(Cursor c) {
        this.quickOrder = l(c, QUICKDIAL_ORDER);

        return this;
    }

    public QuickDialItem setQuickDialOrder(int order) {
        this.quickOrder = order;

        return this;
    }

    public QuickDialItem putQuickDialOrder(ContentValues v) {
        v.put(QUICKDIAL_ORDER, quickOrder);

        return this;
    }

    public int getQuickDialFrom() {
        return this.from;
    }

    public QuickDialItem setQuickDialFrom(Cursor c) {
        this.from = i(c, QUICKDIAL_FROM);

        return this;
    }

    public QuickDialItem setQuickDialFrom(int from) {
        this.from = from;

        return this;
    }

    public QuickDialItem putQuickDialFrom(ContentValues v) {
        v.put(QUICKDIAL_FROM, from);

        return this;
    }

    public String getQuickdialFromId() {
        return this.fromId;
    }

    public QuickDialItem setQuickDialFromId(String fromId) {
        this.fromId = fromId;

        return this;
    }

    public QuickDialItem setQuickDialFromId(Cursor c) {
        this.fromId = s(c, QUICKDIAL_FROM_ID);

        return this;
    }

    public QuickDialItem putQuickDialFromId(ContentValues v) {
        v.put(QUICKDIAL_FROM_ID, fromId);

        return this;
    }

    public int getQuickDialType() {
        return this.type;
    }

    public QuickDialItem setQuickDialType(Cursor c) {
        this.type = i(c, QUICKDIAL_TYPE);

        return this;
    }

    public QuickDialItem setQuickDialType(int type) {
        this.type = type;

        return this;
    }

    public QuickDialItem putQuickDialType(ContentValues v) {
        v.put(QUICKDIAL_TYPE, type);

        return this;
    }

}
