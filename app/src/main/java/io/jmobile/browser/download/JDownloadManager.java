package io.jmobile.browser.download;

import android.app.NotificationManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.jmobile.browser.BaseApplication;
import io.jmobile.browser.data.DashboardItem;
import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.storage.DBController;
import io.jmobile.browser.storage.SPController;
import io.jmobile.browser.utils.Common;
import io.jmobile.browser.utils.file.FileUtil;

public class JDownloadManager {
//    private static int MAX_DOWNLOAD_COUNT = 3;

    private static Map<String, JDownloadTask> downloadTasks = Collections.synchronizedMap(new HashMap<String, JDownloadTask>());
    private ArrayList<String> downloadRequestList = new ArrayList<>();

    private BaseApplication app;
    private DBController db;
    private SPController sp;
    private BaseApplication.ResourceWrapper r;
    private NotificationManager notiManager;
    private List<DownloadListener> listeners = new ArrayList<DownloadListener>();

    public JDownloadManager(BaseApplication baseApplication) {
        app = baseApplication;
        db = app.getDBController();
        sp = app.getSPController();
        r = app.getResourceWrapper();

//        notiManager = (NotificationManager) app.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        notiManager.cancelAll();
    }

    public void addListener(DownloadListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    public void removeListener(DownloadListener listener) {
        listeners.remove(listener);
    }

    public boolean addToRequestList(String id) {
        boolean result = false;
        synchronized (downloadRequestList) {
            if (!downloadRequestList.contains(id))
                result = downloadRequestList.add(id);
        }

        if (downloadTasks.size() < sp.getMaxDownloads() + 1) {
            DownloadItem item = db.getDownloadItem(id);
            startDownload(id, item, item.getDownloadUrl(), item.getDownloadStartAt() != 0);
        }
        return result;
    }

    public void startDownload(String id, DownloadItem item, String url, boolean continuing) {
        JDownloadTask t = new JDownloadTask(this, id, item);
        synchronized (downloadTasks) {
            downloadTasks.put(id, t);
        }

        if (item != null) {
            t.download(url, Common.getVideoDir(app.getApplicationContext()) + item.getDownloadFileName(), continuing, null);

            if (item.getDownloadStartAt() == 0)
                item.setDownloadStartAt(System.currentTimeMillis());
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_DOWNLOADING);
            db.updateDownloadState(item);

//            setNotification(item);

            for (DownloadListener listener : listeners) {
                listener.onDownloadStart(item);
            }
        }
    }

    public void downloadAll(List<DownloadItem> list) {
        for (DownloadItem item : list) {

            if (item.getDownloadState() != DownloadItem.DOWNLOAD_STATE_DOWNLOADING) {
                item.setDownloadState(DownloadItem.DOWNLOAD_STATE_READY);
                db.updateDownloadState(item);
            }
            addToRequestList(item.getDownloadId());
        }
    }

    public void cancelAll(List<DownloadItem> list) {

        synchronized (downloadTasks) {
            Set<String> keySet = downloadTasks.keySet();
            for (String key : keySet) {
                JDownloadTask task = downloadTasks.get(key);
                task.cancel(true);

                if (task.getId() != null) {
                    task.getItem().setDownloadState(DownloadItem.DOWNLOAD_STATE_PAUSE);
                    db.updateDownloadState(task.getItem());
                }
            }

            downloadTasks.clear();
        }

        synchronized (downloadRequestList) {
            downloadRequestList.clear();
        }

        if (list == null)
            return;

//        for (DownloadItem item : list) {
//            notiManager.cancel((int) item.getRowId());
//        }
    }

    public boolean contains(String id) {
        synchronized (downloadRequestList) {
            return downloadRequestList.contains(id);
        }
    }

    public boolean isEmpty() {
        synchronized (downloadRequestList) {
            return downloadRequestList.isEmpty();
        }
    }

    public void removeFromRequestList(String id) {
        synchronized (downloadRequestList) {
            downloadRequestList.remove(id);
        }
    }

    public void cancelDownload(String id, DownloadItem item) {
        synchronized (downloadTasks) {
            JDownloadTask t = downloadTasks.remove(id);
            if (t != null)
                t.cancel(true);
        }

        removeFromRequestList(id);

        if (item != null) {
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_PAUSE);
            db.updateDownloadState(item);
//            setNotification(item);
        }
    }

    public void onDownloadProgress(String id, DownloadItem item, long progress, long total) {
        synchronized (downloadRequestList) {
            if (!downloadRequestList.contains(id))
                return;
        }

        if (item != null) {
            float downloadProgress = progress / (float) total;
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_DOWNLOADING);
            item.setDownloadFileRemainSize(progress);
            db.updateDownloadState(item);
            item = db.getDownloadItem(item.getDownloadId());
//            setNotification(item);
        }

        for (DownloadListener listener : listeners) {
            listener.onDownloadProgress(item, progress, total);
        }
    }

    public void onDownloadSuccess(String id, DownloadItem item) {
        synchronized (downloadTasks) {
            downloadTasks.remove(id);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "")));
            mIntent.setData(uri);
            app.getApplicationContext().sendBroadcast(mIntent);
        } else {
            try {
                app.getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

        removeFromRequestList(id);

        if (item != null) {
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_DONE);
            item.setDownloadAt((int) System.currentTimeMillis() / 1000);
            item.setDownloadFileRemainSize(item.getDownloadFileTotalSize());
            db.updateDownloadState(item);
            item = db.getDownloadItem(item.getDownloadId());
            setNotification(item);

            DashboardItem dItem = new DashboardItem(item);
            if (dItem != null)
                db.insertOrUpdateDashboardItem(dItem);
        }

        for (DownloadListener listener : listeners) {
            listener.onDownloadSuccess(item);
        }

        int index = 0;
        while (downloadRequestList.size() > index && downloadTasks.size() < sp.getMaxDownloads() + 1) {
            DownloadItem temp = db.getDownloadItem(downloadRequestList.get(index));
            if (temp != null) {
                startDownload(temp.getDownloadId(), temp, temp.getDownloadUrl(), temp.getDownloadStartAt() != 0);
                index++;
            }
        }

    }

    public void onDownloadError(String id, DownloadItem item, int resultCode) {
        synchronized (downloadTasks) {
            downloadTasks.remove(id);
        }

        removeFromRequestList(id);

        if (item != null) {
            item.setDownloadState(DownloadItem.DOWNLOAD_STATE_PAUSE);
            db.updateDownloadState(item);
            item = db.getDownloadItem(item.getDownloadId());
            setNotification(item);
        }

        for (DownloadListener listener : listeners) {
            listener.onDownloadError(item, resultCode);
        }

        int index = 0;
        while (downloadRequestList.size() > index && downloadTasks.size() < sp.getMaxDownloads() + 1) {
            DownloadItem temp = db.getDownloadItem(downloadRequestList.get(index));
            if (temp != null) {
                startDownload(temp.getDownloadId(), temp, temp.getDownloadUrl(), temp.getDownloadStartAt() != 0);
                index++;
            }
        }
    }

    public void setNotification(DownloadItem item) {
//        String contentText = "";
//
//        if (item == null)
//            return;
//
//        switch (item.getDownloadState()) {
//            case DownloadItem.DOWNLOAD_STATE_DONE :
//                contentText = "Complete";
//                break;
//
//            case DownloadItem.DOWNLOAD_STATE_DOWNLOADING :
//                if (item.getDownloadFileRemainSize() <= 0)
//                    contentText = "Ready";
//                else
//                    contentText = FileUtil.convertToStringRepresentation(item.getDownloadFileRemainSize()) + " / " + FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize());
//                break;
//
//            case DownloadItem.DOWNLOAD_STATE_PAUSE :
//                contentText = "Paused";
//                break;
//
//        }
//
//        Bitmap thumbnail = null;
//        if (item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DONE) {
//            String path = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
//            File file = new File(path);
//            if (file.exists()) {
//                thumbnail = ImageUtil.getThumbnailFromFilepath(path);
//            }
//        }
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(app.getBaseContext())
//                .setSmallIcon(R.drawable.icon_download)
//                .setContentTitle(item.getDownloadFileName())
//                .setContentText(contentText)
//                .setAutoCancel(item.getDownloadState() == DownloadItem.DOWNLOAD_STATE_DONE)
//                .setProgress((int) item.getDownloadFileTotalSize(), (int) item.getDownloadFileRemainSize(), false);
//
//        if (thumbnail != null)
//            builder.setLargeIcon(thumbnail);
//        else
//            builder.setLargeIcon(BitmapFactory.decodeResource(app.getBaseContext().getResources(), R.drawable.icon_down));
//
//        Intent resultIntent = new Intent(app.getApplicationContext(), MainActivity.class);
//        resultIntent.putExtra(Common.FRAG_INTENT_DOWNLOAD_STATUS, item.getDownloadState());
//
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(app.getBaseContext());
//        stackBuilder.addParentStack(MainActivity.class);
//        stackBuilder.addNextIntent(resultIntent);
//
//        PendingIntent pendingIntent = stackBuilder.getPendingIntent((int) item.getRowId(), PendingIntent.FLAG_UPDATE_CURRENT);
//        builder.setContentIntent(pendingIntent);
//
//        notiManager.notify((int)item.getRowId(), builder.build());
    }

    public static interface DownloadListener {
        public void onDownloadStart(DownloadItem item);

        public void onDownloadProgress(DownloadItem item, long progress, long total);

        public void onDownloadSuccess(DownloadItem item);

        public void onDownloadError(DownloadItem item, int errorCode);
    }
}
