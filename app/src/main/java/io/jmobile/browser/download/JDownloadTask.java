package io.jmobile.browser.download;

import io.jmobile.browser.data.DownloadItem;
import io.jmobile.browser.utils.file.FileUtil;

public class JDownloadTask extends FileUtil.DownloadTask<JDownloadManager> {
    private static final float PROGRESS_THRESHOLD = 1024 * 500;
    private String id;
    private DownloadItem item;
    private float prev = 0;

    public JDownloadTask(JDownloadManager manager, String id, DownloadItem item) {
        super(manager);
        this.id = id;
        this.item = item;
    }

    @Override
    public void onProgress(long progress, long total) {
        if (ref.get() == null)
            return;

//        float downProgress = progress / (float) total;
        if (progress >= prev + PROGRESS_THRESHOLD) {
            ref.get().onDownloadProgress(id, item, progress, total);
            prev = progress;
        }
    }

    @Override
    public void onSuccess() {
        if (ref.get() == null)
            return;

        ref.get().onDownloadSuccess(id, item);
    }

    @Override
    public void onError(int resultCode) {
        if (ref.get() == null)
            return;

        ref.get().onDownloadError(id, item, resultCode);
    }

    public String getId() {
        return this.id;
    }

    public DownloadItem getItem() {
        return this.item;
    }
}
